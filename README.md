Provide a simple way (buttons ▶ play / ⏱ stop) to count your time in project,
use it with [views kanban](https://www.drupal.org/project/views_kanban) theme
[bootstrap 5](https://www.drupal.org/project/bootstrap5_admin).
It's very useful for the project manager to count the time spent on each ticket.
This module will create table work_time, it stores
entity type, entity id, user id, start time, start end, total minutes

### How it works
- It uses vuejs3 and bootstrap 5 + boostrap 5 icon
- Use [boostrap 5 theme](https://www.drupal.org/project/bootstrap5_admin)
- You can add Work time block, add manual title time start/ time stop
- You can add date range field type and select formatter that counts uptime
- The easiest way is to install module Work time demo.
Then you can go to (/node/add/project) to create the project.
- You can set hourly rates for projects, The location field will be used to
determine the project location, if you want to point the time according to
the project's coordinates.
- After creating a project, you can go to create a task (/node/add/task)
select the project you just created, you can count the time on this task.
- A feature of the module is to calculate time points.
You can go to (/worktime/monthly), and point the hours number per day.
- At the end of month, when the manager wants to see the employee's hours,
he can go to (/worktime/confirm/month-user) to confirm the hours.
- In addition, go to (worktime/checkin) to check in time by the coordinates
according to project.
- Finally, the accountant can summarize everyone's hours each month via the
link (/work-time/payroll). Click on each person's name to print
the earnings statement
- Go to (/worktime/projects) to see a summary of hours by project
and (/worktime/user) to view individual hour reports
- Report you can change report in option by week, month or year
  - /worktime/user/{user_id} : view all time user
  - /worktime/project/{entity_id} : view all time in ticket

### Monthly timesheet
- Create view with content type project
- Add nid (hidden) and project title
- Optional with field price by hour if you want count price total
- Select format with work time sheet fill all options


# Work Time Module

## Description
The Work Time module provides an interface (▶ play/ ⏱ stop buttons)
to track and manage work hours spent on projects. It's designed for
efficiency and ease of use in managing and reporting work hours.

## Features
- Simple play/stop button interface for tracking work hours.
- Detailed work time reports with monthly and holiday views.
- Comprehensive work time management with entity and list builders.
- REST API endpoints for work time data.
- Integration with Views for displaying work time data.
- Customizable work time tracking, with support for holidays and salaries.

## Hook declarations
    HOOK_worktime_timekeeper_alter: This hook can be used to customize or
alter employee work time data before the data is processed or stored.
It allows other modules to intervene in the work time data processing workflow.

    HOOK_worktime_settings_alter: This hook provides a mechanism for other
modules to modify or add to the general configuration for managing work time.
This includes customizing settings such as working hours, holidays,
and other regulations related to work time.

    HOOK_worktime_salary_alter: This hook is used to adjust salary calculations
based on work time. It allows for customization of salary calculations,
adding new elements, or modifying existing ones based on work time.

    HOOK_worktime_table_alter: This hook allows other modules to intervene and
change the data in the work timetable before it is displayed or processed.
This could include adjusting work time records, adding notes,
or changing the display of data.

## Requirements
- Dependencies: `datetime_range`, `rest`, `json_table` modules.

## Installation
Download and install the module as you would with any other Drupal module.

## Configuration
Navigate to the provided configuration link
(admin/config/work_time/configuration) to set up your work time settings.

## Usage
Configure the module, then use the provided ▶ play/ ⏱ stop buttons to track your
work hours on project. Access reports & manage settings through admin interface.

## Contributing
Contributions to the Work Time module are welcome. Please adhere to the Drupal
contribution guidelines when submitting patches or features.

## License
[Add your license information here]
