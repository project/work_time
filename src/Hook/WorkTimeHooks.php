<?php

namespace Drupal\work_time\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Hook implementations for module work time.
 */
class WorkTimeHooks {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help($route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      // Main module help for the work time.
      case 'help.page.work_time':
        try {
          $viewMonthlyRoute = Url::fromRoute('view.work_time_monthly.monthly')->toString(TRUE);
        }
        catch (RouteNotFoundException $e) {
          return '<p>' . $this->t('Enable the module Work time demo for help, you can uninstall it later') . '</p>';
        }
        $addProject = Url::fromRoute('node.add', ['node_type' => 'project'])->toString(TRUE)->getGeneratedUrl();
        $addTask = Url::fromRoute('node.add', ['node_type' => 'task'])->toString(TRUE)->getGeneratedUrl();
        $monthly = $viewMonthlyRoute->getGeneratedUrl();
        $confirm = Url::fromRoute('view.worktime.worktime_confirm_user')->toString(TRUE)->getGeneratedUrl();
        $checkin = Url::fromRoute('view.distance_project.worktime_checkin')->toString(TRUE)->getGeneratedUrl();
        $payroll = Url::fromRoute('view.work_time_payroll.worktime_payroll')->toString(TRUE)->getGeneratedUrl();
        $byProject = Url::fromRoute('view.worktime.projects')->toString(TRUE)->getGeneratedUrl();
        $byUser = Url::fromRoute('view.worktime.user')->toString(TRUE)->getGeneratedUrl();
        return '<p>' . $this->t('Provide a simple way (buttons ▶ play/ ⏱ stop) to count your time work in project, use it with <a class="icon-link icon-link-hover" href="https://www.drupal.org/project/views_kanban"><i class="bi bi-kanban"></i> views kanban</a> and theme <a class="icon-link icon-link-hover" href="https://www.drupal.org/project/bootstrap5_admin"><i class="bi bi-bootstrap-fill"></i> bootstrap 5</a>.<br>It is very useful for the project manager to count the time spent on each ticket.') . '</p>' .
          '<p>' . $this->t('This module will create table work_time, it stores entity type, entity id, user id, start time, start end, total minutes.') . '</p>' .
          '<ul class="list-group list-group-flush"><li class="list-group-item">' . $this->t('Use <a class="icon-link icon-link-hover" href="@bootstrap"><i class="bi bi-bootstrap"></i> boostrap 5 theme</a>', ['@bootstrap' => 'https://www.drupal.org/project/bootstrap5_admin']) . '</li>' .
          '<li class="list-group-item">' . $this->t('You can add Work time block, add manual title time start/ time stop') . '</li>' .
          '<li class="list-group-item">' . $this->t('You can add date range field type and select formatter that counts uptime') . '</li>' .
          '<li class="list-group-item">' . $this->t('The easiest way is to install module Work time demo.') . '</li>' .
          '<li class="list-group-item">' . $this->t('Then you can go to (<a class="icon-link icon-link-hover" href="@addProject"><i class="bi bi-file-plus"></i> /node/add/project</a>) to create the project.', ['@addProject' => $addProject]) . '</li>' .
          '<li class="list-group-item">' . $this->t("You can set hourly rates for projects, The location field will be used to determine the project location, if you want to point the time according to the project's coordinates.") . '</li>' .
          '<li class="list-group-item">' . $this->t('After creating a project, you can go to create a task (<a class="icon-link icon-link-hover" href="@addTask"><i class="bi bi-list-task"></i> /node/add/task</a>) select the project you just created, you can count the time on this task.', ['@addTask' => $addTask]) . '</li>' .
          '<li class="list-group-item">' . $this->t('A feature of the module is to calculate time points. You can go to (<a class="icon-link icon-link-hover" href="@monthly"><i class="bi bi-calendar3"></i> /worktime/monthly</a>), and point the hours number per day.', ['@monthly' => $monthly]) . '</li>' .
          '<li class="list-group-item">' . $this->t("At the end of month, when the manager wants to see the employee's hours, he can go to (<a class='icon-link icon-link-hover' href='@confirm'><i class='bi bi-calendar-check'></i> /worktime/confirm/month-user</a>) to confirm the hours.", ['@confirm' => $confirm]) . '</li>' .
          '<li class="list-group-item">' . $this->t('In addition, go to (<a class="icon-link icon-link-hover" href="@checkin"><i class="bi bi-geo-alt"></i> /worktime/checkin</a>) to check in time by the coordinates according to project.', ['@checkin' => $checkin]) . '</li>' .
          '<li class="list-group-item">' . $this->t("Finally, the accountant can summarize everyone's hours each month via the link (<a class='icon-link icon-link-hover' href='@payroll'><i class='bi bi-file-ruled'></i> /work-time/payroll</a>). Click on each person's name to print the earnings statement.", ['@payroll' => $payroll]) . '</li>' .
          '<li class="list-group-item">' . $this->t('Go to (<a class="icon-link icon-link-hover" href="@byProject"><i class="bi bi-cash-coin"></i> /worktime/projects</a>) to see a summary of hours by project and (<a class="icon-link icon-link-hover" href="@byUser"><i class="bi bi-person-badge"></i> /worktime/user</a>) to view individual hour reports',
            ['@byProject' => $byProject, '@byUser' => $byUser]) . '</li>
        <li class="list-group-item">' . $this->t('Report you can change report in option by week, month or year') .
          '<ul><li>' . $this->t('(/worktime/user/{user_id}): view all user time') . '</li>' .
          '<li>' . $this->t('(/worktime/project/{entity_id}): view all time by ticket') . '</li></ul></li></ul>';

      default:
        break;

    }
  }

  /**
   * Implements hook_views_data().
   */
  #[Hook('views_data')]
  public function viewsData(): array {
    $data['views']['work_time_area'] = [
      'title' => $this->t('Work time area'),
      'help' => $this->t('Provide a filter for work time style.'),
      'area' => [
        'id' => 'work_time_views_link',
      ],
    ];

    $data['views']['work_time_filter_area'] = [
      'title' => $this->t('Work time filter input area'),
      'help' => $this->t('Provide a filter for work time style.'),
      'area' => [
        'id' => 'work_time_filter_input',
      ],
    ];
    return $data;
  }

  /**
   * Implements hook_user_cancel().
   */
  #[Hook('user_cancel')]
  public function userCancel($edit, UserInterface $account, $method) {
    switch ($method) {
      case 'user_cancel_block_unpublish':
        // Unpublish work times.
        $storage = \Drupal::entityTypeManager()->getStorage('work_time');
        $work_time_ids = $storage->getQuery()
          ->accessCheck(FALSE)
          ->condition('uid', $account->id())
          ->condition('status', 1)
          ->execute();
        foreach ($storage->loadMultiple($work_time_ids) as $work_time) {
          $work_time->set('status', FALSE)->save();
        }
        break;

      case 'user_cancel_reassign':
        // Anonymize work times.
        $storage = \Drupal::entityTypeManager()->getStorage('work_time');
        $work_time_ids = $storage->getQuery()
          ->accessCheck(FALSE)
          ->condition('uid', $account->id())
          ->execute();
        foreach ($storage->loadMultiple($work_time_ids) as $work_time) {
          $work_time->setOwnerId(0)->save();
        }
        break;
    }
  }

  /**
   * Implements hook_ENTITY_TYPE_predelete() for user entities.
   */
  #[Hook('user_predelete')]
  public function userPredelete($account) {
    // Delete work times that belong to this account.
    $storage = \Drupal::entityTypeManager()->getStorage('work_time');
    $work_time_ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $account->id())
      ->execute();
    $storage->delete(
      $storage->loadMultiple($work_time_ids)
    );
  }

}
