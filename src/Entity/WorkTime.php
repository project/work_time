<?php

declare(strict_types=1);

namespace Drupal\work_time\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\work_time\WorkTimeInterface;

/**
 * Defines the work time entity class.
 *
 * @ContentEntityType(
 *   id = "work_time",
 *   label = @Translation("Work time"),
 *   label_collection = @Translation("Work times"),
 *   label_singular = @Translation("work time"),
 *   label_plural = @Translation("work times"),
 *   label_count = @PluralTranslation(
 *     singular = "@count work times",
 *     plural = "@count work times",
 *   ),
 *   bundle_label = @Translation("Work time type"),
 *   handlers = {
 *     "list_builder" = "Drupal\work_time\WorkTimeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\work_time\WorkTimeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\work_time\Form\WorkTimeForm",
 *       "edit" = "Drupal\work_time\Form\WorkTimeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "work_time",
 *   admin_permission = "administer work time",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/work-time",
 *     "add-form" = "/work-time/add/{work_time_type}",
 *     "add-page" = "/work-time/add",
 *     "canonical" = "/work-time/{work_time}",
 *     "edit-form" = "/work-time/{work_time}/edit",
 *     "delete-form" = "/work-time/{work_time}/delete",
 *     "delete-multiple-form" = "/admin/content/work-time/delete-multiple",
 *   },
 *   bundle_entity_type = "work_time_type",
 *   field_ui_base_route = "entity.work_time_type.edit_form",
 * )
 */
final class WorkTime extends ContentEntityBase implements WorkTimeInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity id'))
      ->setSetting('target_type', 'node')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => 'Task id',
        ],
        'weight' => 2,
      ])
      ->setDescription(t('Entity code task.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['entity_field'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Datetime range field name'))
      ->setDescription(t('Field name stores work time.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Entity type'))
      ->setDescription(t('Entity type. Example: node, user, paragraph'))
      ->setSettings([
        'allowed_values' => [
          'node' => t('Content'),
          'user' => t('User'),
          'paragraph' => t('Paragraphs'),
          'taxonomy_term' => t('Taxonomy'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reference_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Reference id'))
      ->setSetting('target_type', 'node')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => 'Project id',
        ],
        'weight' => 5,
      ])
      ->setDescription(t('Reference code project.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reference_field'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reference field'))
      ->setDescription(t('Reference field name.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reference_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reference type'))
      ->setDescription(t('Reference field type. Example: node, user, paragraph'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'text_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Started'))
      ->setDescription(t('The time that the work time was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['stopped'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Stopped'))
      ->setDescription(t('The time that the work time was stopped.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['time_total'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Total time'))
      ->setDescription(t('Total time of job.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number_integer',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['payroll'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Type payroll'))
      ->setDescription(t('The type of the payroll.'))
      ->setSettings([
        'allowed_values' => [
          t('Overtime'),
          t('Time normal'),
          t('Work in holiday'),
          t('Work in day off'),
          t('Vacation'),
        ],
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 12,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['history'] = BaseFieldDefinition::create('json')
      ->setLabel(t('History'))
      ->setDescription(t('The history of the worktime.'))
      ->setSetting('json_serialize', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'json_tables_formatter',
        'weight' => 13,
      ])
      ->setDisplayOptions('form', [
        'type' => 'json_table_widget',
        'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
