<?php

namespace Drupal\work_time\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Work time type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "work_time_type",
 *   label = @Translation("Work time type"),
 *   label_collection = @Translation("Work time types"),
 *   label_singular = @Translation("work time type"),
 *   label_plural = @Translation("work times types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count work times type",
 *     plural = "@count work times types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\work_time\Form\WorkTimeTypeForm",
 *       "edit" = "Drupal\work_time\Form\WorkTimeTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\work_time\WorkTimeTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer work time",
 *   bundle_of = "work_time",
 *   config_prefix = "work_time_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/work_time_types/add",
 *     "edit-form" = "/admin/structure/work_time_types/manage/{work_time_type}",
 *     "timeline" = "/admin/structure/work_time_types/manage/{work_time_type}/timeline",
 *     "delete-form" = "/admin/structure/work_time_types/manage/{work_time_type}/delete",
 *     "collection" = "/admin/structure/work_time_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class WorkTimeType extends ConfigEntityBundleBase {

  /**
   * {@inheritdoc}
   *
   * The machine name of this work time type.
   */
  protected string $id;

  /**
   * {@inheritdoc}
   *
   * The human-readable name of the work time type.
   */
  protected string $label;

}
