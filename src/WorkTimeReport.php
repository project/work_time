<?php

namespace Drupal\work_time;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class Work Time Monthly.
 *
 * @package Drupal\work_time\Services
 */
class WorkTimeReport implements WorkTimeReportInterface {

  /**
   * Account user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Work time report constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection, AccountInterface $currentUser) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataMonthly($year, $month) {
    $uid = $this->currentUser->id();
    return $this->getUserMonth($year, $month, $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserMonth($year, $month, $uid) {
    $sql = 'SELECT id, label, DATE(FROM_UNIXTIME(created)) AS date, reference_id, time_total/3600 AS total_time, time_total, status AS is_overtime, type
    FROM work_time
    WHERE uid = :uid AND (type = 0 OR type = 3) AND time_total IS NOT NULL AND FROM_UNIXTIME(created, :format) = :date';
    $records = $this->connection->query($sql, [
      ':uid' => $uid,
      ':format' => "%Y-%m",
      ':date' => "$year-$month",
    ])->fetchAll(\PDO::FETCH_ASSOC);
    return $records;
  }

}
