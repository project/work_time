<?php

namespace Drupal\work_time\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for a work time entity type.
 */
class WorkTimeSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'work_time_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $work_time_type = '') {
    $default = [
      'month' => date('Y-m'),
      'week' => date('Y-\WW'),
    ];
    $defaultFilter = $this->config('work_time.settings')->get('filter');
    $date = empty($default[$defaultFilter]) ? $default['month'] : $default[$defaultFilter];
    $form['#attributes']['class'][] = 'row';
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => 'month',
      '#options' => [
        'week' => $this->t('Week'),
        'month' => $this->t('Month'),
      ],
      '#attributes' => [
        'class' => ['type-timeline', 'w-auto'],
      ],
      '#wrapper_attributes' => [
        'class' => ['container-inline', 'col-auto'],
      ],
    ];
    $form['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Filter'),
      '#default_value' => $date,
      '#attributes' => [
        'class' => ['date-timeline', 'w-auto'],
        'type' => $defaultFilter,
      ],
      '#wrapper_attributes' => [
        'class' => ['container-inline', 'col-auto'],
      ],
    ];
    $form['timeline'] = [
      '#type' => 'container',
      '#title' => $this->t('Attendance'),
      '#attributes' => [
        'class' => ['timeline'],
        'id' => 'work-time-timeline',
        'data-bundle' => $work_time_type,
        'data-date' => $date,
      ],
      'list' => [],
    ];
    $form['#attached'] = [
      'library' => [
        'work_time/work-time-line',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

}
