<?php

namespace Drupal\work_time\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form filter for the work time list builder.
 */
class WorkTimeFilterForm extends FormBase {

  /**
   * WorktimeController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(protected EntityTypeBundleInfoInterface $entityTypeBundleInfo, protected Request $request) {
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'work_time_entity_bundle_filter_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->request->query->all();
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $values['label'] ?? '',
      '#attributes' => [
        'placeholder' => [$this->t('Search')],
      ],
    ];
    $entity_bundles = $this->entityTypeBundleInfo->getBundleInfo('work_time');
    $options = ['' => $this->t('- All -')] + array_map(function ($bundle_info) {
        return $bundle_info['label'];
    }, $entity_bundles);
    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $options,
      '#default_value' => $values['bundle'] ?? '',
    ];

    $form['action'] = [
      '#type' => 'actions',
      'submit' => [
        '#name' => '',
        '#type' => 'submit',
        '#value' => $this->t('Filter'),
      ],
    ];
    $form['#attributes']['class'] = ['views-exposed-form'];
    $form['#theme'] = ['views_exposed_form__work_time', 'views_exposed_form'];
    $form['#disable_inline_form_errors'] = TRUE;
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state->setRedirect('entity.work_time.collection', [], ['query' => $values]);
  }

}
