<?php

namespace Drupal\work_time\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a work time entity type.
 */
class WorkTimeConfigurationForm extends ConfigFormBase {

  /**
   * Name of the config.
   *
   * @var string
   */
  public static $configName = 'work_time.settings';

  /**
   * Constructs a Drupalist object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The type config service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Then entity field manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The country manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, protected EntityFieldManagerInterface $entityFieldManager, protected LanguageManagerInterface $languageManager) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_field.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::$configName];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'work_time_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::$configName);
    $form['work_day'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Non working day in week'),
      '#default_value' => $config->get('work_day') ?? [],
      '#options' => array_map(fn($day) => date('l', strtotime("Sunday +$day days")), range(0, 6)),
      '#description' => $this->t('Pick out non-working days of the week'),
      '#multiple' => TRUE,
    ];
    $form['holidays'] = [
      '#title' => $this->t('General Holidays'),
      '#description' => $this->t('Enter holidays separated by, Example "01/01,01/05,09/04/2023,[22/04/2023 - 26/04/2023]... Format d/m, d/m/Y or [22/04/2023 - 26/04/2023] - Rest in between"'),
      '#type' => 'textarea',
      '#default_value' => $config->get('holidays'),
    ];
    $form['hours'] = [
      '#title' => $this->t('Total hours per day'),
      '#description' => $this->t('To determine working hours per day, For example: 8 (hours per day)'),
      '#type' => 'number',
      '#default_value' => $config->get('hours'),
    ];
    $form['over_hours'] = [
      '#title' => $this->t('Total overtime hours per day'),
      '#description' => $this->t('To defined overtime, Example: 4 (hours a day)'),
      '#type' => 'number',
      '#default_value' => $config->get('over_hours'),
    ];
    $form['filter'] = [
      '#title' => $this->t('Filter mode'),
      '#description' => $this->t('Filter default with'),
      '#type' => 'select',
      '#options' => ['week' => $this->t('Week'), 'month' => $this->t('Month')],
      '#default_value' => $config->get('filter'),
    ];
    $form['language'] = [
      '#title' => $this->t('Language'),
      '#description' => $this->t('Serves to display data in the correct format'),
      '#type' => 'select',
      '#options' => $this->listLanguages() ?? [],
      '#default_value' => $config->get('language'),
      '#require' => TRUE,
    ];

    $form['coefficient_overtime'] = [
      '#title' => $this->t('Coefficient overtime'),
      '#description' => $this->t('Coefficient overtime'),
      '#type' => 'number',
      '#step' => '0.1',
      '#default_value' => $config->get('coefficient_overtime'),
    ];

    $form['coefficient_holiday'] = [
      '#title' => $this->t('Coefficient Work in holiday'),
      '#description' => $this->t('Coefficient Work in holiday'),
      '#type' => 'number',
      '#step' => '0.1',
      '#default_value' => $config->get('coefficient_holiday'),
    ];

    $form['coefficient_day_off'] = [
      '#title' => $this->t('Coefficient Work in day off'),
      '#description' => $this->t('Coefficient Work in day off'),
      '#type' => 'number',
      '#step' => '0.1',
      '#default_value' => $config->get('coefficient_day_off'),
    ];

    $form['coefficient_vacation'] = [
      '#title' => $this->t('Coefficient Vacation'),
      '#description' => $this->t('Coefficient Vacation'),
      '#type' => 'number',
      '#step' => '0.1',
      '#default_value' => $config->get('coefficient_vacation'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $work_days = [];
    foreach ($form_state->getValue('work_day') as $item) {
      if (is_string($item)) {
        $work_days[] = $item;
      }
    }

    $this->config(self::$configName)
      // Set the submitted configuration setting.
      ->set('work_day', $work_days)
      ->set('holidays', $form_state->getValue('holidays'))
      ->set('hours', $form_state->getValue('hours'))
      ->set('over_hours', $form_state->getValue('over_hours'))
      ->set('filter', $form_state->getValue('filter'))
      ->set('coefficient_overtime', $form_state->getValue('coefficient_overtime'))
      ->set('coefficient_holiday', $form_state->getValue('coefficient_holiday'))
      ->set('coefficient_day_off', $form_state->getValue('coefficient_day_off'))
      ->set('coefficient_vacation', $form_state->getValue('coefficient_vacation'))
      ->set('language', $form_state->getValue('language'))
      ->save();
    $this->messenger()
      ->addStatus($this->t('The configuration has been updated.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function listLanguages() {
    $languages = $this->languageManager->getStandardLanguageList() ?? [];

    $language_codes = [];
    if (!empty($languages)) {
      foreach ($languages as $code => $value) {
        $language_codes[$code] = reset($value);
      }
    }
    return $language_codes;
  }

}
