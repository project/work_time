<?php

namespace Drupal\work_time\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\BcRoute;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Represents Api Work Time records as resources.
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
#[RestResource(
  id: 'api_work_time',
  label: new TranslatableMarkup("Api Work Time"),
  uri_paths: [
    'create' => '/api/work-time',
    'canonical' => '/api/work-time/{id}/{action}',
  ]
)]
class ApiWorkTimeResource extends ResourceBase implements DependentPluginInterface {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Database\Connection $dbConnection
   *   The database connection.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, protected Connection $dbConnection, protected Request $request, protected EntityTypeManagerInterface $entityManager, protected AccountProxyInterface $account, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param int|null $id
   *   Entity id.
   * @param string|null $action
   *   Entity field.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The response containing the record.
   */
  public function get(?int $id = NULL, ?string $action = NULL) {
    $records = FALSE;
    if ($action == 'view') {
      $records = $this->loadRecord($id);
    }
    return new ModifiedResourceResponse($records, 200);
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(mixed $data) {
    $workTimeStorage = $this->entityManager->getStorage('work_time');
    $task = '';
    $currentTime = new DrupalDateTime();

    if (!empty($type = $data['entityType']) && !empty($idTask = $data['entityId'])) {
      $taskManager = $this->entityManager->getStorage($type);
      $task = $taskManager->load($idTask);
      $taskManager->resetCache([$data['entityId']]);
    }

    $label = $data['label'] ?? $this->t('New task');
    if (!empty($task)) {
      foreach (['label', 'getDisplayName', 'getName', 'getTitle'] as $method) {
        if (method_exists($task, $method)) {
          $label = $task->$method();
          break;
        }
      }
    }
    $workTime = $workTimeStorage
      ->create([
        'label' => $label,
        'uid' => $this->account->id(),
        'created' => $currentTime->getTimeStamp(),
        'entity_id' => $idTask ?? '',
        'entity_type' => $type ?? '',
        'entity_field' => $data['entityField'] ?? '',
        'reference_id' => $data['referenceId'] ?? '',
        'reference_field' => $data['referenceField'] ?? '',
        'reference_type' => $data['referenceType'] ?? '',
        'stopped' => NULL,
        'bundle' => 'work_time',
        'payroll' => 1,
      ]);
    $workTime->save();
    $id = $workTime->id();
    $created_record = $this->loadRecord($id);

    // Return the newly created record in the response body.
    return new ModifiedResourceResponse($created_record, 200);
  }

  /**
   * Responds to entity PATCH requests.
   *
   * @param int $id
   *   The ID of the record.
   * @param string $action
   *   The action of the record.
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function patch(int $id, string $action, $data = []) {
    $updated_record = [];
    switch ($action) {
      case 'stop_all':
        $updated_record = $this->stopAllWorktime();
        break;

      case 'edit':
        if (is_array($data) && !empty($data)) {
          $data['time_total'] = (int) $data['stopped'] - (int) $data['created'];
          $updated = $this->updateRecord($id, $data);
          if ($updated) {
            $result = $this->loadRecord($id);
            $updated_record['updated'] = $result;
          }
        }
        break;

      case 'sheet_commit':
        $this->commit($data['data']);
        break;

      case 'sheet_confirm':
        $updated_record = $this->confirm($data);
        break;

      case 'checkin':
        $params = $this->request->query->all();
        $checkin_only = $params['checkin_only'] ?? FALSE;
        $entity_type = $params['entity_type'] ?? NULL;
        if ((boolean) $checkin_only) {
          $updated_record = $this->checkin($id, $entity_type);
        }
        else {
          $updated_record = $this->checkinCheckout($id, $entity_type);
        }
        break;

      default:

    }

    return new ModifiedResourceResponse($updated_record);
  }

  /**
   * {@inheritDoc}
   */
  protected function confirm($data) {
    $updated_record = [];

    if (!empty($data['wid_over'])) {
      $dataUpdate['status'] = 1;
      $updated = $this->updateRecord($data['wid_over'], $dataUpdate);
      if ($updated) {
        $time_result = DrupalDateTime::createFromTimestamp($updated->get('created')->value);
        $result = [
          'date' => $time_result->format('Y-m-d'),
          'reference_id' => $updated->get('reference_id')->target_id,
          'status' => $updated->get('status')->value,
          'time_total' => $updated->get('time_total')->value,
          'payroll' => $updated->get('payroll')->value,
          'uid' => $updated->get('uid')->target_id,
        ];
        $updated_record['confirmed'] = $result;
      }
    }
    return $updated_record;
  }

  /**
   * {@inheritDoc}
   */
  protected function commit($data = []) {
    foreach ($data as $reference_id => $item) {
      foreach ($item as $date => $hour_sheet) {
        if ($hour_sheet) {
          foreach ($hour_sheet as $sheet) {
            $workTime = NULL;
            if ($sheet['wid']) {
              $workTime = $this->entityManager->getStorage('work_time')->load($sheet['wid']);
            }
            if ($workTime && $sheet['hour'] > 0) {
              $data_update = [
                'status' => (int) $sheet['status'],
                'payroll' => (int) $sheet['payroll'],
                'time_total' => (int) $sheet['hour'],
                'stopped' => (int) $workTime->get('created')->value + (int) $sheet['hour'],
              ];
              $this->updateRecord($workTime->id(), $data_update);
            }
            elseif (!$workTime && $sheet['hour'] > 0) {
              $this->createSheet($date, $reference_id, $sheet['entity_type'], $sheet['hour'], $sheet['status'], $sheet['payroll']);
            }
            elseif ($workTime && $sheet['hour'] <= 0) {
              $this->delete($workTime->id());
            }
          }
        }
      }
    }
  }

  /**
   * Responds to entity DELETE requests.
   *
   * @param int $id
   *   Entity id.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function delete(int $id) {
    $workTime = $this->entityManager->getStorage('work_time')->load($id);
    if (!empty($workTime)) {
      $workTime->delete();
      $this->logger->notice('Api Work Time record @id has been deleted.', ['@id' => $id]);
    }
    // Deleted responses have an empty body.
    return new ModifiedResourceResponse(NULL, 204);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);
    // Set ID validation pattern.
    if ($method != 'POST') {
      $route->setRequirement('id', '\d+');
      $route->setRequirement('action', '\D+');
    }
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $collection = parent::routes();
    // Take out BC routes added in base class.
    // @see https://www.drupal.org/node/2865645
    // @todo Remove this in Drupal 9.
    foreach ($collection as $route_name => $route) {
      if ($route instanceof BcRoute) {
        $collection->remove($route_name);
      }
    }

    return $collection;
  }

  /**
   * Loads record from database.
   *
   * @param int $id
   *   The ID of the record.
   *
   * @return array
   *   The database record.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  protected function loadRecord(int $id) {
    $record = $this->dbConnection->query(
      'SELECT * FROM {work_time} WHERE id = :id', [':id' => $id])->fetchAssoc();
    if (!$record) {
      throw new NotFoundHttpException('The record was not found.');
    }
    return $record;
  }

  /**
   * {@inheritDoc}
   */
  protected function stopAllWorkTime() {
    $result = [];
    $currentTime = new DrupalDateTime();
    $query = $this->dbConnection->select('work_time', 'wt')
      ->fields('wt', [])
      ->condition('uid', $this->account->id())
      ->condition('bundle', 'work_time')
      ->condition('stopped', NULL, 'IS NULL');
    $queryResult = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if ($queryResult) {
      foreach ($queryResult as $item) {
        $createdTimeLastDay = DrupalDateTime::createFromTimestamp($item['created']);
        $createdTimeLastDay->setTime(23, 59, 59);
        $item['stopped'] = $currentTime->getTimestamp();
        if ($item['stopped'] > $createdTimeLastDay->getTimestamp()) {
          $item['stopped'] = $createdTimeLastDay->getTimestamp();
        }

        $item['time_total'] = (int) $item['stopped'] - (int) $item['created'];
        $item['stopped'] = (int) $item['stopped'];
        $updated = $this->updateRecord($item['id'], $item);
        if ($updated) {
          $result['stopped'][$item['id']] = $this->loadRecord($item['id']);
        }
      }
    }

    return $result;
  }

  /**
   * Updates record.
   *
   * {@inheritDoc}
   */
  protected function updateRecord(int $id_record, array $record) {
    $currentTime = new DrupalDateTime();
    $entity = $this->entityManager->getStorage('work_time')->load($id_record);
    $val = $entity->get('history')?->value ?? [];
    $historyOld = !empty($val) ? json_decode($val) : [];
    $historyOld[] = [
      'history_user' => $this->account->id(),
      'history_time' => $currentTime->getTimestamp(),
      'data' => [
        'id' => $entity->id(),
        'uuid' => $entity->uuid(),
        'label' => $entity->label(),
        'uid' => $entity->get('uid')->target_id,
        'entity_id' => $entity->get('entity_id')->value,
        'entity_field' => $entity->get('entity_field')->value,
        'entity_type' => $entity->get('entity_type')->value,
        'reference_id' => $entity->get('reference_id')->target_id,
        'reference_field' => $entity->get('reference_field')->value,
        'reference_type' => $entity->get('reference_type')->value,
        'created' => $entity->get('created')->value,
        'stopped' => $entity->get('stopped')->value,
        'time_total' => $entity->get('time_total')->value,
        'status' => $entity->get('status')->value,
        'bundle' => $entity->bundle(),
        'payroll' => $entity->get('payroll')->value,
      ],
    ];
    foreach ($record as $field => $value) {
      if ($field != 'history' && $entity->hasField($field)) {
        $entity->set($field, $value);
      }
    }
    $entity->set('history', json_encode($historyOld));
    if ($entity->save()) {
      return $entity;
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  protected function createSheet($date, $reference_id, $reference_type, $hour_sheet, $status, $payroll) {
    $startHour = "08:00:00";
    $created = strtotime("$date $startHour");
    $worktime = [
      'label' => 'Time sheet',
      'uid' => $this->account->id(),
      'entity_id' => NULL,
      'created' => $created,
      'reference_id' => $reference_id,
      'reference_type' => $reference_type ?? NULL,
      'bundle' => 'attendance',
      'status' => $status,
      'payroll' => $payroll,
      'stopped' => $created + (int) $hour_sheet,
      'time_total' => (int) $hour_sheet,
    ];
    $workTime = $this->entityManager->getStorage('work_time')->create($worktime);
    $workTime->save();
  }

  /**
   * {@inheritDoc}
   */
  protected function checkin($id, $entity_type) {
    $result = [];
    $now = new DrupalDateTime();
    $date_start = clone $now;
    $date_end = clone $now;
    $date_start->setTime(0, 0, 0);
    $date_end->setTime(23, 59, 59);

    $query = $this->entityManager->getStorage('work_time')->getQuery()
      ->condition('label', 'Time checkin')
      ->condition('uid', $this->account->id())
      ->condition('entity_id', NULL, 'IS NULL')
      ->condition('entity_type', NULL, 'IS NULL')
      ->condition('bundle', 'checkin')
      ->condition('created', [
        $date_start->getTimestamp(),
        $date_end->getTimestamp(),
      ], 'BETWEEN')
      ->accessCheck(TRUE);
    $queryResult = $query->execute();

    $project_checkin = [];
    if (!empty($queryResult)) {
      foreach ($queryResult as $wt_id) {
        $workTime = $this->entityManager->getStorage('work_time')->load($wt_id);
        $project_checkin[$workTime->id()] = $workTime->get('reference_id')->target_id;
      }
      $wt_id = end($project_checkin) == $id ? array_search($id, $project_checkin) : key($project_checkin);
      $workTime = $this->entityManager->getStorage('work_time')->load($wt_id);
      $time_total = $now->getTimestamp() - $workTime->get('created')->value;

      $updated = $this->updateRecord($workTime->id(), [
        'time_total' => $time_total,
        'stopped' => $now->getTimestamp(),
      ]);
      if ($updated) {
        $result[] = [
          'id' => $workTime->get('reference_id')->target_id,
          'checkin' => $workTime->get('created')->value,
          'checkout' => $workTime->get('stopped')->value,
        ];
      }
      if (end($project_checkin) != $id && !empty($workTime)) {
        $time_end = $workTime->get('stopped')->value;
        $time_total = $time_end - $now->getTimestamp();
        if ($this->createCheckin($now->getTimestamp(), $id, $entity_type, $workTime->get('stopped')->value, $time_total)) {
          $result[] = [
            'id' => $id,
            'checkin' => $now->getTimestamp(),
            'checkout' => $time_end,
          ];
        }
      }
    }
    else {
      $created_end = clone $now;
      $created_end->modify('+8 hours');
      if ($created_end->getTimestamp() > $date_end->getTimestamp()) {
        $created_end = $date_end;
      }
      $time_total = $created_end->getTimestamp() - $now->getTimestamp();
      if ($this->createCheckin($now->getTimestamp(), $id, $entity_type, $created_end->getTimestamp(), $time_total)) {
        $result[] = [
          'id' => $id,
          'checkin' => $now->getTimestamp(),
          'checkout' => $created_end->getTimestamp(),
        ];
      }
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  protected function checkinCheckout($id, $entity_type) {
    $result = [];
    $now = new DrupalDateTime();
    $date_start = clone $now;
    $date_end = clone $now;
    $date_start->setTime(0, 0, 0);
    $date_end->setTime(23, 59, 59);

    $query = $this->entityManager->getStorage('work_time')->getQuery()
      ->condition('label', 'Time checkin')
      ->condition('uid', $this->account->id())
      ->condition('entity_id', NULL, 'IS NULL')
      ->condition('entity_type', NULL, 'IS NULL')
      ->condition('bundle', 'checkin')
      ->condition('created', [
        $date_start->getTimestamp(),
        $date_end->getTimestamp(),
      ], 'BETWEEN')
      ->condition('stopped', NULL, 'IS NULL')
      ->accessCheck(TRUE);
    $queryStopCurrent = $query->execute();

    if (!empty($queryStopCurrent)) {
      $project_checkin = [];
      foreach ($queryStopCurrent as $wt_id) {
        $workTime = $this->entityManager->getStorage('work_time')->load($wt_id);
        $project_checkin[] = $workTime->get('reference_id')->target_id;
        $time_total = $now->getTimestamp() - $workTime->get('created')->value;
        $updated = $this->updateRecord($wt_id, [
          'time_total' => $time_total,
          'stopped' => $now->getTimestamp(),
        ]);
        if ($updated) {
          $result[] = [
            'id' => $workTime->get('reference_id')->target_id,
            'checkin' => $workTime->get('created')->value,
            'checkout' => $workTime->get('stopped')->value,
          ];
        }
      }
      if (!in_array($id, $project_checkin)) {
        if ($this->createCheckin($now->getTimestamp(), $id, $entity_type)) {
          $result[] = [
            'id' => $id,
            'checkin' => $now->getTimestamp(),
            'checkout' => NULL,
          ];
        }
      }
    }
    else {
      if ($this->createCheckin($now->getTimestamp(), $id, $entity_type)) {
        $result[] = [
          'id' => $id,
          'checkin' => $now->getTimestamp(),
          'checkout' => NULL,
        ];
      }
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  protected function createCheckin($time, $reference_id, $reference_type, $stopped = NULL, $time_total = NULL) {
    $worktime = [
      'label' => 'Time checkin',
      'uid' => $this->account->id(),
      'entity_id' => NULL,
      'created' => $time,
      'stopped' => $stopped,
      'time_total' => $time_total,
      'reference_id' => $reference_id,
      'reference_type' => $reference_type,
      'bundle' => 'checkin',
      'payroll' => NULL,
    ];
    $workTime = $this->entityManager->getStorage('work_time')->create($worktime);
    if ($workTime->save()) {
      return TRUE;
    }
    return FALSE;
  }

}
