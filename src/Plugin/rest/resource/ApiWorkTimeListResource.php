<?php

namespace Drupal\work_time\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Select;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Represents Api Work Time records as resources.
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
#[RestResource(
  id: 'api_work_time_list',
  label: new TranslatableMarkup("Api Work Time list"),
  uri_paths: [
    'create' => '/api/work-time-list',
    'canonical' => '/api/work-time-list/{limit}',
  ]
)]
class ApiWorkTimeListResource extends ResourceBase implements DependentPluginInterface {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Database\Connection $dbConnection
   *   The database connection.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, protected Connection $dbConnection, protected Request $request, protected EntityTypeManagerInterface $entityTypeManager, protected AccountProxyInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $limit
   *   The filter of worktime in week or in month.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get($limit = 'all') {
    $workTimes = [];
    $uid = $this->account->id();
    $currentDate = new DrupalDateTime();
    $query = NULL;

    switch ($limit) {
      case "worktime_sheet":
        $params = $this->request->query->all();
        $filter = $params['filter'] ?? $currentDate->format("%Y-%m");
        $filter_mode = $params['filter_mode'] ?? 'month';

        switch ($filter_mode) {
          case 'week':
            $timestamp = strtotime($filter);
            $time_start = strtotime('monday this week 00:00:00', $timestamp);
            $time_end = strtotime('sunday this week 23:59:59', $timestamp);
            break;

          case 'year':
            $timestamp = strtotime($filter);
            $time_start = strtotime('first day of January this year 00:00:00', $timestamp);
            $time_end = strtotime('last day of December this year 23:59:59', $timestamp);
            break;

          default:
            $timestamp = strtotime($filter);
            $time_start = strtotime('first day of this month 00:00:00', $timestamp);
            $time_end = strtotime('last day of this month 23:59:59', $timestamp);
        }

        $query_string = "
        SELECT label, FROM_UNIXTIME(created, '%Y-%m-%d') AS 'date', id, uid, reference_id, bundle, status, payroll, time_total, history
        FROM {work_time}
        WHERE created BETWEEN :time_start AND :time_end AND uid = :uid AND time_total IS NOT NULL AND reference_id IS NOT NULL AND bundle IN ('work_time', 'attendance')";

        $query = $this->dbConnection->query($query_string, [
          ':uid' => $uid,
          ':time_start' => $time_start,
          ':time_end' => $time_end,
        ]);

        break;

      case "sheet":
        $params = $this->request->query->all();
        $date = $params['date'] ?? $currentDate->format("%Y-%m");
        $field_limit = $params['limits']['field'];
        $data_limit = $params['limits']['data'];

        $dateObj = DrupalDateTime::createFromFormat('Y-m', $date);
        $time_start = $dateObj->modify('first day of this month 00:00:00')
          ->getTimestamp();
        $time_end = $dateObj->modify('last day of this month 23:59:59')
          ->getTimestamp();

        $query_string = "
        SELECT FROM_UNIXTIME(created, '%Y-%m-%d') AS 'date', id, uid, reference_id, bundle, status, time_total, history
        FROM {work_time}
        WHERE created BETWEEN :time_start AND :time_end AND time_total > 0 AND reference_id IS NOT NULL AND bundle = 'attendance' AND $field_limit IN (:data_limit[])";

        $query = $this->dbConnection->query($query_string, [
          ':data_limit[]' => $data_limit,
          ':time_start' => $time_start,
          ':time_end' => $time_end,
        ]);

        break;

      case "worktime":
        $params = $this->request->query->all();
        $date = $params['date'] ?? $currentDate->format("%Y-%m");
        $field_limit = $params['limits']['field'];
        $data_limit = $params['limits']['data'];

        $dateObj = DrupalDateTime::createFromFormat('Y-m', $date);
        $time_start = $dateObj->modify('first day of this month 00:00:00')
          ->getTimestamp();
        $time_end = $dateObj->modify('last day of this month 23:59:59')
          ->getTimestamp();

        $query_string = "
        SELECT FROM_UNIXTIME(created, '%Y-%m-%d') AS 'date', id, uid, reference_id, bundle, status, time_total, history
        FROM {work_time}
        WHERE created BETWEEN :time_start AND :time_end AND time_total > 0 AND reference_id IS NOT NULL AND bundle = 'work_time' AND $field_limit IN (:data_limit[])";

        $query = $this->dbConnection->query($query_string, [
          ':data_limit[]' => $data_limit,
          ':time_start' => $time_start,
          ':time_end' => $time_end,
        ]);

        break;

      case "playing":
        $query = $this->dbConnection->select('work_time', 'wt')
          ->fields('wt', [])
          ->condition('uid', $uid)
          ->condition('bundle', 'work_time')
          ->condition('stopped', NULL, 'IS NULL');
        break;

      case "entity_history":
        $params = $this->request->query->all();
        $entity_id = $params['entity_id'] ?? NULL;
        $entity_field = $params['entity_field'] ?? NULL;
        if ($entity_id && $entity_field) {
          $query = $this->dbConnection->select('work_time', 'wt')
            ->fields('wt')
            ->condition('stopped', NULL, 'IS NOT NULL')
            ->condition('entity_id', $entity_id)
            ->condition('bundle', 'work_time')
            ->condition('entity_field', $entity_field)
            ->orderBy('created', 'DESC');
          $query->addExpression("FROM_UNIXTIME(created,'%Y-%m-%d')", 'date');
        }
        break;

      case "entity_ids":
        $params = $this->request->query->all();
        $entity_ids = $params['entity_ids'];
        // Check access.
        $query = $this->dbConnection->select('work_time', 'wt');
        $query->addExpression("CONCAT(wt.entity_type, '-', wt.entity_id)", 'entity');
        $query->addExpression('SUM(wt.time_total)', 'total');
        $query->where("CONCAT(wt.entity_type, '-', wt.entity_id) IN (:entity_ids[])", [':entity_ids[]' => $entity_ids]);
        $query->condition('wt.stopped', NULL, 'IS NOT NULL');
        $query->condition('wt.bundle', 'work_time');
        $query->groupBy('entity');
        break;

      case "week":
        $query = $this->dbConnection->select('work_time', 'wt')
          ->fields('wt', [])
          ->condition('uid', $uid)
          ->condition('bundle', 'work_time')
          ->condition('stopped', NULL, 'IS NOT NULL')
          ->where('FROM_UNIXTIME(created, :format) = :date', [
            ':date' => $currentDate->format("Y-W"),
            ':format' => "%Y-%u",
          ])
          ->orderBy('created', 'DESC')
          ->orderBy('time_total', 'DESC');
        break;

      case "month":
        $query = $this->dbConnection->select('work_time', 'wt')
          ->fields('wt', [])
          ->condition('uid', $uid)
          ->condition('bundle', 'work_time')
          ->condition('stopped', NULL, 'IS NOT NULL')
          ->where('FROM_UNIXTIME(created, :format) = :date', [
            ':date' => $currentDate->format("Y-m"),
            ':format' => "%Y-%m",
          ])
          ->orderBy('created', 'DESC');
        break;

      case 'checkin':
        $time_start = clone $currentDate;
        $time_end = clone $currentDate;
        $time_start->setTime(0, 0, 0);
        $time_end->setTime(23, 59, 59);
        $query = $this->dbConnection->select('work_time', 'wt')
          ->fields('wt', [])
          ->condition('uid', $uid)
          ->condition('bundle', 'checkin')
          ->condition('created', [
            $time_start->getTimestamp(),
            $time_end->getTimestamp(),
          ], 'BETWEEN');
        break;

      default:
        $query = $this->dbConnection->select('work_time', 'wt')
          ->fields('wt', [])
          ->condition('uid', $uid)
          ->condition('bundle', 'work_time')
          ->condition('stopped', NULL, 'IS NOT NULL');
    }
    if ($query) {
      if ($query instanceof Select) {
        $query = $query->execute();
      }
      $workTimes = $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    // Get list projects.
    $projects = [];
    $referenceField = $this->request->get('referenceField');
    if (!empty($referenceField)) {
      $entity_type = $this->request->get('entityType');
      $bundle = $this->request->get('entityBundle');
      $fieldConfig = $this->entityTypeManager->getStorage('field_config')
        ->load($entity_type . '.' . $bundle . '.' . $referenceField);
      if ($fieldConfig) {
        $settings = $fieldConfig->getSetting('handler_settings');
        $handler = explode(':', $fieldConfig->getSetting('handler'));
        $handler = end($handler);
        if (!empty($settings["target_bundles"])) {
          $ids = $this->entityTypeManager->getStorage($handler)->getQuery()
            ->accessCheck(TRUE)
            ->condition('bundle', array_values($settings["target_bundles"]), 'IN')
            ->condition('status', 1)
            ->execute();
          $entities = $this->entityTypeManager->getStorage($handler)
            ->loadMultiple($ids);
          if (!empty($entities)) {
            foreach ($entities as $project) {
              $projects[] = [
                'id' => $project->id(),
                'label' => $project->label(),
              ];
            }
          }
        }
      }
    }
    switch ($limit) {
      case 'worktime_sheet':
      case 'sheet':
      case 'worktime':
        $result = $workTimes;
        break;

      case 'entity_history':
        $records = [];
        foreach ($workTimes as $item) {
          if (empty($records[$item['uid']])) {
            $name = User::load($item['uid'])->getDisplayName();
            preg_match_all('/(?<=\b)\w/iu', $name, $initials);
            $records[$item['uid']] = [
              'name' => $name,
              'label' => $item['label'],
              'initial' => strtoupper(implode('', array_slice($initials[0], 0, 2))),
              'details' => [],
            ];
          }
          $records[$item['uid']]['details'][] = $item;
        }
        if (!empty($records[$this->account->id()])) {
          $historySelf = $records[$this->account->id()];
          unset($records[$this->account->id()]);
          array_unshift($records, $historySelf);
        }
        $result = array_values($records);
        break;

      case 'entity_ids':
        $out = [];
        foreach ($workTimes as $item) {
          $entity = $item['entity'];
          $total = $item['total'];
          $out[$entity] = $total;
        }
        $result['entity_ids'] = $out;
        break;

      case 'playing':
        $result['playing'] = reset($workTimes);
        break;

      case 'checkin':
        $data_result = [];
        if ($workTimes) {
          foreach ($workTimes as $item) {
            $data_result[] = [
              'id' => $item['reference_id'],
              'checkin' => $item['created'],
              'checkout' => $item['stopped'],
            ];
          }
        }
        $result = $data_result;
        break;

      default:
        $result['workTimes'] = $workTimes;
        $result['projects'] = $projects;
    }
    return new ModifiedResourceResponse($result, 200);
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    $workTimeStorage = $this->entityTypeManager->getStorage('work_time');
    $user = $this->account;
    $currentTimestamp = strtotime('now');
    $query = $workTimeStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('uid', $user->id())
      ->condition('bundle', 'work_time')
      ->condition('stopped', NULL, 'IS NULL');
    $workTimes = $query->execute();
    $type = $data['entity_type'] ?? '';
    if (!empty($data['entity_type']) && !empty($data['entity_id'])) {
      $taskManager = $this->entityTypeManager->getStorage($type);
      $task = $taskManager->load($idTask = $data['entity_id']);
      $taskManager->resetCache([$data['entity_id']]);
    }
    // State is stopped.
    if (!empty($workTimes)) {
      foreach ($workTimes as $workTimeId) {
        $workTime = $workTimeStorage->load($workTimeId);
        $start = $workTime->get('created')->value;
        $workTime->set('stopped', $currentTimestamp);
        $workTime->set('time_total', $currentTimestamp - $start);
        $workTime->save();
        // Save value to field.
        if (!empty($task) && !empty($data['entity_field'])) {
          $fieldTimeValue = [
            'value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $start),
            'end_value' => gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $currentTimestamp),
          ];
          $task->get($data['entity_field'])->appendItem($fieldTimeValue);
          $task->save();
        }
      }
    }
    $label = '';
    if (!empty($task)) {
      if (in_array($type, ['user'])) {
        $label = $task->getDisplayName();
      }
      elseif ($type == 'taxonomy_term') {
        $label = $task->getName();
      }
      elseif (method_exists($task, 'getTitle')) {
        $label = $task->getTitle();
      }
    }

    $created_record = ['data' => 'Worktime Save'];
    if ($data['play']) {
      $workTime = $workTimeStorage
        ->create([
          'label' => $label,
          'uid' => $user->id(),
          'created' => $currentTimestamp,
          'entity_id' => $idTask ?? '',
          'entity_type' => $type,
          'entity_field' => $data['entity_field'] ?? '',
          'reference_id' => $data['reference_id'] ?? '',
          'reference_field' => $data['reference_field'] ?? '',
          'reference_type' => $data['reference_type'] ?? '',
          'stopped' => NULL,
          'type' => 'work_time',
        ]);
      $workTime->save();
      $id = $workTime->id();
      $created_record = $this->loadRecord($id);
    }

    // Return the newly created record in the response body.
    return new ModifiedResourceResponse($created_record, 200);
  }

  /**
   * Responds to entity PATCH requests.
   *
   * @param int|null $id
   *   The ID of the record.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function patch(?int $id = NULL) {
    $out = [];
    $entity_ids = json_decode($this->request->getContent(), TRUE);
    if (empty($entity_ids) && $id) {
      $entity_ids = [$id];
    }
    // Check access.
    $query = $this->dbConnection->select('work_time', 'wt');
    $query->addExpression("CONCAT(wt.entity_type, '-', wt.entity_id)", 'entity');
    $query->addExpression('SUM(wt.time_total)', 'total');
    $query->where("CONCAT(wt.entity_type, '-', wt.entity_id) IN (:entity_ids[])", [':entity_ids[]' => $entity_ids]);
    $query->condition('wt.stopped', NULL, 'IS NOT');
    $query->groupBy('entity');

    $results = $query->execute();
    foreach ($results as $result) {
      $entity = $result->entity;
      $total = $result->total;
      $out[$entity] = $total;
    }
    return new ModifiedResourceResponse($out, 200);
  }

  /**
   * Responds to entity DELETE requests.
   *
   * @param int $entity_id
   *   Entity id.
   * @param string $entity_field
   *   Entity field.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function delete($entity_id, $entity_field) {

    // Make sure the record still exists.
    $checkExist = $this->loadRecord($entity_id);
    if (!empty($checkExist)) {
      // Check access.
      $this->dbConnection->delete('work_time')
        ->condition('id', $entity_id)
        ->execute();
      $this->logger->notice('Api Work Time record @id has been deleted.', ['@id' => $entity_id]);
    }
    // Deleted responses have an empty body.
    return new ModifiedResourceResponse(NULL, 204);
  }

  /**
   * Validates incoming record.
   *
   * @param mixed $record
   *   Data to validate.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  protected function validate($record) {
    if (!is_array($record) || count($record) == 0) {
      throw new BadRequestHttpException('No record content received.');
    }

    $allowed_fields = [
      'title',
      'description',
    ];

    if (count(array_diff(array_keys($record), $allowed_fields)) > 0) {
      throw new BadRequestHttpException('Record structure is not correct.');
    }

    if (empty($record['title'])) {
      throw new BadRequestHttpException('Title is required.');
    }
    elseif (isset($record['title']) && strlen($record['title']) > 255) {
      throw new BadRequestHttpException('Title is too big.');
    }
  }

  /**
   * Loads record from database.
   *
   * @param int $id
   *   The ID of the record.
   *
   * @return array
   *   The database record.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  protected function loadRecord($id) {
    $record = $this->dbConnection->query(
      'SELECT * FROM {work_time} WHERE id = :id', [':id' => $id])->fetchAssoc();
    if (!$record) {
      throw new NotFoundHttpException('The record was not found.');
    }
    return $record;
  }

  /**
   * Updates record.
   *
   * @param int $id
   *   The ID of the record.
   * @param array $record
   *   The record to validate.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  protected function updateRecord($id, array $record) {

    // Make sure the record already exists.
    $this->loadRecord($id);

    $this->validate($record);

    // Check access.
    $this->dbConnection->update('work_time')
      ->fields($record)
      ->condition('id', $id)
      ->execute();

    $this->logger->notice('Api Work Time record @id has been updated.', ['@id' => $id]);

    // Return the updated record in the response body.
    $updated_record = $this->loadRecord($id);
    return new ModifiedResourceResponse($updated_record, 200);
  }

  /**
   * {@inheritdoc}
   */
  protected function loadEntities(int $entity_id, string $entity_field) {
    // Check access.
    $query = $this->dbConnection->select('work_time', 'wt')
      ->fields('wt')
      ->condition('stopped', '', 'IS NOT NULL')
      ->condition('entity_id', $entity_id)
      ->condition('entity_field', $entity_field);
    $query->addExpression("FROM_UNIXTIME(created,'%Y-%m-%d')", 'date');
    $work_times = $query->execute();
    $records = [];
    $projects = [];
    $options = ['absolute' => TRUE];
    foreach ($work_times as $work_time) {
      $user = User::load($work_time->uid);
      if (empty($records[$work_time->uid])) {
        $name = $user->getDisplayName();
        preg_match_all('/(?<=\b)\w/iu', $name, $initials);
        $records[$work_time->uid] = [
          'name' => $name,
          'label' => $work_time->label,
          'initial' => strtoupper(implode('', array_slice($initials[0], 0, 2))),
          'details' => [],
        ];
      }

      if (!empty($work_time->reference_id) && !empty($work_time->reference_field) && empty($projects[$work_time->reference_id])) {
        $task = $this->entityTypeManager->getStorage($work_time->entity_type)
          ->load($entity_id);
        $referenceItem = $task->get($work_time->reference_field)
          ->referencedEntities();
        $node = current($referenceItem);
        $projects[$work_time->reference_id] = $node;
        $work_time->project = Link::fromTextAndUrl($node->getTitle(), $node->toUrl('canonical', $options))
          ->toString();
      }
      $records[$work_time->uid]['details'][] = (array) $work_time;
    }
    return array_values($records);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentPlaying($entity_id) {
    $uid = $this->account->id();
    // Check access.
    $query = $this->dbConnection->select('work_time', 'wt')
      ->fields('wt', ['created'])
      ->condition('stopped', '', 'IS NULL')
      ->condition('uid', '', $uid)
      ->condition('entity_id', $entity_id);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
