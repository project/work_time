<?php

namespace Drupal\work_time\Plugin\rest\resource;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rest\Attribute\RestResource;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\work_time\Entity\WorkTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Represents Api Timeline records as resources.
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively, you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
#[RestResource(
  id: 'api_timeline',
  label: new TranslatableMarkup("Api Timeline"),
  uri_paths: [
    'create' => '/api/timeline',
    'canonical' => '/api/timeline/{id}',
  ]
)]
class ApiTimelineResource extends ResourceBase {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param array $data
   *   Data post.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object
   */
  public function post(array $data): ModifiedResourceResponse {
    $start = $data["start"] ? new DrupalDateTime($data["start"]) : NULL;
    $end = $data["end"] ? new DrupalDateTime($data["end"]) : NULL;
    $bundle = $data["bundle"] ?? 'work_time';
    $work_time = WorkTime::create([
      'bundle' => $bundle,
      'created' => $start->getTimestamp(),
      'stopped' => !empty($end) ? $end->getTimestamp() : NULL,
      'uid' => $data['group'] ?? NULL,
    ]);
    if ($end) {
      $work_time->set('time_total', $end->getTimestamp() - $start->getTimestamp());
    }
    if ($work_time->save()) {
      Cache::invalidateTags(["work_time_list:$bundle"]);
      $data += $this->getDataTimeline($work_time);
    }
    $this->logger->notice('Created new api timeline record @id.', ['@id' => $data['id']]);
    return new ModifiedResourceResponse($data, 201);
  }

  /**
   * Responds to GET requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record
   */
  public function get(Request $request): ResourceResponse {
    $currentDate = new DrupalDateTime();
    $params = $request->query->all();
    $date = $params['filter'] ?? $currentDate->format("Y-m");
    $bundle = $params['bundle'] ?? NULL;

    $timestamp = strtotime($date);
    $start = strtotime('first day of this month 00:00:00', $timestamp);
    $end = strtotime('last day of this month 23:59:59', $timestamp);
    if (strpos($date, "-W")) {
      $start = strtotime('monday this week 00:00:00', $timestamp);
      $end = strtotime('sunday this week 23:59:59', $timestamp);
    }

    $groups = $results = [];
    $managerWorkTime = $this->entityTypeManager->getStorage('work_time');
    $query = $managerWorkTime->getQuery()
      ->condition('created', [$start, $end], 'BETWEEN')
      ->accessCheck(TRUE);
    if (!empty($bundle)) {
      $query->condition('bundle', $bundle);
    }
    $query_results = $query->execute();
    if ($query_results) {
      $managerUser = $this->entityTypeManager->getStorage('user');
      foreach ($query_results as $delta => $id) {
        $entity = $managerWorkTime->load($id);
        $uid = $entity->getOwnerId();
        if (empty($groups[$uid])) {
          $user = $managerUser->load($uid);
          $groups[$uid] = [
            'id' => $uid,
            'content' => empty($user) ? $entity->get('label')->value : $user->getDisplayName(),
          ];
        }
        $results[$delta] = $this->getDataTimeline($entity);
        $results[$delta] += ['group' => $uid];
      }
    }
    $data = [
      'items' => array_values($results),
      'groups' => array_values($groups),
    ];

    $build = [
      '#cache' => [
        'tags' => !empty($bundle) ? "work_time_list:$bundle" : 'work_time_list',
      ],
    ];
    return (new ResourceResponse($data))->addCacheableDependency($build);
  }

  /**
   * Responds to PATCH requests.
   *
   * @param int $id
   *   Entity id.
   * @param array $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function patch(int $id, array $data): ModifiedResourceResponse {
    $work_time = WorkTime::load($id);
    if ($work_time) {
      $start = new DrupalDateTime($data['start']);
      $end = !empty($data['end']) ? new DrupalDateTime($data['end']) : NULL;
      $bundle = $work_time->get('bundle')->value;
      $work_time->set('created', $start->getTimestamp());
      $work_time->set('stopped', !empty($end) ? $end->getTimestamp() : NULL);
      if (!empty($end)) {
        $total = $end->getTimestamp() - $start->getTimestamp();
        $work_time->set('time_total', $total);
      }
      if ($work_time->save()) {
        Cache::invalidateTags(["work_time_list:$bundle"]);
        $data += $this->getDataTimeline($work_time);
        $this->logger->notice('The api timeline record @id has been updated.', ['@id' => $id]);
        return new ModifiedResourceResponse($data, 200);
      }
    }
    return new ModifiedResourceResponse($data, 404);
  }

  /**
   * Responds to DELETE requests.
   *
   * @param int $id
   *   Entity id.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function delete(int $id): ModifiedResourceResponse {
    $work_time = WorkTime::load($id);
    if ($work_time) {
      $work_time->delete();
    }
    $this->logger->notice('The api timeline record @id has been deleted.', ['@id' => $id]);
    // Deleted responses have an empty body.
    return new ModifiedResourceResponse($id, 204);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method): Route {
    $route = parent::getBaseRoute($canonical_path, $method);
    // Set ID validation pattern.
    if ($method !== 'POST') {
      $route->setRequirement('id', '\d+');
    }
    return $route;
  }

  /**
   * Return data template vis timeline.
   *
   * @param object $work_time
   *   Entity work time.
   *
   * @return array
   *   Result array vis timeline.
   */
  private function getDataTimeline($work_time = NULL) {
    $result = [];
    if ($work_time) {
      $start = DrupalDateTime::createFromTimestamp($work_time->get('created')->value);
      $stopped = $work_time->get('stopped')->value ? DrupalDateTime::createFromTimestamp($work_time->get('stopped')->value) : NULL;

      $time_total = $work_time->get('time_total')->value;
      $arr_time = [];
      if (!empty($time_total)) {
        $hours = floor($time_total / 3600);
        $minutes = floor($time_total % 3600 / 60);
        $seconds = floor($time_total % 3600 % 60);
        if ($hours) {
          $arr_time[] = $this->t('@hoursh', ['@hours' => $hours]);
        }
        if ($minutes) {
          $arr_time[] = $this->t('@minutesm', ['@minutes' => $minutes]);
        }
        if ($seconds && empty($hours)) {
          $arr_time[] = $this->t('@secondss', ['@$seconds' => $seconds]);
        }
      }
      else {
        $arr_time[] = $start->format('H:i');
      }

      $result['id'] = $work_time->id();
      $result['start'] = $start->format('Y-m-d\TH:i:s');
      $result['end'] = !empty($stopped) ? $stopped->format('Y-m-d\TH:i:s') : NULL;
      $result['title'] = $start->format('H:i');
      if (!empty($stopped)) {
        $result['title'] .= "-" . $stopped->format('H:i');
      }
      $result['content'] = implode(', ', $arr_time);
      $result['type'] = !empty($stopped) ? 'range' : 'point';
    }

    return $result;
  }

}
