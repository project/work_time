<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\Views;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Style plugin to render a list of work time in months.
 *
 * @ingroup views_style_plugins
 */
#[ViewsStyle(
  id: 'work_timekeeper',
  title: new TranslatableMarkup('Monthly work time keeper'),
  help: new TranslatableMarkup('Render a list of work timekeeper in month.'),
  theme: 'views_view_work_timekeeper',
  display_types: ["normal"],
)]
class WorkTimekeeper extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * Constructs a WorkTimekeeper instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected ModuleHandlerInterface $moduleHandler, protected EntityFieldManagerInterface $entityFieldManager, protected Request $request, protected AccountProxyInterface $currentUser, protected WorkTimeHoliday $workTimeHoliday) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_user'),
      $container->get('work_time.holiday'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['wrapper_class'] = ['default' => 'item-list'];
    $options['filter'] = ['default' => 'month'];
    $options['group_row'] = ['default' => ''];
    $options['detail_salary'] = ['default' => ''];
    $options['custom_data_leave'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {

    $fields = $this->displayHandler->getHandlers('field');
    $listOption = [];

    foreach ($fields as $field_name => $field) {
      $info_field = $field->definition;
      $field_definitions = $this->entityFieldManager->getFieldStorageDefinitions($info_field["entity_type"]);
      if ($field_definitions[$field_name]) {
        $listOption[$field_definitions[$field_name]->getType()][$info_field["entity_type"] . '.' . $field->options["field"]] = $info_field['title'];
      }
    }

    $form['group_row'] = [
      '#type' => 'select',
      '#title' => $this->t('Group by'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => $this->getConfigurableFields(['entity_reference'], $listOption, 'work_time'),
      '#description' => $this->t('It will group by project or by user.'),
      '#default_value' => $this->options['group_row'],
    ];

    $form['filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#description' => $this->t('Filter time total, use query example for month ?date=@month or for week or for year ?date=@year', [
        '@month' => date('Y-m'),
        '@week' => date('Y-\WW'),
      ]),
      '#options' => [
        'month' => $this->t('Month'),
        'week' => $this->t('Week'),
      ],
      '#default_value' => $this->options['filter'] ?? '',
      '#required' => TRUE,
    ];

    $form['detail_salary'] = [
      '#type' => 'select',
      '#title' => $this->t('Detail salary'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => $this->listViewDetail(),
      '#default_value' => $this->options['detail_salary'],
    ];

    $form['custom_data_leave'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data leave ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,type:1}, {date: "2023-01-20",uid: 1,type: 4}]. Note: type: 4 (paid leave), type: 1 (unpaid leave)'),
      '#default_value' => $this->options['custom_data_leave'] ?? '',
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the query generated by the view.
    $current_user = $this->currentUser;
    $current_date = new DrupalDateTime();
    $uid = $current_user->id();
    if (!$current_user->hasPermission('administer worktime')) {
      $this->view->query->addWhere(0, 'work_time.uid', $uid, '=');
    }
    $this->view->query->addWhere(0, 'work_time.stopped', NULL, 'IS NOT');
    $filter = $this->request->query->get('date');
    if (!empty($this->options["filter"])) {
      switch ($this->options["filter"]) {
        case 'week':
          if (empty($filter)) {
            $filter = $current_date->format('Y-\WW');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('monday this week 00:00:00', $timestamp);
          $time_end = strtotime('sunday this week 23:59:59', $timestamp);
          break;

        default:
          if (empty($filter)) {
            $filter = $current_date->format('Y-m');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('first day of this month 00:00:00', $timestamp);
          $time_end = strtotime('last day of this month 23:59:59', $timestamp);
      }
      $this->view->query->addWhere(0, 'work_time.created', [
        $time_start,
        $time_end,
      ], 'BETWEEN');
    }

    $this->view->element['#attached']['library'][] = 'work_time/work-time-keeper';

    $display = $this->view->getDisplay();

    $this->view->element['#attached']['drupalSettings']['work_time_keeper'] = [
      'filter_value' => $filter,
      'view_id' => $this->view->id(),
      'display_id' => $display->display['id'],
      'options' => $this->workTimeHoliday->workTimeGetConfig($this->view->style_plugin->options),
    ];
    // Call the parent render() method to generate the table.
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);
    $options = $this->options;
    $group_row = explode('.', $options['group_row']);
    $group_row = end($group_row);
    $this->view->element['#attached']['drupalSettings']['work_time_keeper']['group_record'] = $group_row;
    $data = [];

    // Get label group.
    $labels = $this->displayHandler->getFieldLabels();
    $label_group = [];
    $label_group[$group_row] = $labels[$group_row] ?? '';
    $this->view->element['#attached']['drupalSettings']['work_time_keeper']['label_group'] = $label_group;

    // Url detail.
    if (!empty($options['detail_salary'])) {
      [$detail_view_id, $detail_display_id] = explode(':', $options['detail_salary']);
      $view_detail = Views::getView($detail_view_id);
      $view_detail_display = $view_detail->getDisplay($detail_display_id);
      $setting_args = $view_detail_display->display["display_options"]["arguments"];
      if (!empty($setting_args)) {
        $arg_entity_field_detail = reset($setting_args)['entity_field'];
        $arg_entity_reference_detail = reset($setting_args)['relationship'];
        $this->view->element['#attached']['drupalSettings']['work_time_keeper']['arg_entity_field_detail'] = $arg_entity_field_detail;
        $this->view->element['#attached']['drupalSettings']['work_time_keeper']['arg_entity_reference_detail'] = $arg_entity_reference_detail;
      }
    }

    $list_group_record = [];
    if ($results) {
      foreach ($results as $item) {
        if (!empty($item->_relationship_entities[$group_row]) && $entity_group = $item->_relationship_entities[$group_row]) {
          switch ($entity_group->getEntityTypeId()) {
            case 'user':
              $label = $entity_group->getDisplayName();
              break;

            default:
              $label = $entity_group->getTitle();
          }

          if (empty($list_group_record[$entity_group->id()])) {
            $list_group_record[$entity_group->id()] = [
              'id' => $entity_group->id(),
              'uuid' => $entity_group->uuid(),
              'label' => $label,
            ];
            if (!empty($arg_entity_field_detail)) {
              // Get url report.
              if (!empty($detail_view_id) && !empty($detail_display_id)) {
                $list_group_record[$entity_group->id()][$arg_entity_field_detail] = $arg_entity_field_detail != 'id' ? $entity_group->get($arg_entity_field_detail)->value : $entity_group->id();
                $arg = $list_group_record[$entity_group->id()][$arg_entity_field_detail];
                $url_report = Url::fromRoute("view.$detail_view_id.$detail_display_id", ['arg_0' => $arg]);
                $filter = $this->view->element['#attached']['drupalSettings']['work_time_keeper']['filter_value'];
                $url_report->setOption('query', ['date' => $filter]);
                $list_group_record[$entity_group->id()]['link_detail'] = $url_report->toString();
              }
            }
          }
        }
        $worktime_entity = $item->_entity;
        $data[] = [
          'date' => date('Y-m-d', (int) $worktime_entity->get('created')->value),
          'id' => $worktime_entity->id(),
          'uid' => $worktime_entity->get('uid')->target_id,
          'reference_id' => $worktime_entity->get('reference_id')->target_id,
          'bundle' => $worktime_entity->bundle(),
          'status' => $worktime_entity->get('status')->value,
          'time_total' => $worktime_entity->get('time_total')->value,
          'payroll' => $worktime_entity->get('payroll')->value,
          'history' => $worktime_entity->get('history')->value,
        ];
      }
    }

    $this->view->element['#attached']['drupalSettings']['work_time_keeper']['data'] = $data;
    $this->view->element['#attached']['drupalSettings']['work_time_keeper']['list_group_record'] = array_values($list_group_record);

    // Allow modules to alter the mapping array.
    $this->moduleHandler->alter('worktime_timekeeper', $this->view->element);
  }

  /**
   * Get list of fields.
   */
  protected function getConfigurableFields($type = FALSE, $listFields = [], $type_entity = NULL) {
    $resultField = ['' => $this->t('- None -')];
    if (!$type) {
      $resultField += $listFields;
    }
    else {
      foreach ($listFields as $field_name => $fields) {
        if (!$type_entity) {
          if (in_array($field_name, $type)) {
            $resultField += $fields;
          }
        }
        else {
          foreach ($fields as $field => $label) {
            $type_field = explode('.', $field);
            $type_field = reset($type_field);
            if (in_array($field_name, $type) && $type_field == $type_entity) {
              $resultField += $fields;
            }
          }
        }
      }
    }
    return $resultField;
  }

  /**
   * Get list of fields.
   */
  protected function listViewDetail($type = FALSE, $listFields = [], $type_entity = NULL) {
    $listView = [];
    $views = Views::getEnabledViews();
    foreach ($views as $view) {
      $display_default = $view->getDisplay('default') ?? NULL;
      foreach ($view->get('display') as $display_id => $display) {
        if ($display_id != 'default') {
          if (!empty($display["display_options"]['style']['type']) && $display["display_options"]['style']['type'] == 'work_time_worktime_salary') {
            $listView[$view->id() . ':' . $display['id']] = $view->id() . ' - ' . $display["display_title"];
          }
          elseif (!empty($display_default)) {
            if (!empty($display_default["display_options"]['style']['type']) && $display_default["display_options"]['style']['type'] == 'work_time_worktime_salary') {
              $listView[$view->id() . ':' . $display['id']] = $view->id() . ' - ' . $display["display_title"];
            }
          }
        }
      }
    }
    return $listView;
  }

}
