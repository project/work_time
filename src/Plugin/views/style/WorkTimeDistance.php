<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Work Time Distance style plugin.
 */
#[ViewsStyle(
  id: 'work_time_worktime_distance',
  title: new TranslatableMarkup('Work time distance'),
  help: new TranslatableMarkup('Work time style plugin help.'),
  theme: 'views_view_work_time_worktime_distance',
  display_types: ["normal"],
)]
class WorkTimeDistance extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * Constructs a WorkTimeDistance instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityFieldManagerInterface $entityFieldManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['field_label'] = ['default' => 'title'];
    $options['distance'] = ['default' => '50'];
    $options['distance_unit'] = ['default' => 'meters'];
    $options['checkin_only'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $fields = $this->displayHandler->getHandlers('field');
    $labels = $this->displayHandler->getFieldLabels();
    $listOption = [];
    $view = $this->view;
    $view->initDisplay();
    $view->initHandlers();
    $entity_type = $this->view->getBaseEntityType()->id();
    $entity_bundles = $this->view->filter['type']->value;
    foreach ($fields as $field_name => $field) {
      foreach ($entity_bundles as $bundle) {
        $field_definition = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle)[$field->options["id"]] ?? NULL;
        if ($field_definition) {
          break;
        }
      }
      if (!empty($field_definition)) {
        $field_type_links = $field_definition->getType();
        $listOption[$field_type_links][$field_name] = $labels[$field_name];
      }
    }

    $form['field_label'] = [
      '#title' => $this->t('Field label'),
      '#type' => 'select',
      '#options' => $this->getConfigurableFields([
        'list_string',
        'text',
        'text_long',
        'text_with_summary',
        'string',
        'string_long',
      ], $listOption),
      '#default_value' => $this->options['field_label'],
      '#require' => TRUE,
    ];
    $form['field_lat_lon'] = [
      '#title' => $this->t('Field longitude latitude'),
      '#type' => 'select',
      '#options' => $this->getConfigurableFields(['geofield'], $listOption),
      '#default_value' => $this->options['field_lat_lon'],
      '#require' => TRUE,
    ];
    $form['distance'] = [
      '#title' => $this->t('Distance'),
      '#type' => 'number',
      '#default_value' => $this->options['distance'],
      '#require' => TRUE,
    ];
    $form['distance_unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit distance'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => [
        'meters' => $this->t('Meters'),
        'kilometers' => $this->t('Kilometers'),
        'miles' => $this->t('Miles'),
      ],
      '#default_value' => $this->options['distance_unit'],
      '#require' => TRUE,
    ];
    $form['checkin_only'] = [
      '#title' => $this->t('Checkin only'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['checkin_only'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);

    if (empty($this->view->result)) {
      return;
    }
    $handler = $this->view->style_plugin;

    $option = $this->options;
    $data = [];
    foreach ($results as $delta => $result) {
      $entity = $result->_entity;
      if (!empty($option['field_lat_lon'])) {
        $field_label = $option['field_label'];
        $field_geolocation = $entity->get($option['field_lat_lon']);
        $data[] = [
          'id' => $entity->id(),
          'entity_type' => $entity->getEntityTypeId(),
          'label' => $handler->getField($delta, $field_label),
          'latitude' => $field_geolocation->lat,
          'longitude' => $field_geolocation->lon ?? $field_geolocation->lng,
          'lat_lon' => $field_geolocation->latlon,
          'unit' => $option['distance_unit'],
        ];
      }
    }

    $this->view->element['#attached']['library'][] = 'work_time/work-time-distance';
    $this->view->element["#attached"]["drupalSettings"]['worktime_distance']['result_data'] = $data;
    $this->view->element["#attached"]["drupalSettings"]['worktime_distance']['distance'] = $option['distance'];
    $this->view->element["#attached"]["drupalSettings"]['worktime_distance']['distance_unit'] = $option['distance_unit'];
    $this->view->element["#attached"]["drupalSettings"]['worktime_distance']['checkin_only'] = $option['checkin_only'] ? 1 : 0;
  }

  /**
   * Get list of fields.
   */
  protected function getConfigurableFields($type = FALSE, $listFields = []) {
    $resultField = ['' => $this->t('- None -')];
    if (!$type) {
      $resultField += $listFields;
    }
    else {
      foreach ($listFields as $field_name => $fields) {
        if (in_array($field_name, $type)) {
          $resultField += $fields;
        }
      }
    }
    return $resultField;
  }

}
