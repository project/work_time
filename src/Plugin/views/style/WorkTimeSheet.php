<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Style plugin to render a list of work time in months.
 *
 * @ingroup views_style_plugins
 */
#[ViewsStyle(
  id: 'worktime_sheet',
  title: new TranslatableMarkup('Monthly work time sheet'),
  help: new TranslatableMarkup('Render a list of work time by project in month.'),
  theme: 'views_view_work_time_sheet',
  display_types: ["normal"],
)]
class WorkTimeSheet extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Constructs a WorkTimeSheet instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected ModuleHandlerInterface $moduleHandler, protected EntityFieldManagerInterface $entityFieldManager, protected WorkTimeHoliday $workTimeHoliday, protected Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('work_time.holiday'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['reference_id'] = ['default' => ''];
    $options['price_field'] = ['default' => ''];
    $options['holidays'] = ['default' => '01/01,01/05'];
    $options['custom_data'] = ['default' => ''];
    $options['custom_data_leave'] = ['default' => ''];
    $options['only_point_date'] = ['default' => FALSE];
    $config = $this->configFactory->get('work_time.settings');
    $options['filter'] = ['default' => $config->get('filter')];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form["grouping"]);
    $fields = $this->displayHandler->getHandlers('field');
    $labels = $this->displayHandler->getFieldLabels();
    $type = $field_labels = [];

    foreach ($fields as $field_name => $field) {
      if (!empty($field->options["type"])) {
        $type[$field->options["type"]][$field_name] = $labels[$field_name];
      }
      $field_labels[$field_name] = $labels[$field_name];
    }
    $form['reference_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity id field'),
      '#description' => $this->t('Select project id field'),
      '#options' => $type["number_integer"] ?? [],
      "#empty_option" => $this->t("- Select -"),
      '#required' => TRUE,
      '#default_value' => $this->options['reference_id'] ?? '',
    ];

    $type["number_decimal"] = $type["number_decimal"] ?? [];
    $type["number_integer"] = $type["number_integer"] ?? [];
    $form['price_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit price field'),
      '#description' => $this->t('Select unit price field'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => array_merge($type["number_decimal"], $type["number_integer"]),
      '#default_value' => $this->options['price_field'] ?? '',
    ];
    $form['holidays'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Holidays'),
      '#description' => $this->t('Enter holidays separated by, Example "01/01,01/05,09/04/2023,[22/04/2023 - 26/04/2023]... Format d/m, d/m/Y or [22/04/2023 - 26/04/2023] - Rest in between"'),
      '#default_value' => $this->options['holidays'] ?? '',
    ];
    $form['filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#description' => $this->t('Filter time total, use query example for month ?date=@month or for week or for year ?date=@year', [
        '@month' => date('Y-m'),
        '@week' => date('Y-\WW'),
      ]),
      '#options' => [
        'month' => $this->t('Month'),
        'week' => $this->t('Week'),
      ],
      '#default_value' => $this->options['filter'] ?? '',
      '#required' => TRUE,
    ];
    $form['custom_data'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,total_time:8,reference_id:2}]'),
      '#default_value' => $this->options['custom_data'] ?? '',
    ];

    $form['custom_data_leave'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data leave ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,type:1}, {date: "2023-01-20",uid: 1,type: 4}]. Note: type: 4 (paid leave), type: 1 (unpaid leave)'),
      '#default_value' => $this->options['custom_data_leave'] ?? '',
    ];

    $form['only_point_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only point date'),
      '#description' => $this->t('Only grade that day, not individually.'),
      '#default_value' => $this->options['only_point_date'] ?? FALSE,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);
    $view_options = $this->view->style_plugin->options;
    if (empty($this->view->result)) {
      return;
    }

    $current_date = new DrupalDateTime();
    $filter = $this->request->query->get('date');
    if (!empty($this->options["filter"])) {
      switch ($this->options["filter"]) {
        case 'week':
          if (empty($filter)) {
            $filter = $current_date->format('Y-\WW');
          }
          break;

        default:
          if (empty($filter)) {
            $filter = $current_date->format('Y-m');
          }
      }
    }

    $listProject = [];
    foreach ($results as $result) {
      if ($result->_entity) {
        $price = NULL;
        if (!empty($view_options['price_field'])) {
          $entity_type = $result->_entity->getEntityTypeId();
          $bundle = $result->_entity->bundle();
          $field_price = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle)[$view_options['price_field']];
          $suffix_price = $field_price->getSetting('suffix');
          $price = $result->_entity->get($this->view->style_plugin->options['price_field'])->value;
        }
        $listProject[] = [
          'id' => $result->_entity->id(),
          'entity_type' => $result->_entity->getEntityTypeId(),
          'label' => $this->getTitle($result->_entity),
          'price' => $price,
          'suffix_price' => $suffix_price ?? NULL,
        ];
      }
    }

    $this->view->element['#attached']['library'][] = 'work_time/work-time-sheet';

    $display = $this->view->getDisplay();

    $this->view->element["#attached"]["drupalSettings"]['work_time_sheet'] = [
      'filter_value' => $filter,
      'list' => $listProject,
      'view_id' => $this->view->id(),
      'suffix_price' => $suffix_price ?? NULL,
      'display_id' => $display->display['id'],
      'options' => $this->workTimeHoliday->workTimeGetConfig($this->view->style_plugin->options),
    ];

    // Allow modules to alter the mapping array.
    $this->moduleHandler->alter('worktime_sheet', $this->view->element);
  }

  /**
   * {@inheritDoc}
   */
  protected function getTitle($entity) {
    foreach (['getTitle', 'label', 'getName', 'getDisplayName'] as $method) {
      if (method_exists($entity, $method)) {
        return $entity->$method();
      }
    }
  }

}
