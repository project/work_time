<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\Table;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Style plugin to render a list of work time in months.
 *
 * @ingroup views_style_plugins
 */
#[ViewsStyle(
  id: 'worktime',
  title: new TranslatableMarkup('Work time'),
  help: new TranslatableMarkup('Render a list of worktime.'),
  theme: 'views_view_work_time',
  display_types: ["normal"],
)]
class WorkTime extends Table {

  /**
   * Constructs a WorkTime instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected Request $request, protected AccountProxyInterface $currentUser, protected WorkTimeHoliday $workTimeHoliday) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_user'),
      $container->get('work_time.holiday'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['total'] = ['default' => 'time_total'];
    $options['group'] = ['default' => 'reference_id'];
    $options['filter_time'] = ['default' => 'month'];
    $options['worktime_confirm'] = ['default' => FALSE];

    $options['column_aggregation'] = [
      'totals_row_position' => ['default' => [1 => 0, 2 => 2, 3 => 0]],
      'totals_row_class' => ['default' => ''],
      'default' => FALSE,
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $fields = $this->displayHandler->getHandlers('field');
    $labels = $this->displayHandler->getFieldLabels();
    $field_labels = [];
    foreach ($fields as $field_name => $field) {
      $field_labels[$field_name] = $labels[$field_name];
    }

    $form['group'] = [
      '#type' => 'select',
      '#title' => $this->t('Group'),
      '#options' => $field_labels,
      '#required' => TRUE,
      '#description' => $this->t('Group field.'),
      '#default_value' => $this->options['group'] ?? '',
    ];
    $form['total'] = [
      '#type' => 'select',
      '#title' => $this->t('Total time field'),
      '#description' => $this->t('Select a time total field'),
      '#options' => $field_labels,
      '#required' => TRUE,
      '#default_value' => $this->options['total'] ?? '',
    ];

    $form['filter_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#description' => $this->t('Filter time total, use query example for month ?date=@month or for week ?date=@week or for year ?date=@year', [
        '@month' => date('Y-m'),
        '@week' => date('Y-\WW'),
        '@year' => date('Y'),
      ]),
      '#options' => [
        'month' => $this->t('Month'),
        'week' => $this->t('Week'),
        'year' => $this->t('Year'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->options['filter_time'] ?? '',
    ];

    $form['custom_data_leave'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data leave ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,type:1}, {date: "2023-01-20",uid: 1,type: 4}]. Note: type: 1 (Unpaid leave), type: 2 (Half day), type: 3 (Tardiness leave early), type: 4 (Paid leave)'),
      '#default_value' => $this->options['custom_data_leave'] ?? '',
    ];

    $form['worktime_confirm'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Worktime confirm'),
      '#description' => $this->t('Enabling this option allows manager users to verify overtime hours recorded in the timesheet.'),
      '#default_value' => $this->options['worktime_confirm'] ?? FALSE,
    ];

    $form['column_aggregation'] = [
      '#type' => 'details',
      '#title' => $this->t('Total options'),
    ];

    $form['column_aggregation']['totals_row_class'] = [
      '#title' => $this->t('Total row class'),
      '#type' => 'textfield',
      '#description' => $this->t('The CSS class to provide on the row containing the column aggregations.'),
      '#default_value' => $this->options['column_aggregation']['totals_row_class'] ?? '',
      '#weight' => 3,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the query generated by the view.
    $current_user = $this->currentUser;
    $config = $this->configFactory->get('work_time.settings');
    $uid = $current_user->id();
    if (!$current_user->hasPermission('administer work time')) {
      $this->view->query->addWhere(0, 'work_time.uid', $uid, '=');
    }
    $start = $end = '';
    $this->view->query->addWhere(0, 'stopped', NULL, 'IS NOT');
    $filter = $this->request->query->get('date');
    if (!empty($this->options["filter_time"])) {
      switch ($this->options["filter_time"]) {
        case 'month':
          if (empty($filter)) {
            $filter = date('Y-m');
          }
          $timestamp = strtotime($filter . '-01');
          $start = strtotime('first day of this month', $timestamp);
          $end = strtotime('last day of this month', $timestamp) + 24 * 3600 - 1;
          break;

        case 'week':
          if (empty($filter)) {
            $filter = date('Y-\WW');
          }
          $timestamp = strtotime($filter);
          $start = strtotime('this monday', $timestamp);
          $end = strtotime('this sunday', $timestamp);
          break;

        case 'year':
          $month = date('m');
          if (empty($filter)) {
            $filter = date('Y');
          }
          $year = $filter;
          if ($year < date('Y')) {
            $month = 12;
          }
          $timestamp = strtotime("$year-$month-01");
          $start = strtotime("$year-01-01");
          $end = strtotime('last day of this month', $timestamp) + 24 * 3600 - 1;
          break;

      }
      $this->view->query->addWhere(0, 'created', [$start, $end], 'BETWEEN');
    }
    $this->view->element['#attached']['library'][] = 'work_time/work-time-confirm';
    $displayObj = $this->view->getDisplay();
    $param = [
      'mode_display' => $this->options["group"],
      'mode_time' => $this->options['filter_time'],
      'date' => $filter,
    ];
    $holidays = $this->workTimeHoliday->getHolidays('', substr($filter, 0, 4));
    $workDay = array_filter($config->get('work_day') ?? [], function ($val) {
      return is_string($val);
    });
    $url = Url::fromRoute('work_time.ajax_timegeneral', $param)->toString();

    $this->view->element['#attached']['drupalSettings']['table_worktime'] = [
      'url' => $url,
      'options' => $this->options,
      'filter' => $filter,
      'view_id' => $this->view->id(),
      'display_id' => $displayObj->display['id'],
      'holidays' => $holidays,
      'work_day' => $workDay ?? [],
      'start' => date('Y-m-d', $start),
      'end' => date('Y-m-d', $end),
      'custom_data_leave' => $this->options['custom_data_leave'] ?? "",
    ];
    if ($this->options['worktime_confirm']) {
      $url = Url::fromRoute('work_time.ajax_confirm');
      $this->view->element['#attached']['drupalSettings']['table_worktime']['url_confirm'] = $url->toString();
    }
    // Call the parent render() method to generate the table.
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);

    if (empty($this->view->result)) {
      return;
    }

    if (!empty($this->view->total_rows)) {
      $group = $this->options["group"];
      $grouping = $this->options["grouping"];
      // $time_total = $this->options["total"];
      if (!empty($grouping)) {
        $grouping = $grouping[0]['field'];
      }
      $checked = [];
      foreach ($results as $index => $result) {
        $entity = $result->_entity;
        $work_time = [
          'id' => $entity->id(),
          'entity_id' => $entity->entity_id->value,
          'reference_id' => $entity->reference_id->value,
          'created' => date('Y-m-d', $entity->created->value),
          'time_total' => $entity->time_total->value,
          'status' => $entity->status->value,
          'bundle' => $entity->bundle(),
        ];
        $groupValue = $entity->get($group)->getString();
        if (!empty($grouping)) {
          $groupingValue = $entity->get($grouping)->getString();
          $groupValue .= '-' . $groupingValue;
        }
        if (empty($checked[$groupValue])) {
          $checked[$groupValue] = [
            'id' => $index,
            'worktime' => [$work_time],
          ];
        }
        else {
          $checked[$groupValue]['worktime'][] = $work_time;
          $results[$checked[$groupValue]['id']]->_entity->set('time_total', array_sum(array_column($checked[$groupValue]['worktime'], 'time_total')));
          unset($results[$index]);
          unset($this->view->result[$index]);
        }
      }
      if (!empty($checked)) {
        $this->view->element["#attached"]["drupalSettings"]['work_time']['list'] = $checked;
      }
      $this->renderFields($results);
    }
  }

}
