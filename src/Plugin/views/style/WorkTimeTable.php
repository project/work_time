<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\DefaultStyle;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Style plugin to render a list of work time in months.
 *
 * @ingroup views_style_plugins
 */
#[ViewsStyle(
  id: 'worktime_table',
  title: new TranslatableMarkup('Work time table'),
  help: new TranslatableMarkup('Render a list of worktime.'),
  theme: 'views_view_work_time_table',
  display_types: ["normal"],
)]
class WorkTimeTable extends DefaultStyle {

  /**
   * Constructs a WorkTimeTable instance object.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected ModuleHandlerInterface $moduleHandler, protected Request $request, protected AccountProxyInterface $currentUser, protected EntityFieldManagerInterface $entityFieldManager, protected WorkTimeHoliday $workTimeHoliday) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_user'),
      $container->get('entity_field.manager'),
      $container->get('work_time.holiday'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['group_table'] = ['default' => 'uid'];
    $options['group_row'] = ['default' => 'reference_id'];
    $options['filter'] = ['default' => 'month'];
    $options['worktime_confirm'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $fields = $this->displayHandler->getHandlers('field');
    $labels = $this->displayHandler->getFieldLabels();
    $listOption = [];

    foreach ($fields as $field_name => $field) {
      $info_field = $field->definition;
      $field_definitions = $this->entityFieldManager->getFieldStorageDefinitions($info_field["entity_type"]);
      if ($field_definitions[$field_name]) {
        $listOption[$field_definitions[$field_name]->getType()][$info_field["entity_type"] . '.' . $field->options["field"]] = $labels[$field->options["field"]];
      }
    }

    $form['group_table'] = [
      '#type' => 'select',
      '#title' => $this->t('Report by'),
      '#options' => $this->getConfigurableFields(['entity_reference'], $listOption, 'work_time'),
      '#description' => $this->t('It will group by project or by user.'),
      '#default_value' => $this->options['group_table'] ?? '',
      '#required' => TRUE,
    ];

    $form['group_row'] = [
      '#type' => 'select',
      '#title' => $this->t('Group by'),
      '#options' => $this->getConfigurableFields(['entity_reference'], $listOption, 'work_time'),
      '#description' => $this->t('In case of reporting by user, group by project or in case of reporting by user, select project.'),
      '#default_value' => $this->options['group_row'] ?? '',
      '#required' => TRUE,
    ];

    $form['filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#description' => $this->t('Filter time total, use query example for month ?date=@month or for week or for year ?date=@year', [
        '@month' => date('Y-m'),
        '@week' => date('Y-\WW'),
        '@year' => date('Y'),
      ]),
      '#options' => [
        'month' => $this->t('Month'),
        'week' => $this->t('Week'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $this->options['filter'] ?? '',
      '#required' => TRUE,
    ];

    $form['price_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Price'),
      '#options' => $this->getConfigurableFields(['integer', 'decimal', 'float'], $listOption),
      '#default_value' => $this->options['price_field'] ?? '',
    ];

    $form['custom_data_leave'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data leave ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,type:1}, {date: "2023-01-20",uid: 1,type: 4}]. Note: type: 4 (paid leave), type: 1 (unpaid leave)'),
      '#default_value' => $this->options['custom_data_leave'] ?? '',
    ];

    $form['worktime_confirm'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Worktime confirm'),
      '#description' => $this->t('Enabling this option allows manager users to verify overtime hours recorded in the timesheet.'),
      '#default_value' => $this->options['worktime_confirm'] ?? FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the query generated by the view.
    $current_user = $this->currentUser;
    $current_date = new DrupalDateTime();
    $uid = $current_user->id();
    if (!$current_user->hasPermission('administer work time')) {
      $this->view->query->addWhere(0, 'work_time.uid', $uid, '=');
    }
    $this->view->query->addWhere(0, 'work_time.stopped', NULL, 'IS NOT');
    $filter = $this->request->query->get('date');
    if (!empty($this->options["filter"])) {
      switch ($this->options["filter"]) {
        case 'week':
          if (empty($filter)) {
            $filter = $current_date->format('Y-\WW');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('monday this week 00:00:00', $timestamp);
          $time_end = strtotime('sunday this week 23:59:59', $timestamp);
          break;

        case 'year':
          if (empty($filter)) {
            $filter = $current_date->format('Y-m');
          }
          $timestamp = strtotime($filter . '-01');
          $time_start = strtotime('first day of January this year 00:00:00', $timestamp);
          $time_end = strtotime('last day of December this year 23:59:59', $timestamp);
          break;

        default:
          if (empty($filter)) {
            $filter = $current_date->format('Y-m');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('first day of this month 00:00:00', $timestamp);
          $time_end = strtotime('last day of this month 23:59:59', $timestamp);
      }
      $this->view->query->addWhere(0, 'work_time.created', [
        $time_start,
        $time_end,
      ], 'BETWEEN');
    }

    $this->view->element['#attached']['library'][] = 'work_time/bootstrap_table';
    $display = $this->view->getDisplay();

    $this->view->element['#attached']['drupalSettings']['work_time_table'] = [
      'filter_value' => $filter,
      'view_id' => $this->view->id(),
      'display_id' => $display->display['id'],
      'options' => $this->workTimeHoliday->workTimeGetConfig($this->view->style_plugin->options),
    ];
    if ($this->options['worktime_confirm']) {
      $url = Url::fromRoute('work_time.ajax_confirm', [
        'entity' => '${entity}',
        'user' => '${user}',
        'date' => '${date}',
      ]);
      $this->view->element['#attached']['drupalSettings']['work_time_table']['url_confirm'] = $url->toString();
    }
    // Call the parent render() method to generate the table.
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);
    $table_worktime_settings = $this->view->element['#attached']['drupalSettings']['work_time_table'] + [
      'group_table' => NULL,
      'group_record' => NULL,
      'list_group_record' => [],
      'label_group' => [],
    ];
    if (!empty($this->view->total_rows)) {

      $display_handler = $this->displayHandler;
      $field_options = $display_handler->getOption('fields');
      $view_options = $this->view->style_plugin->options;

      $group_table = $this->options["group_table"];
      $group_row = $this->options["group_row"];
      $field_price = $view_options['price_field'];

      if (!empty($group_table)) {
        $group_table = explode('.', $group_table);
        $group_table = end($group_table);
      }
      if (!empty($group_row)) {
        $group_row = explode('.', $group_row);
        $group_row = end($group_row);
      }
      if (!empty($field_price)) {
        $field_price = explode('.', $view_options['price_field']);
        $field_price = end($field_price);
      }

      // Set field group.
      $table_worktime_settings['group_table'] = $group_table;
      $table_worktime_settings['group_record'] = $group_row;

      // Get label group.
      $labels = $this->displayHandler->getFieldLabels();
      $label_group = [];
      $label_group[$group_table] = $labels[$group_table];
      $label_group[$group_row] = $labels[$group_row];
      $table_worktime_settings['label_group'] = $label_group;

      $result_data = [];
      $list_group_table = [];
      $list_group_record = [];
      $list_price = [];
      foreach ($results as $result) {
        if ($field_price) {
          $price_relationship = $field_options[$field_price]['relationship'];
          $entity_reference = $result->_relationship_entities[$price_relationship] ?? NULL;
          if (!empty($entity_reference)) {
            $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_reference->getEntityTypeid(), $entity_reference->bundle());
            if (!empty($field_definitions[$field_price])) {
              $suffix_price = $field_definitions[$field_price]->getSetting('suffix');
            }
            if ($entity_reference->{$field_price}) {
              $price = $entity_reference->{$field_price}->value;
            }
          }
          $list_price[$price_relationship][$entity_reference->id()] = [
            'id' => $entity_reference->id(),
            'price' => $price ?? NULL,
            'suffix_price' => $suffix_price ?? NULL,
          ];
        }

        $worktime_entity = $result->_entity;

        $entity_group_table = current($worktime_entity->get($group_table)
          ->referencedEntities());
        $entity_group_record = current($worktime_entity->get($group_row)
          ->referencedEntities());

        if ($entity_group_table || $entity_group_record) {
          $methods = [
            'label',
            'getDisplayName',
            'getName',
            'getTitle',
          ];
          foreach ($methods as $method) {
            if ($entity_group_table) {
              if (method_exists($entity_group_table, $method)) {
                // Get list group table.
                $list_group_table[$entity_group_table->id()] = [
                  'id' => $entity_group_table->id(),
                  'label' => $entity_group_table->$method(),
                ];
              }
            }
            if ($entity_group_record) {
              if (method_exists($entity_group_record, $method)) {
                // Get list group row.
                $list_group_record[$entity_group_record->id()] = [
                  'id' => $entity_group_record->id(),
                  'label' => $entity_group_record->$method(),
                ];
              }
            }
          }
        }

        $result_data[] = [
          'date' => date('Y-m-d', (int) $worktime_entity->get('created')->value),
          'id' => $worktime_entity->id(),
          'uid' => $worktime_entity->get('uid')->target_id,
          'reference_id' => $worktime_entity->get('reference_id')->target_id,
          'bundle' => $worktime_entity->bundle(),
          'status' => $worktime_entity->get('status')->value,
          'time_total' => $worktime_entity->get('time_total')->value,
          'payroll' => $worktime_entity->get('payroll')->value,
          'history' => $worktime_entity->get('history')->value,
          'relationship' => $worktime_entity->get('history')->value,
        ];
      }

      $table_worktime_settings['data'] = $result_data;

      $table_worktime_settings['list_group_table'] = array_values($list_group_table);
      $table_worktime_settings['list_group_record'] = array_values($list_group_record);
      $table_worktime_settings['list_price'] = $list_price;
      $table_worktime_settings['price_reference'] = $price_relationship ?? NULL;
      $table_worktime_settings['suffix_price'] = $suffix_price ?? '';
    }
    $this->view->element['#attached']['drupalSettings']['work_time_table'] = $table_worktime_settings;

    // Allow modules to alter the mapping array.
    $this->moduleHandler->alter('worktime_table', $this->view->element);
  }

  /**
   * Get list of fields.
   */
  protected function getConfigurableFields($type = FALSE, $listFields = [], $type_entity = NULL) {
    $resultField = ['' => $this->t('- None -')];
    if (!$type) {
      $resultField += $listFields;
    }
    else {
      foreach ($listFields as $field_name => $fields) {
        if (!$type_entity) {
          if (in_array($field_name, $type)) {
            $resultField += $fields;
          }
        }
        else {
          foreach ($fields as $field => $label) {
            $type_field = explode('.', $field);
            $type_field = reset($type_field);
            if (in_array($field_name, $type) && $type_field == $type_entity) {
              $resultField += $fields;
            }
          }
        }
      }
    }
    return $resultField;
  }

}
