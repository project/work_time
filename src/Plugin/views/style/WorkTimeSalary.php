<?php

namespace Drupal\work_time\Plugin\views\style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Worktime salary style plugin.
 */
#[ViewsStyle(
  id: 'work_time_worktime_salary',
  title: new TranslatableMarkup('Work time salary'),
  help: new TranslatableMarkup('Use to print monthly pay slips.'),
  theme: 'views_view_work_time_worktime_salary',
  display_types: ["normal"],
)]
class WorkTimeSalary extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * Constructs a WorkTimeSalary instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory, protected ModuleHandlerInterface $moduleHandler, protected EntityFieldManagerInterface $entityFieldManager, protected Request $request, protected AccountProxyInterface $currentUser, protected WorkTimeHoliday $workTimeHoliday) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_user'),
      $container->get('work_time.holiday'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['wrapper_class'] = ['default' => 'item-list'];
    $options['filter'] = ['default' => 'month'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $fields = $this->displayHandler->getHandlers('field');
    $listOption = [];

    foreach ($fields as $field_name => $field) {
      $info_field = $field->definition;
      $field_definitions = $this->entityFieldManager->getFieldStorageDefinitions($info_field["entity_type"]);
      if ($field_definitions[$field_name]) {
        $listOption[$field_definitions[$field_name]->getType()][$info_field["entity_type"] . '.' . $field->options["field"]] = $info_field['title'];
      }
    }

    $form['fixed_salary'] = [
      '#type' => 'select',
      '#title' => $this->t('Fixed salary'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => $this->getConfigurableFields(['integer'], $listOption),
      '#default_value' => $this->options['fixed_salary'],
      '#required' => TRUE,
    ];

    $form['performance_salary'] = [
      '#type' => 'select',
      '#title' => $this->t('Performance salary'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => $this->getConfigurableFields(['integer'], $listOption),
      '#default_value' => $this->options['performance_salary'],
    ];

    $form['filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#description' => $this->t('Filter time total, use query example for month ?date=@month or for week or for year ?date=@year', [
        '@month' => date('Y-m'),
        '@week' => date('Y-\WW'),
        '@year' => date('Y'),
      ]),
      '#options' => [
        'month' => $this->t('Month'),
        'week' => $this->t('Week'),
        'year' => $this->t('Year'),
      ],
      '#default_value' => $this->options['filter'] ?? '',
      '#required' => TRUE,
    ];

    $form['group'] = [
      '#type' => 'select',
      '#title' => $this->t('Group'),
      "#empty_option" => $this->t("- Select -"),
      '#options' => $this->getConfigurableFields(['entity_reference'], $listOption, 'work_time'),
      '#default_value' => $this->options['group'],
    ];

    $form['unit_salary'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit salary'),
      '#options' => $this->workTimeHoliday->listCodeCurrency(),
      '#default_value' => $this->options['unit_salary'],
      '#require' => TRUE,
    ];

    $form['custom_data_leave'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url custom data leave ajax load'),
      '#description' => $this->t('Array object. Example [{date:"2023-01-20",uid:1,type:1}, {date: "2023-01-20",uid: 1,type: 4}]. Note: type: 4 (paid leave), type: 1 (unpaid leave)'),
      '#default_value' => $this->options['custom_data_leave'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the query generated by the view.
    $current_date = new DrupalDateTime();
    $this->view->query->addWhere(0, 'work_time.stopped', NULL, 'IS NOT');
    $filter = $this->request->query->get('date');
    if (!empty($this->options["filter"])) {
      switch ($this->options["filter"]) {
        case 'week':
          if (empty($filter)) {
            $filter = $current_date->format('Y-\WW');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('monday this week 00:00:00', $timestamp);
          $time_end = strtotime('sunday this week 23:59:59', $timestamp);
          break;

        default:
          if (empty($filter)) {
            $filter = $current_date->format('Y-m');
          }
          $timestamp = strtotime($filter);
          $time_start = strtotime('first day of this month 00:00:00', $timestamp);
          $time_end = strtotime('last day of this month 23:59:59', $timestamp);
      }
      $this->view->query->addWhere(0, 'work_time.created', [
        $time_start,
        $time_end,
      ], 'BETWEEN');
    }

    $this->view->element['#attached']['library'][] = 'work_time/work-time-salary';

    $display = $this->view->getDisplay();

    $this->view->element['#attached']['drupalSettings']['work_time'] = [
      'filter_value' => $filter,
      'view_id' => $this->view->id(),
      'display_id' => $display->display['id'],
      'options' => $this->workTimeHoliday->workTimeGetConfig($this->view->style_plugin->options),
    ];
    // Call the parent render() method to generate the table.
    parent::query();
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($results) {
    parent::preRender($results);
    $options = $this->options;
    $group = explode('.', $options['group']);
    $group = end($group);
    $fixed_salary = explode('.', $this->options["fixed_salary"]);
    $fixed_salary = end($fixed_salary);
    $performance_salary = explode('.', $this->options["performance_salary"]);
    $performance_salary = end($performance_salary);
    $data = [];
    $data_groups = [];
    if ($results) {
      foreach ($results as $item) {
        if ($entity_group = $item->_relationship_entities[$group]) {
          switch ($entity_group->getEntityTypeId()) {
            case 'user':
              $label = $entity_group->getDisplayName();
              break;

            default:
              $label = $entity_group->getTitle();
          }
          $data_groups[$entity_group->id()] = [
            'id' => $entity_group->id(),
            'label' => $label,
            'salary_fixed' => $entity_group->get($fixed_salary)->getValue() ? $entity_group->get($fixed_salary)->value : NULL,
            'salary_performance' => $entity_group->get($performance_salary)->getValue() ? $entity_group->get($performance_salary)->value : NULL,
          ];
        }
        $worktime_entity = $item->_entity;
        $data[] = [
          'date' => date('Y-m-d', (int) $worktime_entity->get('created')->value),
          'id' => $worktime_entity->id(),
          'uid' => $worktime_entity->get('uid')->target_id,
          'reference_id' => $worktime_entity->get('reference_id')->target_id,
          'bundle' => $worktime_entity->bundle(),
          'status' => $worktime_entity->get('status')->value,
          'time_total' => $worktime_entity->get('time_total')->value,
          'payroll' => $worktime_entity->get('payroll')->value,
          'history' => $worktime_entity->get('history')->value,
        ];
      }
    }

    $config = $this->configFactory->get('system.site');
    $site_name = $config->get('name');
    $site_logo = theme_get_setting('logo.url');

    $this->view->element['#attached']['drupalSettings']['work_time']['site_logo'] = $site_logo;
    $this->view->element['#attached']['drupalSettings']['work_time']['site_name'] = $site_name;
    $this->view->element['#attached']['drupalSettings']['work_time']['data'] = $data;
    $this->view->element['#attached']['drupalSettings']['work_time']['data_group'] = array_values($data_groups);

    // Allow modules to alter the mapping array.
    $this->moduleHandler->alter('worktime_salary', $this->view->element);
  }

  /**
   * Get list of fields.
   */
  protected function getConfigurableFields($type = FALSE, $listFields = [], $type_entity = NULL) {
    $resultField = ['' => $this->t('- None -')];
    if (!$type) {
      $resultField += $listFields;
    }
    else {
      foreach ($listFields as $field_name => $fields) {
        if (!$type_entity) {
          if (in_array($field_name, $type)) {
            $resultField += $fields;
          }
        }
        else {
          foreach ($fields as $field => $label) {
            $type_field = explode('.', $field);
            $type_field = reset($type_field);
            if (in_array($field_name, $type) && $type_field == $type_entity) {
              $resultField += $fields;
            }
          }
        }
      }
    }
    return $resultField;
  }

}
