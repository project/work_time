<?php

namespace Drupal\work_time\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Views area handler for a work time input.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("work_time_search_report")
 */
class WorkTimeSearchReport extends AreaPluginBase {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['wrapper_class'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['wrapper_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper class'),
      '#default_value' => $this->options['wrapper_class'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $view_id = $this->view->id();
    $display_id = $this->view->getDisplay()->display['id'];
    $label_group = $this->view->element['#attached']['drupalSettings']["$view_id-$display_id"]['label_group'];
    $group_table = $this->view->element['#attached']['drupalSettings']["$view_id-$display_id"]['group_table'];
    $view_id = $this->view->element['#attached']['drupalSettings']["$view_id-$display_id"]['views']['view_id'];
    $display_id = $this->view->element['#attached']['drupalSettings']["$view_id-$display_id"]['views']['display_id'];
    $this->view->element['#attached']['library'][] = 'work_time/work-time-search-report';
    $container = [
      '#theme' => 'work_time_search_report_area',
      '#wrapper_class' => $this->options['wrapper_class'],
      '#placeholder' => $label_group[$group_table],
      '#view_id' => $view_id,
      '#display_id' => $display_id,
    ];

    return $container;
  }

}
