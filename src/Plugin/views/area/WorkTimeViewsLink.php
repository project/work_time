<?php

namespace Drupal\work_time\Plugin\views\area;

use Drupal\Core\Url;
use Drupal\views\Attribute\ViewsArea;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Views area handler for a work time button.
 *
 * @ingroup views_area_handlers
 */
#[ViewsArea("work_time_views_link")]
class WorkTimeViewsLink extends AreaPluginBase {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    $filter = $this->request->query->get('date');
    $filter_time = $this->view->style_plugin->options["filter_time"];
    if (in_array($filter_time, ['month', 'week'])) {
      return [
        'date' => [
          '#type' => 'date',
          '#title' => $this->t('Date'),
          '#value' => $filter ?? '',
          '#attributes' => [
            'type' => $this->options['time'],
            'class' => ['form-control', 'w-auto', 'work-time-filter'],
          ],
          '#attached' => [
            'library' => ['work_time/work-time-area'],
          ],
        ],
      ];
    }

    $view_name = $this->view->storage->id();
    $display_id = $this->view->current_display;
    $year = $filter ?? date('Y');
    $container = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['btn-group'],
      ],
    ];
    foreach (range($year - 1, $year + 1) as $year) {
      $url = Url::fromRoute("view.$view_name.$display_id", [
        'date' => $year,
      ]);
      $container[$year] = [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $year,
        '#attributes' => [
          'class' => ['btn', 'btn-outline-primary'],
        ],
      ];
    }
    return $container;
  }

}
