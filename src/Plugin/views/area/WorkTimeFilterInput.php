<?php

namespace Drupal\work_time\Plugin\views\area;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\views\Attribute\ViewsArea;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Views area handler for a work time input.
 *
 * @ingroup views_area_handlers
 */
#[ViewsArea("work_time_filter_input")]
class WorkTimeFilterInput extends AreaPluginBase {

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $this->view->element['#attached']['library'][] = 'work_time/work-time-filter-input';
    $filter = $this->request->query->get('date');
    $filter_mode = $this->view->style_plugin->options["filter"];
    $current_date = new DrupalDateTime();
    if (empty($filter)) {
      switch ($filter_mode) {
        case 'week':
          $filter = $current_date->format('Y-\WW');
          break;

        case 'year':
          $filter = $current_date->format('Y');
          break;

        default:
          $filter = $current_date->format('Y-m');
      }
    }

    $container = [
      '#type' => 'container',
      '#attributes' => [
        'data-filter' => $filter,
        'data-filter-mode' => $filter_mode,
        'class' => ['work-time-filter-input'],
      ],
    ];

    return $container;
  }

}
