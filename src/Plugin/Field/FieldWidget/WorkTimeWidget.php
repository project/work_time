<?php

namespace Drupal\work_time\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'work_time' field widget.
 */
#[FieldWidget(
  id: 'work_time',
  label: new TranslatableMarkup('Work Time'),
  field_types: ['daterange'],
)]
class WorkTimeWidget extends DateRangeDefaultWidget {

  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateStorage;

  /**
   * Constructs a Work time Formatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_storage
   *   Storage field date.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityStorageInterface $date_storage, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $date_storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_format'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $entity = $items->getEntity();
    if (is_object($entity)) {
      $workTimeStorage = $this->entityTypeManager->getStorage('work_time');
      $query = $workTimeStorage
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('entity_id', $entity->id());
      $workTimes = $query->execute();
      if (!empty($workTimes)) {
        $workTimes = array_values($workTimes);
        if (!empty($workTimeId = $workTimes[$delta])) {
          $workTime = $workTimeStorage->load($workTimeId);
          $start = $workTime->get('created') ? date('Y-m-d H:i:s', $workTime->get('created')->value) : NULL;
          $stop = $workTime->get('stopped') ? date('Y-m-d H:i:s', $workTime->get('stopped')->value) : NULL;
          if ($start) {
            $element["value"]["#default_value"] = new DrupalDateTime($start);
          }
          if ($stop) {
            $element["end_value"]["#default_value"] = new DrupalDateTime($stop);
          }
        }

      }
    }
    return $element;
  }

}
