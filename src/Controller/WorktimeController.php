<?php

namespace Drupal\work_time\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\work_time\WorkTimeHoliday;
use Drupal\work_time\WorkTimeReportInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines work-time Controller class.
 */
class WorktimeController extends ControllerBase {

  /**
   * WorktimeController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database service.
   * @param \Drupal\work_time\WorkTimeReportInterface $workTimeReport
   *   Work time report service.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   The work-time holiday service.
   */
  public function __construct(protected Connection $database, protected WorkTimeReportInterface $workTimeReport, protected WorkTimeHoliday $workTimeHoliday) {
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('work_time.monthly'),
      $container->get('work_time.holiday'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function timesheet(Request $request) {
    $post = $request->request->all();
    $delete = $post['delete'] ?? FALSE;
    $modifyNormal = $post['modify_normal_sheet'] ?? FALSE;
    $modifyOver = $post['modify_over_sheet'] ?? FALSE;
    $createNormal = $post['create_normal_sheet'] ?? FALSE;
    $createOver = $post['create_over_sheet'] ?? FALSE;
    $labels = $post['label'] ?? FALSE;
    $route_name = $request->request->get('route_name');
    $reference_type = $request->request->get('reference_type');
    $workTimeStorage = $this->entityTypeManager()->getStorage('work_time');
    $user = $this->currentUser();

    // Delete work time.
    if (!empty($delete)) {
      foreach ($delete as $reference_id => $timesheet) {
        foreach ($timesheet as $date => $status) {
          // Check access.
          $select = $this->database->select('work_time', 'w')
            ->fields('w', ['id'])
            ->condition('reference_id', $reference_id)
            ->condition('uid', $user->id());
          $select->where('DATE(FROM_UNIXTIME(created)) = :date', [':date' => $date]);
          $results = $select->execute();
          $wids = $results->fetchCol();
          $entities = $workTimeStorage->loadMultiple($wids);
          $workTimeStorage->delete($entities);
        }
      }
    }
    // Modify work time normal.
    if (!empty($modifyNormal)) {
      $currentTime = new DrupalDateTime();
      foreach ($modifyNormal as $reference_id => $timesheet) {
        foreach ($timesheet as $date => $hour) {
          // Check access.
          $select = $this->database->select('work_time', 'w')
            ->fields('w', ['id'])
            ->condition('uid', $user->id())
            ->condition('bundle', 'attendance')
            ->condition('status', 1)
            ->condition('reference_id', $reference_id);
          $select->where('DATE(FROM_UNIXTIME(created)) = :date', [':date' => $date]);
          $wids = $select->execute()->fetchCol();
          $count = count($wids);
          foreach ($wids as $id) {
            if (!$hour) {
              $this->database->delete('work_time')
                ->condition('id', $id)
                ->execute();
            }
            else {
              $time_total = $hour * $count * 60 * 60;

              $work_time = $this->database->query(
                'SELECT * FROM {work_time} WHERE id = :id', [':id' => $id])->fetchAssoc();
              if (!$work_time) {
                throw new NotFoundHttpException('The record was not found.');
              }
              $history = json_decode($work_time['history']) ?? [];
              if (array_key_exists('history', $work_time)) {
                unset($work_time['history']);
              }
              $work_time['time_total'] = $time_total;
              $history[] = [
                'history_time' => $currentTime->getTimestamp(),
                'history_user' => $user->id(),
                'data' => $work_time,
              ];

              $record = [
                'time_total' => $time_total,
                'status' => 1,
                'history' => json_encode($history),
              ];

              $query = $this->database->update('work_time')
                ->fields($record)
                ->condition('id', $id);

              $query->execute();
            }
          }
        }
      }
    }
    // Modify work time over.
    $currentTime = new DrupalDateTime();
    if (!empty($modifyOver)) {
      foreach ($modifyOver as $reference_id => $timesheet) {
        foreach ($timesheet as $date => $hour) {
          // Check access.
          $select = $this->database->select('work_time', 'w')
            ->fields('w', ['id'])
            ->condition('uid', $user->id())
            ->condition('bundle', 'attendance')
            ->condition('status', 0)
            ->condition('reference_id', $reference_id);
          $select->where('DATE(FROM_UNIXTIME(created)) = :date', [':date' => $date]);
          $wids = $select->execute()->fetchCol();
          $count = count($wids);
          foreach ($wids as $id) {
            if (!$hour) {
              $this->database->delete('work_time')
                ->condition('id', $id)
                ->execute();
            }
            else {
              $time_total = $hour * $count * 60 * 60;

              $work_time = $this->database->query(
                'SELECT * FROM {work_time} WHERE id = :id', [':id' => $id])->fetchAssoc();
              if (!$work_time) {
                throw new NotFoundHttpException('The record was not found.');
              }
              $history = json_decode($work_time['history']) ?? [];
              if (array_key_exists('history', $work_time)) {
                unset($work_time['history']);
              }
              $work_time['time_total'] = $time_total;
              $history[] = [
                'history_time' => $currentTime->getTimestamp(),
                'history_user' => $user->id(),
                'data' => $work_time,
              ];

              $record = [
                'time_total' => $time_total,
                'status' => 0,
                'history' => json_encode($history),
              ];

              $query = $this->database->update('work_time')
                ->fields($record)
                ->condition('id', $id);

              $query->execute();
            }
          }
        }
      }
    }

    // Create work time normal.
    if (!empty($createNormal)) {
      foreach ($createNormal as $reference_id => $timesheet) {
        $label = $labels[$reference_id] ?? $this->t('Time sheet');
        foreach ($timesheet as $date => $hour) {
          $startHour = "08:00:00";
          $created = strtotime("$date $startHour");
          $time_total = $hour * 60 * 60;
          $stopped = $created + $time_total;
          $worktime = [
            'label' => $label,
            'uid' => $user->id(),
            'created' => $created,
            'stopped' => $stopped,
            'time_total' => $time_total,
            'reference_id' => $reference_id,
            'reference_type' => $reference_type ?? 'node',
            'bundle' => 'attendance',
            'status' => 1,
          ];
          $worktime['history'] = json_encode([[
            'history_time' => $currentTime->getTimestamp(),
            'history_user' => $user->id(),
            'data' => $worktime,
          ],
          ]);
          $workTime = $workTimeStorage->create($worktime);
          $workTime->save();
        }
      }
    }

    // Create work time over.
    if (!empty($createOver)) {
      $currentTime = new DrupalDateTime();
      foreach ($createOver as $reference_id => $timesheet) {
        $label = $labels[$reference_id] ?? $this->t('Time sheet');
        foreach ($timesheet as $date => $hour) {
          $startHour = "08:00:00";
          $created = strtotime("$date $startHour");
          $time_total = $hour * 60 * 60;
          $stopped = $created + $time_total;
          $worktime = [
            'label' => $label,
            'uid' => $user->id(),
            'created' => $created,
            'stopped' => $stopped,
            'time_total' => $time_total,
            'reference_id' => $reference_id,
            'reference_type' => $reference_type ?? 'node',
            'bundle' => 'attendance',
            'status' => 0,
          ];
          $worktime['history'] = json_encode([[
            'history_time' => $currentTime->getTimestamp(),
            'history_user' => $user->id(),
            'data' => $worktime,
          ],
          ]);
          $workTime = $workTimeStorage->create($worktime);
          $workTime->save();
        }
      }
    }

    $url = Url::fromRoute($route_name);
    return new RedirectResponse($url->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxTimesheet(Request $request) {
    $getMonth = $request->request->get('month');
    [$year, $month] = explode('-', $getMonth);
    $records = $this->workTimeReport
      ->getDataMonthly($year, $month);
    $response['data'] = $records;
    return new JsonResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxTimeGeneral($mode_display, $mode_time, $date, Request $request) {
    $filter = $request->get('date');
    // Check access.
    $query = $this->database->select('work_time', 'wt');
    $query->fields('wt', [$mode_display]);
    $query->condition('bundle', 'worktime');
    $query->condition($mode_display, NULL, 'IS NOT NULL');
    $resultModeDisplay = $query->distinct()->execute()->fetchAll();
    if ($resultModeDisplay) {
      $data = [];
      foreach ($resultModeDisplay as $item) {
        switch ($mode_time) {
          case 'week':
            if (empty($date)) {
              $date = date('Y-\WW');
            }
            $timestamp = strtotime($date);
            $start = strtotime('monday this week', $timestamp);
            $end = strtotime('sunday this week', $timestamp);
            $data[] = $this->genSql($start, $end, $mode_display, $item);
            break;

          case 'month':
            if (!empty($date)) {
              $timestamp = strtotime($date . '-01');
              $start = strtotime('first day of this month', $timestamp);
              $end = strtotime('last day of this month', $timestamp);
              $data[] = $this->genSql($start, $end, $mode_display, $item);
            }
            break;

          case 'year':
            $month = date('m');
            $year = $filter ?? date('Y');
            if ($year < date('Y')) {
              $month = 12;
            }
            $timestamp = strtotime("$year-$month-01");
            $start = strtotime("$year-01-01");
            $end = strtotime('last day of this month', $timestamp) + 24 * 3600 - 1;
            $data[] = $this->genSql($start, $end, $mode_display, $item, "%Y-%m");
            break;
        }
      }
      return new JsonResponse([
        'data' => $data,
        'method' => 'POST',
        'status' => 200,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function genSql($start, $end, $mode_display, $item, $format = "%Y-%m-%d") {
    $record = $this->database->query("
    SELECT FROM_UNIXTIME(`created`, :format) AS 'date_time', ANY_VALUE(uid) AS 'uid', ANY_VALUE(status) AS 'status', reference_id, SUM(time_total) AS 'time_total' FROM {work_time}
    WHERE FROM_UNIXTIME(`created`, :format) BETWEEN :startTime AND :endTime AND $mode_display = :id AND reference_id IS NOT NULL AND type = 3
    GROUP BY reference_id, status, FROM_UNIXTIME(`created`, :format)", [
      ':startTime' => date('Y-m-d', $start),
      ':endTime' => date('Y-m-d', $end),
      ':format' => $format,
      ':id' => $item->{$mode_display},
    ])->fetchAll();
    return $record;
  }

  /**
   * Confirm overtime.
   *
   * {@inheritdoc}
   */
  public function ajaxConfirm(Request $request) {
    $post = $request->request->all();
    $id = $post['id'];
    $action = $post['action'];
    $currentTime = new DrupalDateTime();
    $work_time = $this->entityTypeManager()->getStorage('work_time')->load($id);
    $history = [
      'history_time' => $currentTime->getTimestamp(),
      'history_user' => $this->currentUser()->id(),
      'data' => $work_time,
    ];
    $work_time->set('history', json_encode($history));
    if ($action == 'close') {
      $work_time->set('time_total', 0);
    }
    if ($action == 'accept') {
      $work_time->set('status', 1);
    }
    // To save an entity.
    $work_time->save();

    return new JsonResponse([
      'method' => 'POST',
      'status' => 200,
    ]);
  }

  /**
   * Visit location.
   *
   * {@inheritdoc}
   */
  public function ajaxVisit(EntityInterface $entity) {
    $isDone = FALSE;
    $now = new DrupalDateTime();
    $label = $entity->label() ?? "Time visit";
    $start = DrupalDateTime::createFromTimestamp(strtotime('midnight', $now->getTimestamp()));
    $end = DrupalDateTime::createFromTimestamp(strtotime('+1 day', $start->getTimestamp()) - 1);
    $created = clone $now;
    $stopped = clone $now;
    $stopped = $stopped->modify("+10 hours");
    if ($stopped->getTimestamp() > $end->getTimestamp()) {
      $stopped = $end;
    }
    $storageWorkTime = $this->entityTypeManager()->getStorage('work_time');

    $query_is_exist = $storageWorkTime->getQuery()
      ->condition('created', [
        $start->getTimestamp(),
        $end->getTimestamp(),
      ], 'BETWEEN')
      ->condition('bundle', 'fingerprint')
      ->condition('uid', $this->currentUser()->id())
      ->condition('reference_id', $entity->id())
      ->sort('created', 'DESC')
      ->accessCheck(TRUE);
    $worktime_reference = $query_is_exist->execute();
    if (!in_array($entity->id(), $worktime_reference)) {
      $query = $storageWorkTime->getQuery()
        ->condition('created', [
          $start->getTimestamp(),
          $end->getTimestamp(),
        ], 'BETWEEN')
        ->condition('bundle', 'fingerprint')
        ->condition('uid', $this->currentUser()->id())
        ->sort('created', 'DESC')
        ->accessCheck(TRUE);
      $worktime_ids = $query->execute();
      if (!empty($worktime_ids)) {
        $work_time = $storageWorkTime->load(reset($worktime_ids));
        $stopped = DrupalDateTime::createFromTimestamp((int) $work_time->get('stopped')->value);
        $work_time->set('stopped', $created->getTimestamp());
        $work_time->set('time_total', abs((int) $work_time->get('created')->value - (int) $created->getTimestamp()));
        $work_time->save();
      }
      $time_total = $created->getTimestamp() - $stopped->getTimestamp();
      $worktime = [
        'label' => $label,
        'uid' => $this->currentUser()->id(),
        'created' => $created->getTimestamp(),
        'stopped' => $stopped->getTimestamp(),
        'time_total' => abs($time_total),
        'reference_id' => $entity->id(),
        'reference_type' => $entity->getEntityTypeId() ?? 'node',
        'bundle' => 'fingerprint',
      ];
      $workTime = $storageWorkTime->create($worktime);
      if ($workTime->save()) {
        $isDone = TRUE;
      }
    }

    return new JsonResponse([
      'isDone' => $isDone,
      'method' => 'POST',
      'status' => 200,
    ]);
  }

}
