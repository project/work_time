<?php

namespace Drupal\work_time;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a work time entity type.
 */
interface WorkTimeInterface extends ContentEntityInterface, EntityOwnerInterface {

}
