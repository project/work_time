<?php

declare(strict_types=1);

namespace Drupal\work_time;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of work time type entities.
 *
 * @see \Drupal\work_time\Entity\WorkTimeType
 */
final class WorkTimeTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No work time types available. <a href=":link">Add work time type</a>.',
      [':link' => Url::fromRoute('entity.work_time_type.add_form')->toString()],
    );

    return $build;
  }

}
