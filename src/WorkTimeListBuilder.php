<?php

namespace Drupal\work_time;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a list controller for the work time entity type.
 */
class WorkTimeListBuilder extends EntityListBuilder {

  /**
   * Constructs a new WorkTimeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    protected DateFormatterInterface $dateFormatter,
    protected Connection $database,
    protected Request $request,
    protected FormBuilderInterface $formBuilder,
    protected LanguageManagerInterface $languageManager,
  ) {
    parent::__construct($entity_type, $storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('form_builder'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = $this->formBuilder->getForm('Drupal\work_time\Form\WorkTimeFilterForm');
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total work times: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Enable language column and filter if multiple languages are added.
    $header = [
      'label' => [
        'data' => $this->t('Label'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'label',
        'specifier' => 'label',
      ],
      'bundle' => [
        'data' => $this->t('Bundle'),
        'field' => 'bundle',
        'specifier' => 'bundle',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      'created' => [
        'data' => $this->t('Created'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'created',
        'specifier' => 'created',
      ],
      'stopped' => [
        'data' => $this->t('Stop'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'stopped',
        'specifier' => 'stopped',
      ],
      'time_total' => [
        'data' => $this->t('Total'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'time_total',
        'specifier' => 'time_total',
      ],
      'status' => [
        'data' => $this->t('Status'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'status',
        'specifier' => 'status',
      ],
      'uid' => [
        'data' => $this->t('Author'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
        'field' => 'uid',
        'specifier' => 'uid',
      ],
    ];
    if ($this->languageManager->isMultilingual()) {
      $header['language_name'] = [
        'data' => $this->t('Language'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ];
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\work_time\WorkTimeInterface $entity */
    $row['label'] = $entity->toLink();
    $row['bundle'] = $entity->bundle();
    $row['created'] = $this->dateFormatter->format($entity->get('created')->value);
    $row['stopped'] = !empty($entity->get('stopped')->value) ? $this->dateFormatter->format($entity->get('stopped')->value) : $this->t('Undefined');
    $row['time_total'] = !empty($entity->get('time_total')->value) ? $entity->get('time_total')->value : $this->t('Undefined');
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()->accessCheck(TRUE);
    $bundle = $this->request->query->get('bundle');
    if (!empty($bundle)) {
      $query->condition('bundle', $bundle);
    }
    $label = $this->request->query->get('label');
    if (!empty($label)) {
      $query->condition('label', '%' . $this->database->escapeLike($label) . '%', 'LIKE');
    }
    $query->sort($this->entityType->getKey('id'));
    return $query->execute();
  }

}
