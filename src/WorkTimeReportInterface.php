<?php

namespace Drupal\work_time;

/**
 * Provides an interface defining a work time report.
 */
interface WorkTimeReportInterface {

  /**
   * {@inheritdoc}
   */
  public function getDataMonthly($year, $month);

  /**
   * {@inheritdoc}
   */
  public function getUserMonth($year, $month, $uid);

}
