<?php

namespace Drupal\work_time;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class Work Time Monthly.
 *
 * @package Drupal\work_time\Services
 */
class WorkTimeHoliday {
  use StringTranslationTrait;

  /**
   * Constructs a new WorkTimeHoliday.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(protected ConfigFactoryInterface $configFactory, protected ModuleHandlerInterface $moduleHandler, protected LanguageManagerInterface $languageManager) {
  }

  /**
   * {@inheritDoc}
   */
  public function workTimeGetConfig($viewConfig) {
    $generalConfig = $this->configFactory->get('work_time.settings');
    $work_days = $generalConfig->get('work_day') ?? [];
    $coefficient_overtime = $generalConfig->get('coefficient_overtime') ?? NULL;
    $coefficient_holiday = $generalConfig->get('coefficient_holiday') ?? NULL;
    $coefficient_day_off = $generalConfig->get('coefficient_day_off') ?? NULL;
    $coefficient_vacation = $generalConfig->get('coefficient_vacation') ?? NULL;
    $holidays = $generalConfig->get('holidays') ?? NULL;
    $language = $generalConfig->get('language') ?? 'en';

    $view_holidays = [];
    if (array_key_exists('holidays', $viewConfig)) {
      $view_holidays = $this->getHolidays($viewConfig['holidays']);
    }

    $general_holidays = $this->getHolidays($holidays);

    $holidays_array = array_merge($general_holidays, $view_holidays);
    $holidays_result = [];
    foreach (array_unique($holidays_array) as $val) {
      $val = explode('/', $val);
      $val = array_reverse($val);
      $holidays_result[] = implode('-', $val);
    }

    if (array_key_exists('holidays', $viewConfig)) {
      unset($viewConfig['holidays']);
    }

    // Allow modules to alter the mapping array.
    $this->moduleHandler->alter('work_time_holiday_settings', $holidays_result);

    $resultGeneralConfig = [
      'work_day' => $work_days,
      'holidays' => $holidays_result ?? [],
      'hours' => $generalConfig->get('hours'),
      'over_hours' => $generalConfig->get('over_hours'),
      'coefficient_overtime' => $coefficient_overtime,
      'coefficient_holiday' => $coefficient_holiday,
      'coefficient_day_off' => $coefficient_day_off,
      'coefficient_vacation' => $coefficient_vacation,
      'language' => $language,
    ];

    $resultConfig = $viewConfig + $resultGeneralConfig;

    return $resultConfig;
  }

  /**
   * {@inheritDoc}
   *
   * Return array holiday config.
   */
  public function getHolidays($holidays = NULL) {
    $array_holiday = [];
    // Find all date patterns in the input string.
    if (empty($holidays)) {
      $holidays = $this->configFactory->get('work_time.settings')->get('holidays');
    }
    preg_match_all('/\d{2}\/\d{2}(?:\/\d{4})?/', $holidays, $matches);

    // Browse sample dates.
    if ($matches[0]) {
      $array_holiday = array_filter($matches[0], function ($val) {
        if ($this->checkDateFormat($val)) {
          return $val;
        }
      });
    }

    // Extract the date ranges from the input string.
    preg_match_all('/\[(.*?)\]/', $holidays, $date_ranges);

    // Initialize the output array.
    $output_arr = [];

    // Iterate over each date range.
    foreach ($date_ranges[1] as $date_range) {
      $dates = preg_split('/[, -]+/', $date_range);
      $output_arr[] = $dates;
    }

    if (is_array($output_arr) && $output_arr) {
      foreach ($output_arr as $range) {
        if (is_array($range) && count($range) > 1) {
          $start_check = $this->checkDateFormat($range[0]);
          $end_check = $this->checkDateFormat($range[count($range) - 1]);
          if ($start_check && $end_check) {
            if ($start_check['format'] == 'd/m' || $end_check['format'] == 'd/m') {
              continue;
            }
            $start = DrupalDateTime::createFromFormat('d/m/Y', $range[0]);
            $end = DrupalDateTime::createFromFormat('d/m/Y', $range[count($range) - 1]);
            if ($start->getTimestamp() < $end->getTimestamp()) {
              while ($start->getTimestamp() <= $end->getTimestamp()) {
                $array_holiday[] = $start->format('d/m/Y');
                $start->modify('+1 day');
              }
            }
          }
          else {
            continue;
          }
        }
      }
    }

    return $array_holiday;
  }

  /**
   * Return array weekend config.
   *
   * {@inheritDoc}
   */
  public function getWorkDay() {
    $generalConfig = $this->configFactory->get('work_time.settings');
    return $generalConfig->get('work_day') ?? [];
  }

  /**
   * Check date string match format 'd/m/Y' or d/m.
   *
   * {@inheritDoc}
   */
  public function checkDateFormat($str) {
    $pattern1 = "/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(20|21)\d\d$/";
    $pattern2 = "/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])$/";
    if (preg_match($pattern1, $str) || preg_match($pattern2, $str)) {
      $str_convert = explode('/', $str);
      switch (count($str_convert)) {
        case 2:
          $format = 'd/m';
          $current_date = new DrupalDateTime();
          $str_date = explode('/', $str);
          $str_date[] = $current_date->format('Y');
          $str_date = implode('/', $str_date);
          break;

        case 3:
          $format = 'd/m/Y';
          $str_date = $str;
          break;

        default:
          return FALSE;
      }
      return [
        'date' => $str_date,
        'format' => $format,
      ];
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function listCodeCurrency(): array {
    return [
      'ALL' => $this->t('Albania Lek'),
      'AFN' => $this->t('Afghanistan Afghani'),
      'ARS' => $this->t('Argentina Peso'),
      'AWG' => $this->t('Aruba Guilder'),
      'AUD' => $this->t('Australia Dollar'),
      'AZN' => $this->t('Azerbaijan New Manat'),
      'BSD' => $this->t('Bahamas Dollar'),
      'BBD' => $this->t('Barbados Dollar'),
      'BDT' => $this->t('Bangladeshi taka'),
      'BYR' => $this->t('Belarus Ruble'),
      'BZD' => $this->t('Belize Dollar'),
      'BMD' => $this->t('Bermuda Dollar'),
      'BOB' => $this->t('Bolivia Boliviano'),
      'BAM' => $this->t('Bosnia and Herzegovina Convertible Marka'),
      'BWP' => $this->t('Botswana Pula'),
      'BGN' => $this->t('Bulgaria Lev'),
      'BRL' => $this->t('Brazil Real'),
      'BND' => $this->t('Brunei Darussalam Dollar'),
      'KHR' => $this->t('Cambodia Riel'),
      'CAD' => $this->t('Canada Dollar'),
      'KYD' => $this->t('Cayman Islands Dollar'),
      'CLP' => $this->t('Chile Peso'),
      'CNY' => $this->t('China Yuan Renminbi'),
      'COP' => $this->t('Colombia Peso'),
      'CRC' => $this->t('Costa Rica Colon'),
      'HRK' => $this->t('Croatia Kuna'),
      'CUP' => $this->t('Cuba Peso'),
      'CZK' => $this->t('Czech Republic Koruna'),
      'DKK' => $this->t('Denmark Krone'),
      'DOP' => $this->t('Dominican Republic Peso'),
      'XCD' => $this->t('East Caribbean Dollar'),
      'EGP' => $this->t('Egypt Pound'),
      'SVC' => $this->t('El Salvador Colon'),
      'EEK' => $this->t('Estonia Kroon'),
      'EUR' => $this->t('Euro Member Countries'),
      'FKP' => $this->t('Falkland Islands (Malvinas) Pound'),
      'FJD' => $this->t('Fiji Dollar'),
      'GHC' => $this->t('Ghana Cedis'),
      'GIP' => $this->t('Gibraltar Pound'),
      'GTQ' => $this->t('Guatemala Quetzal'),
      'GGP' => $this->t('Guernsey Pound'),
      'GYD' => $this->t('Guyana Dollar'),
      'HNL' => $this->t('Honduras Lempira'),
      'HKD' => $this->t('Hong Kong Dollar'),
      'HUF' => $this->t('Hungary Forint'),
      'ISK' => $this->t('Iceland Krona'),
      'INR' => $this->t('India Rupee'),
      'IDR' => $this->t('Indonesia Rupiah'),
      'IRR' => $this->t('Iran Rial'),
      'IMP' => $this->t('Isle of Man Pound'),
      'ILS' => $this->t('Israel Shekel'),
      'JMD' => $this->t('Jamaica Dollar'),
      'JPY' => $this->t('Japan Yen'),
      'JEP' => $this->t('Jersey Pound'),
      'KZT' => $this->t('Kazakhstan Tenge'),
      'KPW' => $this->t('Korea (North) Won'),
      'KRW' => $this->t('Korea (South) Won'),
      'KGS' => $this->t('Kyrgyzstan Som'),
      'LAK' => $this->t('Laos Kip'),
      'LVL' => $this->t('Latvia Lat'),
      'LBP' => $this->t('Lebanon Pound'),
      'LRD' => $this->t('Liberia Dollar'),
      'LTL' => $this->t('Lithuania Litas'),
      'MKD' => $this->t('Macedonia Denar'),
      'MYR' => $this->t('Malaysia Ringgit'),
      'MUR' => $this->t('Mauritius Rupee'),
      'MXN' => $this->t('Mexico Peso'),
      'MNT' => $this->t('Mongolia Tughrik'),
      'MZN' => $this->t('Mozambique Metical'),
      'NAD' => $this->t('Namibia Dollar'),
      'NPR' => $this->t('Nepal Rupee'),
      'ANG' => $this->t('Netherlands Antilles Guilder'),
      'NZD' => $this->t('New Zealand Dollar'),
      'NIO' => $this->t('Nicaragua Cordoba'),
      'NGN' => $this->t('Nigeria Naira'),
      'NOK' => $this->t('Norway Krone'),
      'OMR' => $this->t('Oman Rial'),
      'PKR' => $this->t('Pakistan Rupee'),
      'PAB' => $this->t('Panama Balboa'),
      'PYG' => $this->t('Paraguay Guarani'),
      'PEN' => $this->t('Peru Nuevo Sol'),
      'PHP' => $this->t('Philippines Peso'),
      'PLN' => $this->t('Poland Zloty'),
      'QAR' => $this->t('Qatar Riyal'),
      'RON' => $this->t('Romania New Leu'),
      'RUB' => $this->t('Russia Ruble'),
      'SHP' => $this->t('Saint Helena Pound'),
      'SAR' => $this->t('Saudi Arabia Riyal'),
      'RSD' => $this->t('Serbia Dinar'),
      'SCR' => $this->t('Seychelles Rupee'),
      'SGD' => $this->t('Singapore Dollar'),
      'SBD' => $this->t('Solomon Islands Dollar'),
      'SOS' => $this->t('Somalia Shilling'),
      'ZAR' => $this->t('South Africa Rand'),
      'LKR' => $this->t('Sri Lanka Rupee'),
      'SEK' => $this->t('Sweden Krona'),
      'CHF' => $this->t('Switzerland Franc'),
      'SRD' => $this->t('Suriname Dollar'),
      'SYP' => $this->t('Syria Pound'),
      'TWD' => $this->t('Taiwan New Dollar'),
      'THB' => $this->t('Thailand Baht'),
      'TTD' => $this->t('Trinidad and Tobago Dollar'),
      'TRY' => $this->t('Turkey Lira'),
      'TRL' => $this->t('Turkey Lira'),
      'TVD' => $this->t('Tuvalu Dollar'),
      'UAH' => $this->t('Ukraine Hryvna'),
      'GBP' => $this->t('United Kingdom Pound'),
      'USD' => $this->t('United States Dollar'),
      'UYU' => $this->t('Uruguay Peso'),
      'UZS' => $this->t('Uzbekistan Som'),
      'VEF' => $this->t('Venezuela Bolivar'),
      'VND' => $this->t('Viet Nam Dong'),
      'YER' => $this->t('Yemen Rial'),
      'ZWD' => $this->t('Zimbabwe Dollar'),
    ];
  }

}
