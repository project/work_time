(function (Drupal, $, once) {
  'use strict';
  // create visualization
  let container = document.getElementById('fingerprint-timeline');
  let options = {
    groupOrder: 'content',
    verticalScroll: true,
    showWeekScale: true,
    zoomKey: 'ctrlKey',
    editable: true,
    stack: true,
    tooltip: {
      template: function(originalItemData, parsedItemData) {
        return `<span>${originalItemData.title}</span>`;
      }
    },
    showCurrentTime: true,
    locale: 'sq',
    locales: {
      tr: {
        current: 'geçerli',
        time: 'kere',
      },
      sq: {
        current: 'ընթացիկ',
        time: 'ժամ',
      },
      ar: {
        current: 'حالي',
        time: 'وقت',
      }
    }
  };

  // Create an empty DataSet.
  // This DataSet is used for two-way data binding with the Timeline.
  let items = new vis.DataSet();
  let groups = new vis.DataSet();
  let timeline = new vis.Timeline(container, items, groups, options);

  const href = $("#fingerprint-timeline").data('url');
  let date = $('.edit-date').val();
  $.get(href + '/' + date, function (data) {
    loadData(data);
  });

  function loadData(data) {
    items.clear();
    groups.clear();
    if (data && data.groups.length) {
      groups.add(data.groups);
    }
    if (data && data.items.length) {
      items.add(data.items);
      timeline.fit();
    }
  }
    Drupal.behaviors.timeline = {
    attach: function (context, settings) {
      $(once('type-filter', '.type-timeline')).on('change', function () {
        let type = $(this).val();
        $('.date-timeline').attr('type', type);
      });
      $(once('month-timeline', '.date-timeline')).on('change', function () {
        let date = $(this).val();
        $.get(href + '/' + date, function (data) {
          loadData(data);
        });
      });

    }
  };
}(Drupal, jQuery, once));
