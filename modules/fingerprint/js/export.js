/**
 * @file
 * Attaches the behaviors for the SN QUICK Add field data module.
 */

(function (Drupal, $, once) {
  'use strict';

  Drupal.behaviors.fingerprint_export = {
    attach: function (context, settings) {
      $(once('fingerprint-export', "#export", context)).click(function (event) {
        let separator = $('#edit-csv-delimiter').val();
        if(separator == 'tab'){
          separator = "\t";
        }
        let outputCSV = $(this).parent().find('#export-header').text();
        let columns = outputCSV.split(",");
        if(columns.length == 1){
          columns = outputCSV.split(";");
          if(columns.length == 1){
            columns = outputCSV.split("\t");
          }
        }
        outputCSV = columns.join(separator);
        const universalBOM = "\uFEFF";
        let blobby = 'data:application/csv;charset=utf-8,' + encodeURIComponent(universalBOM + outputCSV);
        $(this).attr({
          'href': blobby,
          'target': '_blank'
        });
      });
    }
  };
}(Drupal, jQuery, once));
