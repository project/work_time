(function (Drupal, $, once) {
  'use strict';

  Drupal.behaviors.fingerprint_timeline = {
    attach: function (context, settings) {
      $(once('fingerprint_timeline', '#work-time-timeline', context)).each(function () {
        // Function
        // return format date, example: "%Y-%m-%d"
        const getFormatDate = (date, format = "%Y-%m-%d") => {
          let year = date.getFullYear();
          let month = (date.getMonth() + 1).toString().padStart(2, '0');
          let day = date.getDate().toString().padStart(2, '0');
          let hours = date.getHours().toString().padStart(2, '0');
          let minutes = date.getMinutes().toString().padStart(2, '0');
          let seconds = date.getSeconds().toString().padStart(2, '0');
          let results = format;
          results = results.replaceAll('%Y', year).replaceAll('%m', month).replaceAll('%d', day);
          results = results.replaceAll('%H', hours).replaceAll('%i', minutes).replaceAll('%s', seconds);
          return results;
        }

        const processTimeline = (data) => {
          let results = {};
          let session = null;
          for (let i = 0; i < data.length; i++) {
            let timePoint = new Date(data[i].start);
            let timePointNext = data?.[i + 1]?.start ? new Date(data[i + 1].start) : null;

            let date_str = getFormatDate(timePoint);
            if (session) {
              date_str = getFormatDate(session['start']);
            }

            if (!session) {
              const hours8 = 28800000; // 8 hours;
              let timePointEnd = new Date(data[i].start + hours8);
              const hours16 = 57600000; // 16 hours;
              let timePointEndMax = new Date(data[i].start + hours16);

              const minutes30 = 1800000; // 30 minutes;
              let margin_after_start = new Date(data[i].start + minutes30);

              let margin_before_end = new Date(timePointEnd.valueOf() - minutes30);
              let margin_after_end = new Date(timePointEnd.valueOf() + minutes30);

              let margin_before_end_max = new Date(timePointEndMax.valueOf() - minutes30);
              let margin_after_end_max = new Date(timePointEndMax.valueOf() + minutes30);

              session = {
                'start': timePoint,
                'end': timePointEnd,
                'end_max': timePointEndMax,
                'margin_after_start': margin_after_start,
                'margin_before_end': margin_before_end,
                'margin_after_end': margin_after_end,
                'margin_before_end_max': margin_before_end_max,
                'margin_after_end_max': margin_after_end_max,
              };
            }

            if (session) {
              if (!results?.[date_str]?.['checkin']) {
                if (!results?.[date_str]) {
                  results[date_str] = {};
                }
                results[date_str]['checkin'] = [];
              }
              if (timePoint.valueOf() <= session['margin_after_start'].valueOf()) {
                results[date_str]['checkin'].push(timePoint);
              }

              if (!results?.[date_str]?.['break']) {
                if (!results?.[date_str]) {
                  results[date_str] = {};
                }
                results[date_str]['break'] = [];
              }
              if (session['margin_after_start'].valueOf() < timePoint.valueOf() && timePoint.valueOf() < session['margin_before_end'].valueOf()) {
                results[date_str]['break'].push(timePoint);
              }

              if (!results?.[date_str]?.['checkout']) {
                if (!results?.[date_str]) {
                  results[date_str] = {};
                }
                results[date_str]['checkout'] = [];
              }
              if (session['margin_before_end'].valueOf() <= timePoint.valueOf() && timePoint.valueOf() <= session['margin_after_end_max'].valueOf()) {
                results[date_str]['checkout'].push(timePoint);
              }

              if (!timePointNext || timePointNext.valueOf() > session['margin_after_end_max'].valueOf()) {
                session = null;
                if (!results[date_str]['checkout'].length && results[date_str]['break'].length) {
                  results[date_str]['checkout'].push(results[date_str]['break'].pop());
                }
              }
            }
          }
          return results;
        }

        const calculatorTimeline = (data, groups) => {
          data.sort((a, b) => a.start - b.start);
          let results = {};
          let dataTable = [];
          let dataWokTime = [];
          if (groups.length) {
            for (let i = 0; i < groups.length; i++) {
              let data_process = data.filter(item => item.group == groups[i].id)
              if (data_process.length) {
                results[groups[i].id] = processTimeline(data_process)
                dataTable = dataTable.concat(convertTable(results[groups[i].id], groups[i]))
                dataWokTime = dataWokTime.concat(convertWorkTime(results[groups[i].id], groups[i]))
              }
            }
          }
          return {'process': results, 'data_table': dataTable, 'data_work_time': dataWokTime};
        }

        const updateWorkTime = (data) => {
          $('#work-time-data').val(JSON.stringify(data))
        }

        const updateTable = (data) => {
          data.change.forEach(item => {
            table.bootstrapTable('updateRow', {
              index: item.index,
              row: item.row
            })
          })
          data.add.forEach(item => {
            table.bootstrapTable('insertRow', {
              index: item.index,
              row: item.row
            })
          })
        }

        const getChangeTable = (data) => {
          let results = {
            'add': [],
            'change': [],
          };
          let data_bootstrap = $('#timeline-table').bootstrapTable("getData");
          if (data.length) {
            data.forEach((item, index) => {
              let find_row = data_bootstrap.find(row => row.uid == item.uid && row.date == item.date);
              if (!find_row) {
                results.add.push({
                  'index': index,
                  'row': item,
                })
              }
              else {
                results.change.push({
                  'index': index,
                  'row': item,
                })
              }
            })
          }
          return results;
        }

        const convertTable = (data, group) => {
          let results = [];
          let holidays = drupalSettings['work-time']['holidays'];
          let work_day = drupalSettings['work-time']['holidays'];;
          for (const str_date in data) {
            let space = [];
            if (data[str_date]['break'].length) {
              space.push(getFormatDate(data[str_date]['break'][0], '%H:%i'));
              if (data[str_date]['break'].length > 1) {
                space.push(getFormatDate(data[str_date]['break'][data[str_date]['break'].length - 1], '%H:%i'));
              }
            }
            space = space.length ? space.join('-') : null;

            // total, overtime.
            let total = [];
            let overtime = [];
            let checkin = data[str_date]['checkin'].length ? data[str_date]['checkin'][0] : null;
            let checkout = data[str_date]['checkout'].length ? data[str_date]['checkout'][data[str_date]['checkout'].length - 1] : null;

            if (checkin && checkout) {
              let total_time = checkout.valueOf() - checkin.valueOf();
              total_time /= 1000;
              let total_hours = Math.floor(total_time / 3600);
              let total_minutes = Math.floor(total_time % 3600 / 60);
              if (total_hours) {
                total.push(Drupal.t('@hourshours', {'@hours': total_hours}))
              }
              if (total_minutes) {
                total.push(Drupal.t('@minutesminutes', {'@minutes': total_minutes}))
              }

              if (total_time > 8 * 3600) {
                let overtime_time = total_time - 8 * 3600;
                if (overtime_time >= 3600) {
                  let overtime_hours = Math.floor(overtime_time / 3600);
                  let overtime_minutes = Math.floor(overtime_time % 3600 / 60);
                  if (overtime_hours) {
                    overtime.push(Drupal.t('@hourshours', {'@hours': overtime_hours}))
                  }
                  if (overtime_minutes) {
                    overtime.push(Drupal.t('@minutesminutes', {'@minutes': overtime_minutes}))
                  }
                }
              }
            }

            let date = new Date(str_date);
            // Weekend.
            let day_week = date.getDay();
            let weekend = null;
            if (day_week in work_day) {
              weekend = 1;
            }

            // Holiday.
            let day_format_full = getFormatDate(date, '%d/%m/%Y');
            let day_format_short = getFormatDate(date, '%d/%m');
            let holiday = null;
            if (day_format_full in holidays || day_format_short in holidays) {
              holiday = 1;
            }

            let checkin_str = checkin ? getFormatDate(checkin, '%d/%m/%Y %H:%i') : null;
            let checkout_str = checkout ? getFormatDate(checkout, '%d/%m/%Y %H:%i') : null;
            results.push({
              'uid': group.id,
              'label': group.content,
              'date': day_format_full,
              'checkin': checkin_str,
              'checkout': checkout_str || '',
              'lunch': space || '',
              'total': Array.isArray(total) ? total.join(', ') : total,
              'overtime': Array.isArray(overtime) ? overtime.join(', ') : overtime,
              'weekend': weekend || '',
              'holiday': holiday || '',
              'note': '',
            });
          }
          return results;
        }

        const convertWorkTime = (data, group) => {
          let results = [];
          const holidays = [];
          const work_day = [0];
          for (const str_date in data) {
            let date = new Date(str_date);
            // Weekend.
            let day_week = date.getDay();
            let weekend = null;
            if (day_week in work_day) {
              weekend = 3;
            }

            // Holiday.
            let day_format_full = getFormatDate(date, '%d/%m/%Y');
            let day_format_short = getFormatDate(date, '%d/%m');
            let holiday = null;
            if (day_format_full in holidays || day_format_short in holidays) {
              holiday = 2;
            }

            let start = data[str_date]['checkin'].length ? data[str_date]['checkin'][0] : null;
            let end = data[str_date]['checkout'].length ? data[str_date]['checkout'][data[str_date]['checkout'].length - 1] : null;

            let data_overtime = null;

            let created = start.valueOf();
            let stopped = end ? end.valueOf() : null;
            created /= 1000;
            stopped = stopped ? stopped / 1000 : null;
            if (stopped && stopped - created > 8 * 60 * 60) {
              if (stopped - (created + 8 * 60 * 60) >= 3600) {
                let overtime = Math.round((stopped - (created + (8 * 60 * 60))) / 3600);
                let created_overtime = created + 8 * 60 * 60;
                let stopped_overtime = created_overtime + overtime * 3600;
                data_overtime = {
                  'uid': group.id,
                  'label': group.content,
                  'created': created_overtime,
                  'stopped': stopped_overtime,
                  'time_total': overtime * 3600,
                  'status': 0,
                  'payroll': holiday || weekend || 0,
                }

                stopped = created_overtime;
              }
            }

            let time_total = stopped ? stopped - created : null;
            results.push({
              'uid': group.id,
              'label': group.content,
              'created': created,
              'stopped': stopped|| null,
              'time_total': time_total,
              'status': 1,
              'payroll': holiday || weekend || 1,
            })
            if (data_overtime) {
              results.push(data_overtime);
            }
          }
          return results;
        }

        // create visualization
        let options = {
          groupOrder: 'content',
          verticalScroll: true,
          showWeekScale: false,
          editable: {
            add: true,         // add new items by double tapping
            updateTime: true,  // drag items horizontally
            updateGroup: true, // drag items from one group to another
            remove: true,       // delete an item by tapping the delete button top right
            overrideItems: false  // allow these options to override item.editable
          },
          limitSize: false,
          stack: false,
          type: 'box',
          tooltip: {
            template: function (originalItemData, parsedItemData) {
              let tooltip = null;
              if (originalItemData?.title) {
                tooltip = `<span>${originalItemData.title}</span>`
              }
              return tooltip;
            }
          },
          showCurrentTime: true,
          locale: navigator.language || 'en',
          maxHeight: '500px',
          groupHeightMode: 'fixed',
          orientation: 'both',
          horizontalScroll: true,
          zoomMin: 1000 * 60 * 60 * 6,
          zoomMax: 1000 * 60 * 60 * 24 * 31 * 3,
          onAdd: async function (item, callback) {
            if (item.end) {
              callback(null); // cancel updating the item
              return;
            }
            item.start = item.start.valueOf();
            item.content = item.content = getFormatDate(new Date(item.start), '%H:%i')
            callback(item) // send back adjusted item
            let dataHandle = calculatorTimeline(items.get(), groups.get());
            updateWorkTime(dataHandle['data_work_time'])
            updateTable(getChangeTable(dataHandle['data_table']))
          },

          onMove: function (item, callback) {
            item.start = item.start.valueOf();
            item.content = item.content = getFormatDate(new Date(item.start), '%H:%i')
            callback(item) // send back adjusted item
            let dataHandle = calculatorTimeline(items.get(), groups.get());
            updateWorkTime(dataHandle['data_work_time'])
            updateTable(getChangeTable(dataHandle['data_table']))
          },

          onMoving: function (item, callback) {
            if (item.end) {
              callback(null); // send back the (possibly) changed item
              return;
            }
            item.content = item.content = getFormatDate(new Date(item.start), '%H:%i')
            callback(item);
          },

          onUpdate: function (item, callback) {
            callback(null); // cancel updating the item
          },

          onRemove: function (item, callback) {
            callback(item) // send back adjusted item
            let dataHandle = calculatorTimeline(items.get(), groups.get());
            updateWorkTime(dataHandle['data_work_time'])
            updateTable(getChangeTable(dataHandle['data_table']))
          }
        };
        // Create table.
        let data = drupalSettings.data_timeline.data;
        let group = drupalSettings.data_timeline.group;
        let dataHandle = calculatorTimeline(data, group)
        let table = $('#timeline-table');
        table.bootstrapTable({
          search: true,
          showRefresh: true,
          showToggle: true,
          showFullscreen: true,
          clickToSelect: true,
          showColumnsToggleAll: true,
          showColumns: true,
          exportDataType: 'all',
          showExport: true,
          exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
          pagination: true,
          data: dataHandle['data_table']
        })

        // Create an empty DataSet.
        // This DataSet is used for two-way data binding with the Timeline.
        let container = document.getElementById('work-time-timeline');
        let items = new vis.DataSet(data);
        let groups = new vis.DataSet(group);
        let timeline = new vis.Timeline(container, items, groups, options);
      })
    }
  };
}(Drupal, jQuery, once));
