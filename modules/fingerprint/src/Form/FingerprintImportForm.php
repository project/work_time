<?php

namespace Drupal\fingerprint\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fingerprint\ProcessTimeService;
use Drupal\work_time\WorkTimeHoliday;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides fingerprint import attendance.
 */
class FingerprintImportForm extends FormBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected $delimiter = ',';

  /**
   * {@inheritdoc}
   */
  protected $formatDay = 'd/m/Y';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
    protected ProcessTimeService $fingerprintProcess,
    protected WorkTimeHoliday $workTimeHoliday,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('fingerprint.process_services'),
      $container->get('work_time.holiday')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fingerprint_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($form_state->has('page') && $form_state->get('page') == 2) {
      return self::formPageNext($form, $form_state);
    }

    $filename = 'fingerprint-import.csv';
    $options_fields = [
      'uid' => $this->t("User id"),
      'label' => $this->t("Name"),
      'created' => $this->t("Time"),
    ];
    $separate = ',';
    $form['files']['csv_file_upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Choose a file'),
      '#description' => implode('<br>', [
        $this->t('<a class="btn btn-link icon-link icon-link-hover" href="/modules/contrib/work_time/modules/fingerprint/DataDemoImport/fingerprint brut data from machine.csv"><i class="bi bi-fingerprint"></i> Download example file</a>'),
        $this->t('<a class="btn btn-link icon-link icon-link-hover button button--small" id="export" data-filename="@filename" download="@filename"> <i class="bi bi-filetype-csv"></i> Download CSV template: </a> <span id="export-header" >@template</span>', [
          '@filename' => $filename,
          '@template' => implode($separate, $options_fields),
        ]),
      ]),
      '#required' => TRUE,
      '#upload_validators' => [
        'FileExtension' => ['extensions' => 'csv'],
      ],
    ];
    $form['files']['csv_delimiter'] = [
      '#type' => 'select',
      '#title' => $this->t('Character used to separate'),
      '#default_value' => $separate,
      '#options' => [
        ',' => ',',
        ';' => ';',
        'tab' => 'Tab',
      ],
    ];
    array_unshift($options_fields, $this->t('- Select -'));
    if ($fid = $form_state->getValue('csv_file_upload')) {
      $csv_rows = $this->convertCsvArray($fid, 10);
      $header_title = !empty($csv_rows) ? array_shift($csv_rows) : [$this->t('Empty')];
      $header = [];
      $field = [
        '#type' => 'select',
        '#options' => $options_fields,
        '#empty_option' => $this->t('- Select -'),
      ];
      foreach ($header_title as $col => $title_col) {
        $title_col = trim($title_col);
        $header[$col]["data"] = $field;
        $header[$col]["data"]["#title"] = $title_col;
        $header[$col]["data"]["#name"] = 'col[' . $col . ']';
        foreach ($options_fields as $val => $name) {
          if ($title_col == (string) $name) {
            $header[$col]["data"]["#value"] = $val;
          }
        }
      }

      $form['files']['csv_file_upload']['#description'] = [
        '#type' => 'details',
        '#title' => $this->t('Preview'),
        '#open' => TRUE,
        'preview' => [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => !empty($csv_rows) ? $csv_rows : [],
          '#caption' => $this->t('Preview attendance'),
          '#attributes' => ['class' => ['admin-dblog']],
        ],
      ];
    }
    $form['files']['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Select format date'),
      '#default_value' => 'd/m/Y',
      '#options' => [
        'd/m/Y' => implode('/', [
          $this->t('Day'),
          $this->t('Month'),
          $this->t('Year'),
        ]),
        'm/d/Y' => implode('/', [
          $this->t('Month'),
          $this->t('Day'),
          $this->t('Year'),
        ]),
      ],
    ];

    $form['#attached'] = [
      'library' => ['fingerprint/export'],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'success',
      '#value' => $this->t('Next'),
      '#submit' => ['::nextCsvSubmit'],
    ];
    return $form;
  }

  /**
   * Step 2 form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function formPageNext(array &$form, FormStateInterface $form_state) {
    $col = array_flip($form_state->getUserInput()['col']);
    $data = $this->calculateCsv($form_state->getValue("csv_file_upload"), $col);
    $form['#attached']['drupalSettings']['data_timeline'] = $data['data_timeline'];
    $header = [
      ['data' => $this->t('uid'), 'data-field' => 'uid'],
      ['data' => $this->t('Name'), 'data-field' => 'label'],
      ['data' => $this->t('Day'), 'data-field' => 'date'],
      ['data' => $this->t('Check in'), 'data-field' => 'checkin'],
      ['data' => $this->t('Check out'), 'data-field' => 'checkout'],
      ['data' => $this->t('Time break'), 'data-field' => 'lunch'],
      ['data' => $this->t('Total'), 'data-field' => 'total'],
      ['data' => $this->t('Overtime'), 'data-field' => 'overtime'],
      ['data' => $this->t('Weekend'), 'data-field' => 'weekend'],
      ['data' => $this->t('Holiday'), 'data-field' => 'holiday'],
      ['data' => $this->t('Note'), 'data-field' => 'note'],
    ];
    $form['data_table'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'id' => 'work-time-data',
        'class' => ['js-hide'],
      ],
      '#default_value' => json_encode($data['data_work_time']),
    ];
    $form['table'] = [
      '#type' => 'table',
      '#caption' => $this->t('Attendance calculate'),
      '#title' => $this->t('Attendance'),
      '#header' => $header,
      '#rows' => $data['data_table'],
      '#attributes' => [
        'id' => "timeline-table",
      ],
    ];
    $form['timeline'] = [
      '#type' => 'container',
      '#title' => $this->t('Visualize'),
      '#attributes' => [
        'id' => 'work-time-timeline',
        'class' => ['timeline'],
      ],
    ];
    $form['#attached']['library'][] = 'work_time/bootstrapTable';
    $form['#attached']['library'][] = 'fingerprint/visualize_timeline';
    $form['#attached']['drupalSettings']['work-time'] = [
      'holidays' => $this->workTimeHoliday->getHolidays(),
      'work_day' => $this->workTimeHoliday->getWorkDay(),
    ];
    $form['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      '#submit' => ['::pageBack'],
      '#limit_validation_errors' => [],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * Button submit back.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function pageBack(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('col', $form_state->get('col'))
      ->set('page', 1)
      ->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('csv_file_upload'))) {
      $csv_rows = $this->convertCsvArray($form_state->getValue("csv_file_upload"), 5);
      if (count($csv_rows) < 1) {
        $form_state->setErrorByName('csv_file_upload', $this->t('The list should be at least 1 line.'));
      }
      if (count($csv_rows[0]) < 2 && $csv_rows[0][0] != 'Email' && $csv_rows[0][1] != 'First name' && $csv_rows[0][0] != 'Last name') {
        $form_state->setErrorByName('csv_file_upload', $this->t('The file csv is not compatible.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function nextCsvSubmit(array &$form, FormStateInterface $form_state) {
    $this->formatDay = $form_state->getValue('date_format');
    $form_state->set('page', 2)->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = $form_state->getValue('data_table');
    $data = json_decode($data, TRUE);
    $operations = [];
    foreach ($data as $item) {
      $operations[] = [
        '\Drupal\fingerprint\CreateWorkTimeBatch::createWorkTime',
        [$item],
      ];
    }
    $batch = [
      'title' => $this->t('Creating work time...'),
      'operations' => $operations,
      'finished' => '\Drupal\fingerprint\CreateWorkTimeBatch::createWorkTimeFinishedCallback',
    ];

    batch_set($batch);
    $this->messenger()
      ->addStatus($this->t('The fingerprint has been imported.'));
    $form_state->setRedirect('fingerprint.import');
  }

  /**
   * {@inheritDoc}
   */
  protected function getFile($fid) {
    if (is_array($fid)) {
      $fid = end($fid);
    }
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return FALSE;
    }
    return $file->get('uri')->value;
  }

  /**
   * {@inheritDoc}
   */
  protected function getDelimiter($row, &$delimiter = ',') {
    $line = explode($this->delimiter, $row);
    if (count($line) <= 1) {
      $delimiters = [',', ';', "\t"];
      foreach ($delimiters as $delimiter) {
        $line = explode($delimiter, $row);
        if (count($line) > 1) {
          break;
        }
      }
    }
    return $line;
  }

  /**
   * {@inheritDoc}
   */
  protected function convertTimestamp($date) {
    $date = str_replace(['/', '.'], '-', $date);
    // Convert format US month day Year.
    if ($this->formatDay == 'm/d/Y') {
      $extract = explode('-', $date);
      $temp = $extract[0];
      $extract[0] = $extract[1];
      $extract[1] = $temp;
      $date = implode('-', $extract);
    }
    return strtotime($date);
  }

  /**
   * {@inheritDoc}
   */
  public function calculateCsv($fid, $col) {
    $filename = $this->getFile($fid);
    if (($f = fopen($filename, "r")) !== FALSE) {
      $line = trim(fgets($f));
      $delimiter = $this->delimiter;
      $this->getDelimiter($line, $delimiter);
      $label = $lines = [];
      $rows = $data_json = $data_timeline = [];
      while (($data = fgetcsv($f, 0, $delimiter)) !== FALSE) {
        $uid = $data[$col['uid']] ?? '';
        $date = $data[$col['created']] ?? '';
        if (empty($uid) || empty($date)) {
          continue;
        }
        $label[$uid] = $data[$col['label']] ?? '';
        $date = $this->convertTimestamp($date);
        $lines[$uid][] = $date;
      }
      fclose($f);
      if (!empty($lines)) {
        foreach ($lines as $uid => $attendance) {
          $data_process = $this->fingerprintProcess->processTimeRecords($attendance);
          $data_work_time = $this->fingerprintProcess->convertDataWorkTime($data_process);
          $data_table = $this->fingerprintProcess->convertDataTable($data_process);
          foreach ($data_work_time as &$process) {
            $process += [
              'uid' => $uid,
              'label' => $label[$uid],
            ];
            $data_json[] = $process;
          }
          foreach ($data_table as $process) {
            $rows[] = [
              'uid' => $uid,
              'label' => $label[$uid],
              'date' => $process['date'],
              'checkin' => $process['checkin'],
              'checkout' => $process['checkout'],
              'lunch' => $process['lunch'],
              'total' => $process['total'],
              'overtime' => $process['overtime'],
              'weekend' => $process['weekend'],
              'holiday' => $process['holiday'],
              'history' => $process['history'],
            ];
          }
          // Data timeline.
          $data_timeline['group'][$uid] = [
            'id' => $uid,
            'content' => $label[$uid],
          ];
          if (count($attendance)) {
            foreach ($attendance as $delta => $time) {
              $data_timeline['data'][] = [
                'id' => "$uid-$delta",
                'content' => date('H:i', $time),
                'title' => date('H:i', $time),
                'start' => $time * 1000,
                'type' => 'box',
                'group' => $uid,
              ];
            }
          }
        }
      }
      if (!empty($data_timeline) && !empty($data_timeline['group'])) {
        $data_timeline['group'] = array_values($data_timeline['group']);
      }
      return [
        'data_table' => $rows,
        'data_work_time' => $data_json,
        'data_timeline' => $data_timeline,
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function convertCsvArray($fid, $limit = FALSE) {
    $filename = $this->getFile($fid);
    if (($f = fopen($filename, "r")) !== FALSE) {
      $line = trim(fgets($f));
      $delimiter = $this->delimiter;
      $csv_rows = [$this->getDelimiter($line, $delimiter)];
      $i = 0;
      while (($data = fgetcsv($f, 0, $delimiter)) !== FALSE) {
        $csv_rows[] = $data;
        if ($limit && $i++ > $limit) {
          break;
        }
      }
      fclose($f);
      return $csv_rows;
    }
    return FALSE;
  }

}
