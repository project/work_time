<?php

namespace Drupal\fingerprint\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the work time fingerprint entity edit forms.
 */
class FingerprintForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New work time fingerprint %label has been created.', $message_arguments));
        $this->logger('fingerprint')->notice('Created new work time fingerprint %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The work time fingerprint %label has been updated.', $message_arguments));
        $this->logger('fingerprint')->notice('Updated work time fingerprint %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.fingerprint.canonical', ['fingerprint' => $entity->id()]);

    return $result;
  }

}
