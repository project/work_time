<?php

namespace Drupal\fingerprint;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a work time fingerprint entity type.
 */
interface FingerprintInterface extends ContentEntityInterface, EntityOwnerInterface {

}
