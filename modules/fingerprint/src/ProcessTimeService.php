<?php

namespace Drupal\fingerprint;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\work_time\WorkTimeHoliday;

/**
 * Class Calculate Service Fingerprint.
 *
 * @package Drupal\fingerprint\Services
 */
class ProcessTimeService {

  use StringTranslationTrait;

  /**
   * Constructs a new GroupAdminRouteSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\work_time\WorkTimeHoliday $workTimeHoliday
   *   Work time holiday service.
   */
  public function __construct(protected ConfigFactoryInterface $configFactory, protected WorkTimeHoliday $workTimeHoliday) {
  }

  /**
   * Process time records by user from fingerprint machine.
   *
   * {@inheritdoc}
   */
  public function processTimeRecords($timeRecords) {
    $data = [];
    sort($timeRecords);
    $session = [];
    if ($timeRecords) {
      foreach ($timeRecords as $delta => $point) {
        $timePoint = DrupalDateTime::createFromTimestamp($point);
        $timePointNext = !empty($timeRecords[$delta + 1]) ? DrupalDateTime::createFromTimestamp($timeRecords[$delta + 1]) : NULL;

        $date_str = $timePoint->format('Y-m-d');
        if (!empty($session)) {
          $date_str = $session['start']->format('Y-m-d');
        }

        if (empty($session)) {
          $timePointEnd = DrupalDateTime::createFromTimestamp($point);
          $timePointEndMax = DrupalDateTime::createFromTimestamp($point);
          $timePointEnd = $timePointEnd->modify('+8 hours');
          $timePointEndMax = $timePointEndMax->modify('+16 hours');

          $margin_after_start = clone $timePoint;
          $margin_after_start = $margin_after_start->modify('+30 minutes');

          $margin_before_end = clone $timePointEnd;
          $margin_before_end = $margin_before_end->modify('-30 minutes');
          $margin_after_end = clone $timePointEnd;
          $margin_after_end = $margin_after_end->modify('+30 minutes');

          $margin_before_end_max = clone $timePointEndMax;
          $margin_before_end_max = $margin_before_end_max->modify('-30 minutes');
          $margin_after_end_max = clone $timePointEndMax;
          $margin_after_end_max = $margin_after_end_max->modify('+30 minutes');

          $session = [
            'start' => $timePoint,
            'end' => $timePointEnd,
            'end_max' => $timePointEndMax,
            'margin_after_start' => $margin_after_start,
            'margin_before_end' => $margin_before_end,
            'margin_after_end' => $margin_after_end,
            'margin_before_end_max' => $margin_before_end_max,
            'margin_after_end_max' => $margin_after_end_max,
          ];
        }
        if ($timePoint->getTimestamp() <= $session['margin_after_start']->getTimestamp()) {
          $data[$date_str]['checkin'][] = $timePoint;
        }

        if ($session['margin_after_start']->getTimestamp() < $timePoint->getTimestamp() && $timePoint->getTimestamp() < $session['margin_before_end']->getTimestamp()) {
          $data[$date_str]['break'][] = $timePoint;
        }

        if ($session['margin_before_end']->getTimestamp() <= $timePoint->getTimestamp() && $timePoint->getTimestamp() <= $session['margin_after_end_max']->getTimestamp()) {
          $data[$date_str]['checkout'][] = $timePoint;
        }

        if (empty($timePointNext) || $timePointNext->getTimestamp() > $session['margin_after_end_max']->getTimestamp()) {
          $session = [];
          if (empty($data[$date_str]['checkout']) && !empty($data[$date_str]['break'])) {
            $data[$date_str]['checkout'][] = end($data[$date_str]['break']);
            unset($data[$date_str]['break'][count($data[$date_str]['break']) - 1]);
          }
        }
      }
    }

    return $data;
  }

  /**
   * Convert to show data.
   *
   * {@inheritdoc}
   */
  public function convertDataTable($data) {
    $results = [];
    $work_day = $this->workTimeHoliday->getWorkDay();
    $holidays = $this->workTimeHoliday->getHolidays();
    foreach ($data as $day => $process) {
      $break = NULL;
      if (!empty($process['break'])) {
        $break[] = reset($process['break'])->format('H:i');
        if (count($process['break']) > 1) {
          $break[] = end($process['break'])->format('H:i');
        }
      }
      if ($break) {
        $break = implode('-', $break);
      }

      // total, overtime.
      $total = NULL;
      $overtime = NULL;
      $checkin = is_array($process['checkin']) ? reset($process['checkin']) : NULL;
      $checkout = !empty($process['checkout']) && is_array($process['checkout']) ? end($process['checkout']) : NULL;

      if (!empty($checkin) && !empty($checkout)) {
        $total_time = $checkout->getTimestamp() - $checkin->getTimestamp();
        $total_hours = floor($total_time / 3600);
        $total_minutes = floor($total_time % 3600 / 60);
        if ($total_hours) {
          $total[] = $this->t('@hourshours', ['@hours' => $total_hours]);
        }
        if ($total_minutes) {
          $total[] = $this->t('@minutesminutes', ['@minutes' => $total_minutes]);
        }

        if ($total_time > 8 * 3600) {
          $overtime_time = $total_time - 8 * 3600;
          if ($overtime_time >= 3600) {
            $overtime_hours = floor($overtime_time / 3600);
            $overtime_minutes = floor($overtime_time % 3600 / 60);
            if ($overtime_hours) {
              $overtime[] = $this->t('@hourshours', ['@hours' => $overtime_hours]);
            }
            if ($overtime_minutes) {
              $overtime[] = $this->t('@minutesminutes', ['@minutes' => $overtime_minutes]);
            }
          }
        }
      }

      // Weekend.
      $date = DrupalDateTime::createFromFormat('Y-m-d', $day);
      $day_week = $date->format('w');
      $weekend = NULL;
      if (in_array($day_week, $work_day)) {
        $weekend = 1;
      }

      // Holiday.
      $date = DrupalDateTime::createFromFormat('Y-m-d', $day);
      $day_format_full = $date->format('d/m/Y');
      $day_format_short = $date->format('d/m');
      $holiday = NULL;
      if (in_array($day_format_full, $holidays) || in_array($day_format_short, $holidays)) {
        $holiday = 1;
      }

      $checkin_str = $checkin ? $checkin->format('d/m/Y H:i') : NULL;
      $checkout_str = $checkout ? $checkout->format('d/m/Y H:i') : NULL;
      $results[] = [
        'date' => $day_format_full,
        'checkin' => $checkin_str,
        'checkout' => $checkout_str,
        'lunch' => $break ?? '',
        'total' => is_array($total) ? implode(', ', $total) : $total,
        'overtime' => is_array($overtime) ? implode(', ', $overtime) : $overtime,
        'weekend' => $weekend,
        'holiday' => $holiday,
        'history' => '',
      ];
    }

    return $results;
  }

  /**
   * Convert data to import work time.
   *
   * {@inheritdoc}
   */
  public function convertDataWorkTime($data) {
    $results = [];
    $work_day = $this->workTimeHoliday->getWorkDay();
    $holidays = $this->workTimeHoliday->getHolidays();
    foreach ($data as $day => $process) {
      // Weekend.
      $date = DrupalDateTime::createFromFormat('Y-m-d', $day);
      $day_week = $date->format('w');
      $weekend = NULL;
      if (in_array($day_week, $work_day)) {
        $weekend = 3;
      }

      // Holiday.
      $date = DrupalDateTime::createFromFormat('Y-m-d', $day);
      $day_format_full = $date->format('d/m/Y');
      $day_format_short = $date->format('d/m');
      $holiday = NULL;
      if (in_array($day_format_full, $holidays) || in_array($day_format_short, $holidays)) {
        $holiday = 2;
      }

      $start = reset($process['checkin']);
      $end = NULL;
      if (!empty($process['checkout'])) {
        $end = end($process['checkout']);
      }

      $data_overtime = NULL;

      $created = $start->getTimeStamp();
      $stopped = !empty($end) ? $end->getTimeStamp() : NULL;
      if (!empty($stopped) && $stopped - $created > 8 * 60 * 60) {
        if ($stopped - ($created + 8 * 60 * 60) >= 3600) {
          $overtime = round(($stopped - ($created + (8 * 60 * 60))) / 3600);
          $created_overtime = $created + 8 * 60 * 60;
          $stopped_overtime = $created_overtime + $overtime * 3600;
          $data_overtime = [
            'created' => $created_overtime,
            'stopped' => $stopped_overtime,
            'time_total' => $overtime * 3600,
            'status' => 0,
            'payroll' => $holiday ?? $weekend ?? 0,
          ];

          $stopped = $created_overtime;
        }
      }

      $time_total = !empty($stopped) ? $stopped - $created : NULL;
      $results[] = [
        'created' => $created,
        'stopped' => $stopped ?? NULL,
        'time_total' => $time_total,
        'status' => 1,
        'payroll' => $holiday ?? $weekend ?? 1,
      ];
      if ($data_overtime) {
        $results[] = $data_overtime;
      }
    }
    return $results;
  }

}
