<?php

namespace Drupal\fingerprint\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for fingerprint routes.
 */
class FingerprintController extends ControllerBase {

  /**
   * Ajax json response.
   */
  public function ajaxTimeline($date = '') {
    // Current week.
    $start = strtotime('this week');
    $end = strtotime('this week +6 days');
    if (!empty($date)) {
      if (strpos($date, "-W") !== FALSE) {
        [$year, $week] = explode('-W', $date);
        $start = strtotime($year . 'W' . str_pad($week, 2, '0', STR_PAD_LEFT));
        $end = strtotime('+6 days', $start);
      }
      else {
        $month = date('Y-m', strtotime($date));
        $start = strtotime(date('Y-m-01', strtotime($month)));
        $end = strtotime(date('Y-m-t', strtotime($month)));
      }
    }
    $response = $this->getData($start, $end);
    $response['method'] = 'GET';
    $response['status'] = 200;
    return new JsonResponse($response);
  }

  /**
   * Get time data.
   *
   * {@inheritdoc}
   */
  public function getData($start, $end) {
    $groups = $result = [];
    $entityManager = $this->entityTypeManager();
    $query = $entityManager->getStorage('work_time')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('bundle', 'fingerprint')
      ->condition('created', [$start, $end], 'BETWEEN');
    $fingerprint_ids = $query->execute();
    if ($fingerprint_ids) {
      $fingerService = $entityManager->getStorage('work_time');
      $userService = $entityManager->getStorage('user');
      foreach ($fingerprint_ids as $id) {
        $entity = $fingerService->load($id);
        $uid = $entity->getOwnerId();
        if (empty($groups[$uid])) {
          $user = $userService->load($uid);
          $groups[$uid] = [
            'id' => $uid,
            'content' => empty($user) ? $entity->get('label')->value : $user->getDisplayName(),
          ];
        }
        $hour = round($entity->get('time_total')->value / 3600);
        $result[] = [
          "id" => $entity->id() . "-start",
          "content" => empty($hour) ? 'N/A' : "$hour",
          "start" => date('Y-m-d\TH:i', $entity->get('created')->value),
          'title' => date('H:i', $entity->get('created')->value),
        ];
        if (!empty($entity->get('stopped')->value)) {
          $result[] = [
            "id" => $entity->id() . "-end",
            "content" => empty($hour) ? 'N/A' : "$hour",
            "start" => date('Y-m-d\TH:i', $entity->get('stopped')->value),
            'title' => date('H:i', $entity->get('stopped')->value),
          ];
        }
      }
    }
    return ['items' => $result, 'groups' => array_values($groups)];
  }

}
