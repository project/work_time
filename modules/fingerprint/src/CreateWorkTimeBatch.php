<?php

namespace Drupal\fingerprint;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\work_time\Entity\WorkTime;

/**
 * Class action create work time.
 *
 * {@inheritdoc}.
 */
class CreateWorkTimeBatch {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(protected MessengerInterface $messenger) {
  }

  /**
   * {@inheritdoc}
   */
  public static function createWorkTime($data, &$context) {
    $work_time = WorkTime::create([
      'bundle' => 'fingerprint',
      'uid' => $data['uid'],
      'label' => $data['label'],
      'created' => $data['created'],
      'stopped' => $data['stopped'],
      'time_total' => $data['time_total'],
      'status' => $data['status'],
      'payroll' => $data['payroll'],
    ]);
    $results = $work_time->save();
    $context['message'] = 'Creating work time...';
    $context['results'] = $results;
  }

  /**
   * {@inheritdoc}
   */
  public function createWorkTimeFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = $this->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = $this->t('Finished with an error.');
    }
    $this->messenger->addMessage($message);
  }

}
