<?php

namespace Drupal\fingerprint\Hook;

use Drupal\Core\Hook\Attribute\Hook;

/**
 * Hook implementations for filter.
 */
class FingerprintHooks {

  /**
   * Implements hook_theme().
   */
  #[Hook('theme')]
  public function theme() : array {
    return [
      'fingerprint' => [
        'render element' => 'elements',
      ],
    ];
  }

}
