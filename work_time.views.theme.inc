<?php

/**
 * @file
 * Theme for worktime views.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Core\Utility\TableSort;

/**
 * Prepares variables for views table templates.
 *
 * Default template: views-view-work-time.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_view_work_time(array &$variables) {
  $view = $variables['view'];

  $user = [];
  foreach ($view->result as $item) {
    $wt_item = $item->_entity;
    if (!in_array($wt_item->get('uid')->target_id, $user)) {
      $user[] = $wt_item->get('uid')->target_id;
    }
  }
  $variables['view']->element['#attached']['drupalSettings']['table_worktime']['user'] = $user;

  $year = date('Y');
  $work_time_get_config = function ($holidays = NULL, $year = NULL) {
    $configResult = \Drupal::config('work_time.settings')->getRawData();
    $generalHoliday = $configResult['holidays'];
    if (!empty($holidays)) {
      $generalHoliday .= ',' . $holidays;
    }
    $holidays = explode(',', preg_replace('/\s+/', '', preg_replace('/\//', '-', $generalHoliday)));
    $resultHolidays = [];
    if (!empty($year)) {
      foreach ($holidays as $holiday) {
        if (strlen($holiday) < 6) {
          $holiday = "$holiday-$year";
          $resultHolidays[] = date('m-d', strtotime($holiday));
        }
        else {
          $resultHolidays[] = date('Y-m-d', strtotime($holiday));
        }
      }
    }
    $configResult['holidays'] = $resultHolidays;
    return $configResult;
  };
  $generalConfig = $work_time_get_config(NULL, $year);
  $variables['view']->element['#attached']['drupalSettings']['table_worktime']['holidays'] = !empty($generalConfig['holidays']) ? $generalConfig['holidays'] : [];
  $variables['view']->element['#attached']['drupalSettings']['table_worktime']['general_work_day'] = !empty($generalConfig['work_day']) ? $generalConfig['work_day'] : [];

  // We need the raw data for this grouping, which is passed in as
  // $variables['rows']. However, the template also needs to use for
  // the rendered fields. We therefore swap the raw data out to a new variable
  // and reset $variables['rows'] so that it can get rebuilt.
  // Store rows so that they may be used by further preprocess functions.
  $result = $variables['result'] = $variables['rows'];
  $variables['rows'] = [];
  $variables['header'] = [];

  $options = $view->style_plugin->options;
  $handler = $view->style_plugin;

  $fields = &$view->field;
  $columns = $handler->sanitizeColumns($options['columns'], $fields);

  $active = !empty($handler->active) ? $handler->active : '';
  $order = !empty($handler->order) ? $handler->order : 'asc';

  // A boolean variable which stores whether the table has a responsive class.
  $responsive = FALSE;

  // For the actual site we want to not render full URLs, because this would
  // make pagers cacheable per URL, which is problematic in blocks, for example.
  // For the actual live preview though the javascript relies on properly
  // working URLs.
  $route_name = !empty($view->live_preview) ? '<current>' : '<none>';

  $query = TableSort::getQueryParameters(\Drupal::request());
  if (isset($view->exposed_raw_input)) {
    $query += $view->exposed_raw_input;
  }

  // A boolean to store whether the table's header has any labels.
  $has_header_labels = FALSE;
  foreach ($columns as $field => $column) {
    // Create a second variable, so we can easily find what fields we have and
    // what the CSS classes should be.
    $variables['fields'][$field] = Html::cleanCssIdentifier($field);
    if ($active == $field) {
      $variables['fields'][$field] .= ' is-active';
    }

    // Render the header labels.
    if ($field == $column && empty($fields[$field]->options['exclude'])) {
      $label = !empty($fields[$field]) ? $fields[$field]->label() : '';
      if (empty($options['info'][$field]['sortable']) || !$fields[$field]->clickSortable()) {
        $variables['header'][$field]['content'] = $label;
      }
      else {
        $initial = !empty($options['info'][$field]['default_sort_order']) ? $options['info'][$field]['default_sort_order'] : 'asc';

        if ($active == $field) {
          $initial = ($order == 'asc') ? 'desc' : 'asc';
        }

        $title = t('sort by @s', ['@s' => $label]);
        if ($active == $field) {
          $variables['header'][$field]['sort_indicator'] = [
            '#theme' => 'tablesort_indicator',
            '#style' => $initial,
          ];
        }

        $query['order'] = $field;
        $query['sort'] = $initial;
        $link_options = [
          'query' => $query,
        ];
        $url = new Url($route_name, [], $link_options);
        $variables['header'][$field]['url'] = $url->toString();
        $variables['header'][$field]['content'] = $label;
        $variables['header'][$field]['title'] = $title;
      }

      $variables['header'][$field]['default_classes'] = $fields[$field]->options['element_default_classes'];
      // Set up the header label class.
      $variables['header'][$field]['attributes'] = [];
      $class = $fields[$field]->elementLabelClasses(0);
      if ($class) {
        $variables['header'][$field]['attributes']['class'][] = $class;
      }
      // Add responsive header classes.
      if (!empty($options['info'][$field]['responsive'])) {
        $variables['header'][$field]['attributes']['class'][] = $options['info'][$field]['responsive'];
        $responsive = TRUE;
      }
      // Add a CSS align class to each field if one was set.
      if (!empty($options['info'][$field]['align'])) {
        $variables['header'][$field]['attributes']['class'][] = Html::cleanCssIdentifier($options['info'][$field]['align']);
      }
      // Add a header label wrapper if one was selected.
      if ($variables['header'][$field]['content']) {
        $element_label_type = $fields[$field]->elementLabelType(TRUE, TRUE);
        if ($element_label_type) {
          $variables['header'][$field]['wrapper_element'] = $element_label_type;
        }
        // Improves accessibility of complex tables.
        $variables['header'][$field]['attributes']['id'] = Html::getUniqueId('view-' . $field . '-table-column');
      }
      // Check if header label is not empty.
      if (!empty($variables['header'][$field]['content'])) {
        $has_header_labels = TRUE;
      }

      $variables['header'][$field]['attributes'] = new Attribute($variables['header'][$field]['attributes']);
    }

    // Add a CSS align class to each field if one was set.
    if (!empty($options['info'][$field]['align'])) {
      $variables['fields'][$field] .= ' ' . Html::cleanCssIdentifier($options['info'][$field]['align']);
    }

    // Render each field into its appropriate column.
    foreach ($result as $num => $row) {

      // Skip building the attributes and content if the field is to be excluded
      // from the display.
      if (!empty($fields[$field]->options['exclude'])) {
        continue;
      }

      // Reference to the column in the loop to make the code easier to read.
      $column_reference =& $variables['rows'][$num]['columns'][$column];

      $column_reference['default_classes'] = $fields[$field]->options['element_default_classes'];

      // Set the field key to the column, so it can be used for adding classes
      // in a template.
      $column_reference['fields'][] = $variables['fields'][$field];

      // Add field classes.
      if (!isset($column_reference['attributes'])) {
        $column_reference['attributes'] = [];
      }

      if ($classes = $fields[$field]->elementClasses($num)) {
        $column_reference['attributes']['class'][] = $classes;
      }

      // Add responsive header classes.
      if (!empty($options['info'][$field]['responsive'])) {
        $column_reference['attributes']['class'][] = $options['info'][$field]['responsive'];
      }

      // Improves accessibility of complex tables.
      if (isset($variables['header'][$field]['attributes']['id'])) {
        $column_reference['attributes']['headers'] = [$variables['header'][$field]['attributes']['id']];
      }

      if (!empty($fields[$field])) {
        $field_output = $handler->getField($num, $field);
        $column_reference['wrapper_element'] = $fields[$field]->elementType(TRUE, TRUE);
        if (!isset($column_reference['content'])) {
          $column_reference['content'] = [];
        }

        // Only bother with separators and stuff if the field shows up.
        // Place the field into the column, along with an optional separator.
        if (trim($field_output) != '') {
          if (!empty($column_reference['content']) && !empty($options['info'][$column]['separator'])) {
            $column_reference['content'][] = [
              'separator' => ['#markup' => $options['info'][$column]['separator']],
              'field_output' => ['#markup' => $field_output],
            ];
          }
          else {
            $column_reference['content'][] = [
              'field_output' => ['#markup' => $field_output],
            ];
          }
        }
      }
      $column_reference['attributes'] = new Attribute($column_reference['attributes']);
    }

    // Remove columns if the "empty_column" option is checked and the
    // field is empty.
    if (!empty($options['info'][$field]['empty_column'])) {
      $empty = TRUE;
      foreach ($variables['rows'] as $columns) {
        $empty &= empty($columns['columns'][$column]['content']);
      }
      if ($empty) {
        foreach ($variables['rows'] as &$column_items) {
          unset($column_items['columns'][$column]);
        }
        unset($variables['header'][$column]);
      }
    }
  }

  // Hide table header if all labels are empty.
  if (!$has_header_labels) {
    $variables['header'] = [];
  }

  foreach ($variables['rows'] as $num => $row) {
    $variables['rows'][$num]['attributes'] = [
      'data-row-index' => $num,
    ];
    if ($row_class = $handler->getRowClass($num)) {
      $variables['rows'][$num]['attributes']['class'][] = $row_class;
    }
    $variables['rows'][$num]['attributes'] = new Attribute($variables['rows'][$num]['attributes']);
  }
  $variables['view']->element['#attached']['drupalSettings']['table_worktime']['user'] = $user;

  if (empty($variables['rows']) && !empty($options['empty_table'])) {
    $build = $view->display_handler->renderArea('empty');
    $variables['rows'][0]['columns'][0]['content'][0]['field_output'] = $build;
    $variables['rows'][0]['attributes'] = new Attribute(['class' => ['odd']]);
    // Calculate the amounts of rows with output.
    $variables['rows'][0]['columns'][0]['attributes'] = new Attribute([
      'colspan' => count($variables['header']),
      'class' => ['views-empty'],
    ]);
  }

  $variables['sticky'] = FALSE;
  if (!empty($options['sticky'])) {
    $variables['view']->element['#attached']['library'][] = 'core/drupal.tableheader';
    $variables['sticky'] = TRUE;
  }

  // Add the caption to the list if set.
  if (!empty($handler->options['caption'])) {
    $variables['caption'] = ['#markup' => $handler->options['caption']];
    $variables['caption_needed'] = TRUE;
  }
  elseif (!empty($variables['title'])) {
    $variables['caption'] = ['#markup' => $variables['title']];
    $variables['caption_needed'] = TRUE;
  }
  else {
    $variables['caption'] = '';
    $variables['caption_needed'] = FALSE;
  }

  // For backwards compatibility, initialize the 'summary' and 'description'
  // variables, although core templates now all use 'summary_element' instead.
  $variables['summary'] = $handler->options['summary'];
  $variables['description'] = $handler->options['description'];
  if (!empty($handler->options['summary']) || !empty($handler->options['description'])) {
    $variables['summary_element'] = [
      '#type' => 'details',
      '#title' => $handler->options['summary'],
      // To ensure that the description is properly escaped during rendering,
      // use an 'inline_template' to let Twig do its magic, instead of 'markup'.
      'description' => [
        '#type' => 'inline_template',
        '#template' => '{{ description }}',
        '#context' => [
          'description' => $handler->options['description'],
        ],
      ],
    ];
    $variables['caption_needed'] = TRUE;
  }

  $variables['responsive'] = FALSE;
  // If table has headers, and it should react responsively to columns hidden
  // with the classes represented by the constants RESPONSIVE_PRIORITY_MEDIUM
  // and RESPONSIVE_PRIORITY_LOW, add the tableresponsive behaviors.
  if (isset($variables['header']) && $responsive) {
    $variables['view']->element['#attached']['library'][] = 'core/drupal.tableresponsive';
    // Add 'responsive-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $variables['responsive'] = TRUE;
  }
}

/**
 * Prepares variables for views table templates.
 *
 * Default template: views-view-work-timekeeper.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_view_work_timekeeper(array &$variables) {
  $view = $variables['view'];
  $options = $view->style_plugin->options;

  // Fetch wrapper classes from handler options.
  if ($options['wrapper_class']) {
    $variables['attributes']['class'] = explode(' ', $options['wrapper_class']);
  }

  $variables['default_row_class'] = $options['default_row_class'];
  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id] = [
      'content' => $row,
      'attributes' => new Attribute(),
    ];
    if ($row_class = $view->style_plugin->getRowClass($id)) {
      $variables['rows'][$id]['attributes']->addClass($row_class);
    }
  }
  $site_config = \Drupal::config('system.site');
  $variables['site_name'] = $site_config->get('name');
  $variables['site_slogan'] = $site_config->get('slogan');
  $variables['site_logo'] = theme_get_setting('logo.url');

}

/**
 * Prepares variables for views table templates.
 *
 * Default template: views-view-work-time-worktime-distance.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_view_work_time_worktime_distance(array &$variables) {
  $view = $variables['view'];
  $options = $view->style_plugin->options;

  // Fetch wrapper classes from handler options.
  $variables['default_row_class'] = $options['default_row_class'];
  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id] = [];
    $variables['rows'][$id]['content'] = $row;
    $variables['rows'][$id]['attributes'] = new Attribute();
    if ($row_class = $view->style_plugin->getRowClass($id)) {
      $variables['rows'][$id]['attributes']->addClass($row_class);
    }
  }
}

/**
 * Prepares variables for views table templates.
 *
 * Default template: views-view-work-time-worktime-salary.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_view_work_time_worktime_salary(array &$variables) {
  $view = $variables['view'];
  $options = $view->style_plugin->options;
  $result = $view->result;
  $group = explode('.', $options['group']);
  $group = end($group);

  $list_salary = [];
  $total_hour = 0;
  $total_hour_date = [];
  $total_hour_payroll = [];
  $name_employee = '';
  $total_row = [];
  if ($result) {
    foreach ($result as $item) {
      // Get name employee.
      $entity_group = NULL;
      if ($item->_relationship_entities[$group]) {
        $entity_group = $item->_relationship_entities[$group];
      }
      if ($entity_group && empty($list_salary[$entity_group->id()])) {
        $name_employee = $entity_group->getDisplayName();
        $field_fixed_salary = explode('.', $options['fixed_salary']);
        $field_fixed_salary = end($field_fixed_salary);
        $field_performance_salary = explode('.', $options['performance_salary']);
        $field_performance_salary = end($field_performance_salary);
        $list_salary[$entity_group->id()] = [
          'fixed_salary' => $entity_group->get($field_fixed_salary)->value ?? NULL,
          'performance_salary' => $entity_group->get($field_performance_salary)->value ?? NULL,
        ];
      }

      // Sum total date.
      $entityWorktime = $item->_entity;
      $date = DrupalDateTime::createFromTimestamp($entityWorktime->get('created')->value);
      $str_date = $date->format('Y-m-d');
      if (empty($total_hour_date[$str_date])) {
        $total_hour_date[$str_date] = 0;
      }
      $total_hour_date["$str_date"] += (int) $entityWorktime->get('time_total')->value;

      // Sum total payroll.
      $payroll = $entityWorktime->get('payroll')->value;
      if (empty($total_hour_payroll[$payroll])) {
        $total_hour_payroll[$payroll] = 0;
      }
      $total_hour_payroll[$payroll] += (int) $entityWorktime->get('time_total')->value;

      $total_hour += (int) $entityWorktime->get('time_total')->value;

      // Sum total row.
      $week_number = $date->format('Y-\WW');
      if (empty($total_row[$week_number])) {
        $total_row[$week_number] = [
          'normal' => 0,
          'over' => 0,
        ];
      }
      if ($payroll == 1) {
        $total_row[$week_number]['normal'] += (int) $entityWorktime->get('time_total')->value;
      }
      else {
        $total_row[$week_number]['over'] += (int) $entityWorktime->get('time_total')->value;
      }

      // Sum total col.
      $week_day = $date->format("w");
      if (empty($total_row[$week_day])) {
        $total_row[$week_day] = 0;
      }
      $total_row[$week_day] += (int) $entityWorktime->get('time_total')->value;
    }
  }

  $generalConfig = \Drupal::config('work_time.settings');
  $work_days = $generalConfig->get('work_day') ?? [];
  $hours = $generalConfig->get('hours');
  $coefficient_overtime = $generalConfig->get('coefficient_overtime') ?? NULL;
  $coefficient_holiday = $generalConfig->get('coefficient_holiday') ?? NULL;
  $coefficient_day_off = $generalConfig->get('coefficient_day_off') ?? NULL;
  $coefficient_vacation = $generalConfig->get('coefficient_vacation') ?? NULL;
  $holidays_result = \Drupal::service('work_time.holiday')->getHolidays($generalConfig->get('holidays'));
  $holidays_result = array_values(array_unique($holidays_result));

  $total_holiday = 0;
  $total_work = 0;
  $array_table = [];
  // Function format hours.
  $format_hours = function ($time) {
    $hour = floor($time / 3600);
    $minute = floor(($time % 3600) / 60);
    $hour_format = [];
    if ($hour) {
      $hour_format[] = $hour . "h";
    }
    if ($minute) {
      $hour_format[] = $minute . "m";
    }
    return implode(' ', $hour_format);
  };

  // Get range filter.
  $get_range_filter = function ($filter, $mode_filter) {
    $current_date = new DrupalDateTime();
    switch ($mode_filter) {
      case 'week':
        if (empty($filter)) {
          $filter = $current_date->format('Y-\WW');
        }
        $timestamp = strtotime($filter);
        $time_start = strtotime('monday this week 00:00:00', $timestamp);
        $time_end = strtotime('sunday this week 00:00:00', $timestamp);
        break;

      default:
        if (empty($filter)) {
          $filter = $current_date->format('Y-m');
        }
        $timestamp = strtotime($filter);
        $time_start = strtotime('first day of this month 00:00:00', $timestamp);
        $time_end = strtotime('last day of this month 00:00:00', $timestamp);
    }
    if ($time_start < $time_end) {
      $range = [];
      while ($time_start <= $time_end) {
        $date = DrupalDateTime::createFromTimestamp($time_start);
        $range[] = $date;
        $time_start += 86400;
      }
      return $range;
    }
    return FALSE;
  };

  $filter_value = $variables['view']->element['#attached']['drupalSettings']['work_time']['filter_value'];
  $rangeFilter = $get_range_filter($filter_value, $options['filter']);
  if ($rangeFilter) {
    foreach ($rangeFilter as $date) {
      // Calculator day working and holiday.
      if (!in_array($date->format("w"), $work_days)) {
        $total_work++;
      }
      if (in_array($date->format('d/m/Y'), $holidays_result) || in_array($date->format('d/m'), $holidays_result)) {
        $total_holiday++;
      }

      // Table.
      $week_number = $date->format('Y-\WW');
      if (empty($array_table[$week_number])) {
        $array_table[$week_number] = [];
      }
    }
  }
  // Set total table.
  foreach ($array_table as $week => $row) {
    $range = $get_range_filter($week, 'week');
    $str_range = reset($range)->format('d/m') . ' - ' . end($range)->format('d/m');
    $array_table[$week]['range_date'] = $str_range;
    foreach ($range as $date_range) {
      $str_date = $date_range->format('Y-m-d');
      $array_table[$week]['cells'][] = !empty($total_hour_date[$str_date]) ? round($total_hour_date[$str_date] / 3600, 1) . 'h' : '--';
    }
    if (!empty($total_row[$week])) {
      $array_table[$week]['total_normal'] = !empty($total_row[$week]['normal']) ? round(($total_row[$week]['normal'] / 3600), 1) . 'h' : '--';
      $array_table[$week]['total_over'] = !empty($total_row[$week]['over']) ? round(($total_row[$week]['over'] / 3600), 1) . 'h' : '--';
    }
    else {
      $array_table[$week]['total_normal'] = '--';
      $array_table[$week]['total_over'] = '--';
    }
  }

  $salary = reset($list_salary);
  $rate = (int) $salary['fixed_salary'] / $total_work / (int) $hours;

  $total_wage = 0;
  foreach ($total_hour_payroll as $key => $value) {
    if ($key == 0) {
      $hours_time = $value / 3600;
      $total_wage += $hours_time * $rate * ((float) $coefficient_overtime ?? 1);
    }
    if ($key == 1) {
      $hours_time = $value / 3600;
      $total_wage += $hours_time * $rate;
    }
    if ($key == 2) {
      $hours_time = $value / 3600;
      $total_wage += $hours_time * $rate * ((float) $coefficient_holiday ?? 1);
    }
    if ($key == 3) {
      $hours_time = $value / 3600;
      $total_wage += $hours_time * $rate * ((float) $coefficient_day_off ?? 1);
    }
    if ($key == 4) {
      $hours_time = $value / 3600;
      $total_wage += $hours_time * $rate * ((float) $coefficient_vacation ?? 1);
    }
  }

  $variables['payroll'] = [
    'Overtime',
    'Regular time',
    'Work in holiday',
    'Work in day off',
    'Work in vacation',
  ];

  $result_total_hour_payroll = [];
  if ($total_hour_payroll) {
    foreach ($total_hour_payroll as $key => $value) {
      $result_total_hour_payroll[] = [
        'value' => $format_hours($value),
        'label' => $variables['payroll'][$key],
      ];
    }
  }

  if ($options['filter'] == 'week') {
    $range_report = [
      reset($rangeFilter)->format('d/m/Y'),
      end($rangeFilter)->format('d/m/Y'),
    ];
  }
  else {
    $range_report = [$rangeFilter[0]->format('m/Y')];
  }
  $language = $generalConfig->get('language') ?? 'en';
  $fmt = numfmt_create($language, NumberFormatter::CURRENCY);
  $site_config = \Drupal::config('system.site');
  $variables['site_name'] = $site_config->get('name');
  $variables['site_slogan'] = $site_config->get('slogan');
  $variables['site_logo'] = theme_get_setting('logo.url');
  $variables['range_report'] = implode(' - ', $range_report);
  $variables['name_employee'] = $name_employee;
  $variables['fixed_salary'] = $fmt->formatCurrency($salary['fixed_salary'] ?? 0, $options['unit_salary']);
  $variables['performance_salary'] = $fmt->formatCurrency($salary['performance_salary'] ?? 0, $options['unit_salary']);
  $variables['total_hour_payroll'] = $result_total_hour_payroll;
  $variables['total_hour'] = $format_hours($total_hour);
  $variables['total_work'] = $total_work;
  $variables['wage_total'] = $fmt->formatCurrency($total_wage, $options['unit_salary']);
  $variables['total_holiday'] = $total_holiday;
  $variables['rate'] = $fmt->formatCurrency($rate, $options['unit_salary']);
  $variables['table'] = $array_table;

  // Fetch wrapper classes from handler options.
  if ($options['wrapper_class']) {
    $variables['attributes']['class'] = explode(' ', $options['wrapper_class']);
  }

  $variables['default_row_class'] = $options['default_row_class'];
  foreach ($variables['rows'] as $id => $row) {
    $variables['rows'][$id] = [
      'content' => $row,
      'attributes' => new Attribute(),
    ];
    if ($row_class = $view->style_plugin->getRowClass($id)) {
      $variables['rows'][$id]['attributes']->addClass($row_class);
    }
  }
}
