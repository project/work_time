(function (Drupal, $, once) {
  'use strict';

  // getCsrfToken.
  let getCsrfToken = async () => {
    await axios.get(Drupal.url('session/token')).then((response) => {
      csrf_token = response.data;
    }).then(() => {

    });
  }
  let csrf_token;
  getCsrfToken();

  // Function handle.
  async function actionAdd(data, callback) {
    let action_status = false;
    let action_response = null;
    await axios.post(Drupal.url('api/timeline'),
      {
        group: data.group,
        start: data.start,
        end: data.end || null,
      },
      {
        params: {
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': csrf_token
        }
      }
    ).then((response) => {
      action_status = true;
      action_response = response.data;
    }).catch(error => {
      console.error("There was an error!", error);
    });
    callback(action_status, action_response)
  }

  async function actionMove(data, callback) {
    let action_status = false;
    let action_response = null;
    await axios.patch(Drupal.url('api/timeline/' + data.id),
      {
        start: data.start,
        end: data.end || null,
      },
      {
        params: {
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': csrf_token
        }
      }
    ).then((response) => {
      action_status = true;
      action_response = response.data;
    }).catch(error => {
      console.error("There was an error!", error);
    });
    callback(action_status, action_response)
  }

  async function actionDelete(data, callback) {
    let action_status = false;
    let action_response = null;
    await axios.delete(Drupal.url('api/timeline/' + data.id),
      {
        params: {
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': csrf_token
        }
      }
    ).then((response) => {
      action_status = true;
      action_response = response.data;
    }).catch(error => {
      console.error("There was an error!", error);
    });
    callback(action_status, action_response)
  }

  let loadData = async (bundle = null, date = null) => {
    if (!date) {
      date = new Date();
      date = date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0');
    }
    let action_status = false;
    let action_response = null;
    await axios.get(Drupal.url('api/timeline/0'),
      {
        params: {
          filter: date,
          bundle: bundle,
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': csrf_token
        }
      }
    ).then((response) => {
      action_status = true;
      action_response = response.data;
    }).catch(error => {
      console.error("There was an error!", error);
    });

    items.clear();
    groups.clear();
    if (action_response?.groups.length) {
      groups.add(action_response.groups);
    }
    if (action_response?.items.length) {
      items.add(action_response.items);
      timeline.fit();
    }
  }

  // create visualization
  let container = document.getElementById('work-time-timeline');
  let options = {
    groupOrder: 'content',
    verticalScroll: true,
    showWeekScale: false,
    editable: {
      add: true,         // add new items by double tapping
      updateTime: true,  // drag items horizontally
      updateGroup: true, // drag items from one group to another
      remove: true,       // delete an item by tapping the delete button top right
      overrideItems: false  // allow these options to override item.editable
    },
    limitSize: false,
    stack: false,
    type: 'box',
    tooltip: {
      template: function (originalItemData, parsedItemData) {
        let tooltip = null;
        if (originalItemData?.title) {
          tooltip = `<span>${originalItemData.title}</span>`
        }
        return tooltip;
      }
    },
    showCurrentTime: true,
    locale: navigator.language || 'en',
    maxHeight: '500px',
    groupHeightMode: 'fixed',
    orientation: 'both',
    horizontalScroll: true,
    zoomMin: 1000 * 60 * 60 * 6,
    zoomMax: 1000 * 60 * 60 * 24 * 31 * 3,
    onAdd: async function (item, callback) {
      if (item.start && item.end && item.end.valueOf() - item.start.valueOf() <= 0) {
        callback(null) // cancel updating the item
        return;
      }
      actionAdd(item, function (status, result) {
        if (status) {
          item.id = result.id
          item.title = result.title
          item.content = result.content
          item.type = result.type
          callback(item) // send back adjusted item
        }
        else {
          callback(null) // cancel updating the item
        }
      });
    },

    onMove: function (item, callback) {
      if (item.start && item.end && item.end.valueOf() - item.start.valueOf() <= 0) {
        callback(null) // cancel updating the item
        return;
      }
      actionMove(item, function (status, result) {
        if (status) {
          item.title = result.title
          item.content = result.content
          callback(item) // send back adjusted item
        }
        else {
          callback(null) // cancel updating the item
        }
      })
    },

    onMoving: function (item, callback) {
      let time_total = item.end.valueOf() - item.start.valueOf() || null;
      let arr_time = [];
      if (time_total > 0) {
        time_total /= 1000;
        let hours = Math.floor(time_total / 3600);
        let minutes = Math.floor(time_total % 3600 / 60);
        let seconds = Math.floor(time_total % 3600 % 60);
        if (hours) {
          arr_time.push(Drupal.t('@hoursh', {'@hours': hours}));
        }
        if (minutes) {
          arr_time.push(Drupal.t('@minutesm', {'@minutes': minutes}));
        }
        if (seconds && hours) {
          arr_time.push(Drupal.t('@secondss', {'@seconds': seconds}));
        }

        item.content = arr_time.join(', ');
        callback(item);
      }
      callback(null); // send back the (possibly) changed item
    },

    onUpdate: function (item, callback) {
      callback(null); // cancel updating the item
    },

    onRemove: function (item, callback) {
      actionDelete(item, function (status) {
        if (status) {
          callback(item) // send back adjusted item
        }
        else {
          callback(null) // cancel updating the item
        }
      });
    }
  };

  // Create an empty DataSet.
  // This DataSet is used for two-way data binding with the Timeline.
  let items = new vis.DataSet();
  let groups = new vis.DataSet();
  let timeline = new vis.Timeline(container, items, groups, options);
  let bundle = $('#work-time-timeline').data('bundle') || null;
  loadData(bundle);

  Drupal.behaviors.timeline = {
    attach: function (context, settings) {
      $(once('type-filter', '.type-timeline')).on('change', function () {
        let type = $(this).val();
        $('.date-timeline').attr('type', type);
      });
      $(once('month-timeline', '.date-timeline')).on('change', function () {
        let date = $(this).val();
        loadData(bundle, date);
      });

    }
  };
}(Drupal, jQuery, once));
