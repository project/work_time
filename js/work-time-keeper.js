import TableComponent from './components/table-component.js';

let templateWorkTimeKeeperDefault = `
<div>
  <TableComponent :settings="settings" :leaveDays="leaveDays" :rawData="rawData" :listRowGroup="settings.list_group_record" />
</div>
`;

let App = Vue.createApp({
  template: typeof templateWorkTimeKeeper !== 'undefined' ? templateWorkTimeKeeper : templateWorkTimeKeeperDefault,
  data() {
    return {
      rawData: drupalSettings['work_time_keeper']['data'],
      settings: drupalSettings['work_time_keeper'],
      leaveDays: [],
    }
  },
  methods: {
    getLeave() {
      // Load custom leave.
      let url_leave = this.settings.options.custom_data_leave;
      if (url_leave.length) {
        url_leave = url_leave.trim();
        axios.get(url_leave, {
          params: {
            _format: 'json',
            filter: this.settings.filter_value,
            filter_mode: this.settings.options.filter,
          },
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': ''
          }
        }).then((response) => {
          this.leaveDays = response.data;
        }).catch(error => {

        })
      }
    },
  },
  components: {
    'TableComponent': TableComponent
  },
  created() {
    this.getLeave()
  }
});

App.mount('#work-time-keeper');

