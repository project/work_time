import MultipleTableComponent from "./components/multiple-table-component.js";

let templateWorkTimeTableDefault = `
<div>
  <MultipleTableComponent :settings="settings" :leaveDays="leaveDays" />
</div>
`;

let App = Vue.createApp({
  template: typeof templateWorkTimeTable !== 'undefined' ? templateWorkTimeTable : templateWorkTimeTableDefault,
  data() {
    return {
      settings: drupalSettings['work_time_table'],
      leaveDays: [],
    }
  },
  methods: {
    getLeave() {
      // Load custom leave.
      let url_leave = this.settings.options.custom_data_leave;
      if (url_leave.length) {
        url_leave = url_leave.trim();
        axios.get(url_leave, {
          params: {
            _format: 'json',
            filter: this.settings.filter_value,
            filter_mode: this.settings.options.filter,
          },
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': ''
          }
        }).then((response) => {
          this.leaveDays = response.data;
        }).catch(error => {

        })
      }
    },
  },
  components: {
    'MultipleTableComponent': MultipleTableComponent,
  },
  created() {
    this.getLeave()
  }
});

App.mount('#work-time-table');
