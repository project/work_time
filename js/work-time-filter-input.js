
(function (Drupal, drupalSettings, once) {

  const vueElements = document.querySelectorAll(".work-time-filter-input");

  if (vueElements.length) {
    vueElements.forEach(element => {
      let filter = element.dataset.filterMode;
      let filter_value = element.dataset.filter;
      let templateFilterDefault = `
      <div class="input-group mb-3 d-inline-flex w-auto">
        <label for="filter" class="input-group-text">${Drupal.t('Filter')}</label>
        <template v-if="filterMode == 'year'">
          <select class="form-select" v-model="filter">
            <template v-for="data in selectYear">
            <option :value="data">{{ data }}</option>
            </template>
          </select>
        </template>
        <template v-else>
          <input id="filter" :type="filterMode" class="form-control" v-model="filter">
        <template/>
      </div>
      `;

      let App = Vue.createApp({
        template: typeof templateFilter !== 'undefined' ? templateFilter : templateFilterDefault,
        data() {
          return {
            filterMode: filter,
            filter: filter_value,
          };
        },
        computed: {
          selectYear() {
            let value = parseInt(filter_value);
            return [
              value - 2,
              value - 1,
              value,
              value + 1,
              value + 2,
            ];
          }
        },
        watch: {
          filter(newValue) {
            let url = new URL(window.location.href);
            url.searchParams.set('date', newValue);
            window.location = url;
          }
        },
      });

      App.mount(element)
    });
  }

})(Drupal, drupalSettings, once);
