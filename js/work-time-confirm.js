/** @format */
(function ($, Drupal, drupalSettings, once) {
  window.filterOnChange = function(input) {
    const value = input.value;
    const url = new URL(window.location.href);
    url.searchParams.set(input.name, value);
    window.location.href = url.href;
  }
  window.confirm = function (event) {
    let url = drupalSettings.table_worktime.url_confirm;
    let data = $(event).data();
    $.post( url, data, function( result ) {
      let a =  $(event).closest('div[data-bs-trigger]');
      $(event).closest('div[data-bs-trigger]').removeClass('text-danger')
    });
  }
  const showFilter = (mode, filterValue) => {
    let view_id = drupalSettings.table_worktime.view_id;
    let display_id = drupalSettings.table_worktime.display_id;
    let viewSelector = `.view-id-${view_id}.view-display-id-${display_id}`;
    let year = new Date().getFullYear();

    let inputDate = {
      value: filterValue,
      type: mode,
      min: mode === 'year' ? year - 3 : undefined,
      max: mode === 'year' ? year + 1 : undefined,
      name: 'date',
      class: 'form-control w-auto',
      'aria-labelledby': mode,
      onChange: 'filterOnChange(this)',
      id: 'filter',
    };

    // Show filter
    let filter = $(`<div class="row"><label for="filter" class="col-auto col-form-label text-capitalize">${Drupal.t(mode)}</label></div>`).append($('<input/>', inputDate));
    filter.prependTo(viewSelector);
  };
  const getAllDatesInRange = (startDate, endDate) => {
    const dateArray = [];
    let currentDate = new Date(startDate);
    const targetDate = new Date(endDate);
    while (currentDate <= targetDate) {
      dateArray.push(new Date(currentDate));
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
  }
  const addColumnDate = (mode, filterValue) => {
    let view_id = drupalSettings.table_worktime.view_id;
    let display_id = drupalSettings.table_worktime.display_id;
    let viewSelector = `.view-id-${view_id}.view-display-id-${display_id}`;
    let table = $(`${viewSelector} .table`);
    let thead = table.find('thead tr');
    let tbody = table.find('tbody tr');
    let tfoot = table.find('tfoot tr');
    let holidays = drupalSettings.table_worktime.holidays;
    let totalCol = [];
    let start = drupalSettings.table_worktime.start;
    let end = drupalSettings.table_worktime.end;
    let year = filterValue.substr(0, 4);
    let days = getAllDatesInRange(start,end);
    switch (mode) {
      case 'month':
        days.forEach((currentDay)=>{
          totalCol.push({
            'in': `${currentDay.getFullYear()}-${(currentDay.getMonth() + 1).toString().padStart(2, '0')}-${currentDay.getDate().toString().padStart(2, '0')}`,
            'name': currentDay.getDate(),
          });
        });
        break;

      case 'week':
        const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const dayNames = days.map(currentDay => {
          totalCol.push( {
            'in': `${currentDay.getFullYear()}-${(currentDay.getMonth() + 1).toString().padStart(2, '0')}-${currentDay.getDate().toString().padStart(2, '0')}`,
            'name': Drupal.t(weekday[currentDay.getDay()])
          });
        });
        break;

      case 'year':
        const numberMonth = 12;
        const monthNames = Array.from({ length: numberMonth }, (_, index) => {
          const thisDate = new Date(year, index, 1);
          totalCol.push( { 'in': `${thisDate.getFullYear()}-${(thisDate.getMonth() + 1).toString().padStart(2, '0')}`, 'name': Drupal.t(thisDate.toLocaleDateString('en', { month: 'long' })) });
        });
        break;
    }
    totalCol.forEach((element) => {
      let date = element.in;
      let textClass = cellClass(date);
      thead.append(`<th class="${textClass}" data-field=${element.in} data-date="${date}">${element.name}</th>`);
      tbody.each(function() {
        let index = $(this).data('row-index');
        let group = index + '_' + date;
        $(this).append(`<td class="${group} ${textClass}" data-date=${date} data-day=${element.in}><span></span></td>`);
      });
      tfoot.append(`<th class="${textClass}"></th>`);
    });
  };

  function showTooltip(total, tootip) {
    return `<div data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="${Drupal.t('Total hours: ')} ${tootip}">
        ${total/3600}
    </div>`;
  }

  function popOverDialog(itemData, total, tootip) {
    let content = showTooltip(total, tootip);
    let {id, time_total} = itemData;
    let hour = parseInt(time_total) / 3600;
    let dialog = `
       <button data-action="accept" data-id="${id}" class="btn btn-success btn-sm" type="button" onclick="confirm(this)">${Drupal.t('Accept')} </button>
       <button data-action="close" data-id="${id}" class="btn btn-danger btn-sm" type="button" onclick="confirm(this)">${Drupal.t('Close')}</button>`;
    return `<div data-bs-title="${Drupal.t('Overtime')} ${hour}h" data-bs-content='${dialog}' tabindex="0" data-bs-trigger="focus" role="button" data-bs-toggle="popover" data-bs-html="true" data-bs-sanitize="true"  class="text-danger">
        ${content}
        </div>`;
  }

  function fillData() {
    // Get data from view style worktime.
    const work_time = drupalSettings.work_time;
    let results = [];
    if(work_time && work_time.list){
      results = work_time.list;
      let timeTotalByDate = [];
      Object.keys(results).forEach(key => {
        let items = results[key].worktime;
        let index = results[key].id;
        // Group by date.
        let total = {};
        let details = [];
        items.forEach(item => {
          let {created, status, time_total} = item;
          let [y, m, d] = created.split('-');
          let key = created;
          if (mode == 'year') {
            key = `${y}-${m}`;
          }
          let timeTotalValue = parseInt(time_total, 10);
          if (!total[key]) {
            total[key] = 0;
          }
          total[key] += timeTotalValue;
          if (!details[key]) {
            details[key] = [];
          }
          details[key].push(item);
        });
        // show work time after group.
        Object.entries(details).forEach(([date, detail]) => {
          let group = index + '_' + date;
          let tooltip = '';
          let confirm = null;
          let overtime = 0;
          let normal = 0;
          detail.forEach(item => {
            let {status, time_total} = item;
            let hour = parseInt(time_total) / 3600;
            if (status == 0) {
              overtime += hour;
              confirm = item;
            } else {
              normal += hour;
            }
          });
          if (normal) {
            tooltip += Drupal.t(' Normal hour') + ': ' + normal + 'h ';
          }
          if (overtime) {
            tooltip += Drupal.t(' Overtime hour') + ': ' + overtime + 'h ';
          }
          if (!confirm) {
            $('.' + group).html(showTooltip(total[date], tooltip));
          } else {
            $('.' + group).html(popOverDialog(confirm, total[date], tooltip));
          }
        });
      });
    }

  }
  function leave() {
    if (drupalSettings.table_worktime.custom_data_leave.length) {
      $.ajax({
        url: Drupal.url(drupalSettings.table_worktime.custom_data_leave),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
          if (response.length) {
            response.data.forEach(function (item) {
              let css = 'bg-dark-subtle';
              if (item.type == 1) {
                css = 'bg-body-secondary';
              }
              $(`tr[data-uid-reference^="${item.uid}"] td[data-date="${item.date}"], tr[data-uid-reference^="${item.uid}"] th[data-date="${item.date}"]`).addClass(css);
            })
          }
        }
      });
    }
  }

  function cellClass(day_long) {
    const date = new Date(day_long);
    const dayOfWeekIndex = date.getUTCDay();
    const day_short = `${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
    const work_day = drupalSettings.table_worktime.work_day;
    const holidays = drupalSettings.table_worktime.holidays;
    const leave_days = []; //Leave day off user.
    let css = '';
    if (dayOfWeekIndex in work_day || holidays.includes(day_long) || holidays.includes(day_short)) {
      css += 'weekend';
    }
    if (leave_days.includes(day_long)) {
      css += ' leave';
    }
    return css;
  }
  function calculateColumn(indexTable) {
    $(indexTable).each(function (indexTable) {
      $(this).find('thead th').each(function (index) {
        var total = 0;
        $(this).closest('table').find('tbody tr').each(function () {
          var value = parseInt($('td', this).eq(index).text());
          if (!isNaN(value)) {
            total += value;
          }
        });
        if (total) {
          $(this).closest('table').find('tfoot tr th').eq(index).text(total);
        }
      });
    })

  }

// show showFilter
  let mode = drupalSettings.table_worktime.options.filter_time;
  let filterValue = drupalSettings.table_worktime.filter;
  showFilter(mode, filterValue);

// Use addColumnDate to exists tables
  addColumnDate(mode, filterValue);
// Get data leave.
  leave();
// field data to table.
  fillData();
// Sum total footer.
  calculateColumn('.work-time-confirm');

// Active bootstrap 5 tooltip and popover.
  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
  const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
  const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
  const popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl, {
      html: true,
      sanitize: false,
    });
  });
}(jQuery, Drupal, drupalSettings, once));
