import Edit from "./edit-inline.js";
import Delete from "./delete.js";
import Store from "./store.js";
// Get Bootstrap popover.
const {Popover} = bootstrap;
let templateHistoryDefault = `
<button type="button" class="btn btn-secondary btn-sm" :class="{'bi bi-clock-history': !bsPopover, 'bi-x-circle': bsPopover }" @click="showPopupHistory($event)" data-bs-toggle="popover" data-bs-trigger="focus" ref="btnToggle"></button>
<div class="d-none">
  <div class="popup-history content-popup-worktime-history mt-2" v-if="showHistory" ref="history">
    <button class="btn-close position-absolute" type="button" aria-label="Close" @click="close" ref="btnClose"></button>
    <table class="table mt-2 mb-0 wt-table" v-for="(data, indexData) in historyData">
      <thead>
        <tr>
          <th><div class="rounded-circle text-bg-primary py-1 text-align-center" style="width: 2em; height: 2em">{{ data.initial }}</div></th>
          <th><b>{{ data.name }}</b></th>
          <th><b>{{ displayTimeTotal(data.total_time) }}</b></th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(task, indexTask) in data.details" :key="task.id" class="position-relative">
          <td class="wc-20">{{ displayTimeTotal(task.time_total) }}</td>
          <td>{{ displayDate(task.created) }}</td>
          <td class="wc-25">
            <edit @editTask="editTask" :task="task" :index_data="indexData" :index_task="indexTask" v-if="this.uid == task.uid" />
            <delete @removeTask="removeTask" :task="task" :index_data="indexData" :index_task="indexTask" v-if="this.uid == task.uid" />
         </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
`;
const history = {
  template: typeof templateHistory !== 'undefined' ? templateHistory : templateHistoryDefault,
  props: ['uid', 'entity', 'history_data'],
  data() {
    return {
      showHistory: false,
      bsPopover: null,
      showPopupEdit: false,
    }
  },
  computed: {
    historyData() {
      let historyData = this.history_data;
      if (historyData.length) {
        historyData.forEach(details => {
          if (details.details.length) {
            let total = 0;
            details.details.forEach(detail => {
              total += detail.time_total;
            });
            details.total_time = total;
          }
        });
      }
      return historyData;
    }
  },
  methods: {
    displayDate(time) {
      let date = new Date(time);
      return date.toLocaleDateString();
    },
    displayTimeTotal(time) {
      return Store.formatTime(time, Store.timeFormatFull.hour, Store.timeFormatFull.minute, Store.timeFormatFull.second, ':', 'compact')
    },
    close() {
      this.$refs.btnToggle.click();
    },
    async showPopupHistory(event) {
      if (!this.history_data.length) {
        if (!Store.csrfToken) {
          await Store.csrfTokenLoader();
        }
        axios.get(Drupal.url('api/work-time-list/entity_history'), {
          params: {
            entity_id: this.entity.entityId,
            entity_field: this.entity.entityField,
            _format: 'json'
          }
        }, {
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': Store.csrfToken
          }
        }).then((response) => {
          if (response.data.length) {
            response.data.forEach(details => {
              if (details.details.length) {
                details.details.forEach(detail => {
                  detail.created = parseInt(detail.created) * 1000 || null;
                  detail.stopped = parseInt(detail.stopped) * 1000 || null;
                  detail.time_total = parseInt(detail.time_total) * 1000 || null;
                });
              }
            });
            this.$parent.historyData = response.data;
          }
        }).catch(error => {
          console.error("There was an error!", error);
        });
      }
      this.showHistory = true;
      if (!this.bsPopover) {
        this.bsPopover = new Popover(event.target, {
          trigger: 'click',
          html: true,
          content: () => {
            return this.$refs.history;
          }
        });
        this.bsPopover.toggle();
      }
      else {
        this.bsPopover = null;
      }
    },
    editTask(e) {
      e.data.created = parseInt(e.data.created) * 1000;
      e.data.stopped = parseInt(e.data.stopped) * 1000;
      e.data.time_total = parseInt(e.data.time_total) * 1000;
      this.$parent.historyData[e.index_data].details[e.index_task] = e.data;
    },

    removeTask(e) {
      this.$parent.historyData[e.index_data].details.splice(e.index_task, 1);
    },
    saveTask(task) {
      task.created = task.created / 1000
      task.stopped = task.stopped / 1000
      task.time_total = task.time_total * 60 / 1000
    }
  },
  components: {
    'edit': Edit,
    'delete': Delete,
  }
};

export default history;
