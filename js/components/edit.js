import Store from "./store.js";

const {Modal} = bootstrap;
let templateEditDefault = `
<button type="button" @click="modal.show()" class="btn btn-light"><i class="bi bi-pencil-square"></i></button>
<div class="modal fade" ref="editTask" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="form-group">
          <label for="edit-title">${Drupal.t('Title')}</label>
          <input type="text" class="form-control form-control-sm" id="edit-title" v-model="label">
        </div>
        <div class="form-group d-none">
          <label for="date">${Drupal.t('Date')}</label>
          <input type="date" class="form-control form-control-sm" id="date" v-model="date">
        </div>
        <div class="form-group">
          <label for="edit-start">${Drupal.t('Time Start')}</label>
          <input type="time" class="form-control form-control-sm" id="edit-start" @input="calTotal" v-model="startTime">
        </div>
        <div class="form-group">
          <label for="edit-end">${Drupal.t('Time end')}</label>
           <input type="time" class="form-control form-control-sm" id="edit-end" @input="calTotal" v-model="endTime">
        </div>
        <div class="form-group">
          <label for="edit-total">${Drupal.t('Total minutes')}</label>
          <div class="input-group">
             <input type="number" class="form-control form-control-sm" min="1" :max="maxTotal" id="edit-total" @input="calEnd" v-model="total">
             <span class="input-group-text">m</span>
          </div>
        </div>
        <div class="form-group">
          <label for="edit-project">${Drupal.t('Project')}</label>
           <input type="text" class="form-control form-control-sm" id="edit-project" v-model="project" list="work-time-project">
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><i class="bi bi-x-circle"></i> ${Drupal.t('Cancel')}</button>
          <button ref="btnSubmit" type="button" class="btn btn-success" @click="saveTask"><i class="bi bi-save"></i> ${Drupal.t('Save')}</button>
      </div>
    </div>
  </div>
</div>`;
const edit = {
  template: typeof templateEdit !== 'undefined' ? templateEdit : templateEditDefault,
  emits: ['saveEditTask'],
  props: ['task', 'index_task'],

  data() {
    return {
      label: '',
      date: '',
      startTime: '',
      endTime: '',
      total: '',
      modal: null,
      saveEditTask: {},
      searchProject: '',
      project: '',
      timeout: null,
    }
  },

  computed: {
    maxTotal() {
      let date = this.date
      let startTime = this.startTime
      let objTime = new Date(date + " " + startTime + ":00")
      let objTimeLastDay = new Date(date + " 23:59:59")
      let rangeTime = Math.floor(Math.abs((objTimeLastDay.valueOf() - objTime.valueOf())) / 60000)
      return rangeTime
    }
  },

  methods: {
    async saveTask() {
      let created = new Date(this.date + ' ' + this.startTime + ':00');
      let stopped = new Date(this.date + ' ' + this.endTime + ':00');
      let reference_id = null;
      if (this.project) {
        const match = this.project.match(/\((.*?)\)/);
        if (match && match[1] != undefined) {
          reference_id = match[1];
        }
      }

      let task_id = this.task.id;
      let data = {
        label: this.label,
        reference_id: reference_id,
        created: created.valueOf() / 1000,
        stopped: stopped.valueOf() / 1000,
      };

      let url = Drupal.url('api/work-time/' + task_id + '/edit') + '?_format=json';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader()
      }
      axios.patch(url, data,
        {
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': Store.csrfToken
          }
        }
      ).then((response) => {
        this.saveEditTask = {
          index_task: this.index_task,
          data: response.data.updated
        };
        this.modal.hide();
        this.$emit('saveEditTask', this.saveEditTask);
      }).catch(error => {
        console.error("There was an error!", error);
      });
    },
    calTotal() {
      this.$refs.btnSubmit.setAttribute('disabled', 'disabled');
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let startTime = new Date(this.date + ' ' + this.startTime + ':00');
        let endTime = new Date(this.date + ' ' + this.endTime + ':00');
        if (endTime < startTime) {
          let temp = this.startTime;
          this.startTime = this.endTime;
          this.endTime = temp;
        }
        let diff = Math.abs(endTime - startTime);
        this.total = diff / 60000;
        this.$refs.btnSubmit.removeAttribute('disabled');
      }, 1000);
    },
    calEnd() {
      let startTime = new Date(this.date + ' ' + this.startTime + ':00');
      startTime.setMinutes(startTime.getMinutes() + parseInt(this.total));
      this.endTime = startTime.toLocaleTimeString().slice(0, 5);
    },
  },

  mounted() {
    let project = this.$parent.projects.find((item) => item.nid == this.task.reference_id);
    this.project = project ? project.title + ' (' + this.task.reference_id + ')' : null;
    this.label = this.task.label;
    let dateFormatStart = new Date(this.task.created);
    this.date = Store.formatDate(dateFormatStart, ['year', 'month', 'day'], '-');
    this.startTime = Store.formatDate(dateFormatStart, ['hour', 'minute'], ':');
    if (this.task.stopped) {
      let dateFormatEnd = new Date(this.task.stopped);
      this.endTime = Store.formatDate(dateFormatEnd, ['hour', 'minute'], ':');
    }
    this.total = Math.ceil(parseInt(this.task.time_total) / 60000);
    this.modal = new Modal(this.$refs.editTask);
  },
};
export default edit;
