import Store from "./store.js";
let templateRemoveDefault = `
<button type="button" @click="toggle_show_remove()" class="btn btn-outline-danger"><i class="bi bi-trash"></i></button>
<div class="box-confirm confirm-delete hc-100" :class="{'confirm-active': showRemove}">
  <div class="table-cell p-2 wc-75">${Drupal.t('This operation cannot be undone?')}</div>
  <div class="table-cell p-2 wc-25">
    <button class="btn btn-success bi bi-check-square" @click="remove()"></button>
    <button class="btn btn-danger bi bi-x-square" @click="toggle_show_remove()"></button>
  </div>
</div>
`;
const componentRemove = {
  template: typeof templateRemove !== 'undefined' ? templateRemove : templateRemoveDefault,
  emits: ["removeTask"],
  props: ['task', 'index_data', 'index_task'],

  data() {
    return {
      showRemove: false,
      removeTask: {},
    }
  },

  methods: {
    toggle_show_remove() {
      this.showRemove = !this.showRemove;
    },
    async remove() {
      let task_id = this.task.id;
      let url = Drupal.url('api/work-time/' + task_id + '/delete') + '?_format=json';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader()
      }
      axios.delete(url, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }).then((response) => {
        this.removeTask = {
          index_data: this.index_data,
          index_task: this.index_task,
        },
        this.$emit('removeTask', this.removeTask);
        this.showRemove = false
      }).catch(error => {
        console.error('There was an error!', error);
      });
    },
  }
};
export default componentRemove;
