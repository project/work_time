import Store from './store.js';

const reactive_worktime = Vue.reactive({
  isCallPlaying: false,
  state: {},
  elapsedTime: 0,
  elapsedTimeTotal: 0,
  projects: [],
  interval: null,
  isPlaying: false,
  taskPlaying: null,
  historyData: [],

  // Play.
  async play(id, data) {
    if (!Store.csrfToken) {
      await Store.csrfTokenLoader();
    }
    axios.post(Drupal.url('api/work-time'), data,
      {
        params: {
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }
    ).then((response) => {
      response.data.created = parseInt(response.data.created) * 1000 || null;
      response.data.stopped = parseInt(response.data.stopped) * 1000 || null;
      response.data.time_total = parseInt(response.data.time_total) * 1000 || null;

      this.isPlaying = true;
      this.taskPlaying = response.data;
      this.countUp(Date.now());

      if (this.state.hasOwnProperty(id) && !this.state[id].isPlaying && id !== 'work-time-block') {
        this.state[id].isPlaying = true;
        this.state[id].taskPlaying = response.data;
        this.state[id].countUp(Date.now());
      }
    }).catch(error => {
      console.error("There was an error!", error);
    });
  },

  // Stop.
  async stop() {
    if (!Store.csrfToken) {
      await Store.csrfTokenLoader();
    }
    await axios.patch(Drupal.url('api/work-time/0/stop_all') + '?_format=json', {},
      {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }
    ).then((response) => {
      if (response.data.hasOwnProperty('stopped')) {
        for (const [key, value] of Object.entries(response.data.stopped)) {
          value.created = parseInt(value.created) * 1000 || null;
          value.stopped = parseInt(value.stopped) * 1000 || null;
          value.time_total = parseInt(value.time_total) * 1000 || null;

          this.historyData.unshift(value)

          // Set false playing.
          if (this.isPlaying) {
            this.isPlaying = false;
          }

          // Remove task playing.
          if (this.taskPlaying) {
            this.taskPlaying = null;
          }

          // Clear interval.
          clearInterval(this.interval);

          // Set elapsedTimeTotal, elapsedTime block.
          this.elapsedTimeTotal += value.time_total;
          this.elapsedTime = 0;

          let id = value.entity_type + '-' + value.entity_id;
          if (this.state.hasOwnProperty(id) && this.state[id].isPlaying && id !== 'work-time-block') {
            // Set elapsedTimeTotal, elapsedTime worktime.
            this.state[id].elapsedTimeTotal += value.time_total;

            if (this.state[id].historyData.length && this.state[id].historyData[0].hasOwnProperty('details')) {
              this.state[id].historyData[0].details.unshift(value);
            }
            this.state[id].isPlaying = false;
            this.state[id].taskPlaying = null;
            clearInterval(this.state[id].interval);
          }
        }
      }
    }).catch(error => {
      console.error("There was an error!", error);
    });
  },

  // Load data condition.
  async getDataCondition(limit) {
    let url = Drupal.url('api/work-time-list/' + limit) + '?_format=json';
    if (!Store.csrfToken) {
      await Store.csrfTokenLoader()
    }
    axios.get(url, {
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': Store.csrfToken
      }
    }).then((response) => {
      if (response.data.hasOwnProperty('projects') && response.data.projects.length) {
        this.projects = response.data.projects.reduce((itemMap, item) => {
          itemMap[item.id] = item.label;
          return itemMap;
        }, {});
      }
      if (response.data.hasOwnProperty('workTimes') && response.data.workTimes.length) {
        let total = 0;
        response.data.workTimes.forEach(item => {
          item.created = parseInt(item.created) * 1000;
          item.stopped = parseInt(item.stopped) * 1000;
          item.time_total = parseInt(item.time_total) * 1000;
          total += item.time_total
        })
        this.historyData = response.data.workTimes;
        this.elapsedTimeTotal = this.elapsedTime = total;
      }
      if (response.data.hasOwnProperty('playing') && response.data.playing) {
        response.data.playing.created = parseInt(response.data.playing.created) * 1000 || null;
        response.data.playing.stopped = parseInt(response.data.playing.stopped) * 1000 || null;
        response.data.playing.time_total = parseInt(response.data.playing.time_total) * 1000 || null;

        this.isPlaying = true;
        this.taskPlaying = response.data.playing;
        this.countUp(this.taskPlaying.created);

        // Check button formatter.
        let id = this.taskPlaying.entity_type + '-' + this.taskPlaying.entity_id;
        if (this.state.hasOwnProperty(id)) {
          this.state[id].isPlaying = true;
          this.state[id].taskPlaying = response.data.playing;
          this.state[id].countUp(this.taskPlaying.created);
        }
      }
    }).catch(error => {
      console.error("There was an error!", error);
    });
  },

  // Load total button.
  async loadTotalTime(ids) {
    if (!Store.csrfToken) {
      await Store.csrfTokenLoader()
    }
    // load time playing.
    axios.get(Drupal.url('api/work-time-list/entity_ids'),
      {
        params: {
          entity_ids: ids,
          _format: 'json'
        }
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }
    ).then((response) => {
      if (response.data) {
        for (let id in response.data.entity_ids) {
          this.state[id].elapsedTimeTotal = parseInt(response.data.entity_ids[id]) * 1000;
        }
      }
    }).catch(error => {
      console.error("There was an error!", error);
    });
  },

  // Counter time.
  countUp(time) {
    if (this.isPlaying) {
      this.interval = setInterval(() => {
        this.elapsedTime = Math.floor((Date.now() - time));
      }, 1000);
    }
  },
});

export default reactive_worktime;
