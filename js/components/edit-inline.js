import Store from "./store.js";
let templateEditInlineDefault = `
<button type="button" @click="toggle_show_edit()" class="btn btn-light"><i class="bi bi-pencil-square"></i></button>
<div class="box-confirm confirm-delete hc-100" :class="{'confirm-active': showEdit}">
  <div class="table-cell p-2 wc-25">
    <input type="number" class="form-control form-control-sm" min="1" :max="maxTotal" id="edit-total" @input="calEnd" v-model="total">
  </div>
  <div class="table-cell p-2 wc-25">
    <input type="time" class="form-control form-control-sm" id="edit-start" @input="calTotal" v-model="startTime">
  </div>
  <div class="table-cell p-2 wc-25">
    <input type="time" class="form-control form-control-sm" id="edit-end" @input="calTotal" v-model="endTime">
  </div>
  <div class="table-cell p-2 wc-25">
    <button ref="btnSubmit" class="btn btn-success bi bi-check-square" @click="edit()"></button>
    <button class="btn btn-danger bi bi-x-square" @click="toggle_show_edit()"></button>
  </div>
</div>
`;
const componentEdit = {
  template: typeof templateEditInline !== 'undefined' ? templateEditInline : templateEditInlineDefault,
  emits: ["editTask"],
  props: ['task', 'index_data', 'index_task'],

  data() {
    return {
      showEdit: false,
      editTask: {},
      timeout: null,
      startTime: '',
      endTime: '',
      total: '',
      date: ''
    }
  },

  computed: {
    maxTotal() {
      let date = this.date
      let startTime = this.startTime
      let objTime = new Date(date + " " + startTime + ":00")
      let objTimeLastDay = new Date(date + " 23:59:59")
      let rangeTime = Math.floor(Math.abs((objTimeLastDay.valueOf() - objTime.valueOf())) / 60000)
      return rangeTime
    }
  },

  methods: {
    toggle_show_edit() {
      this.showEdit = !this.showEdit;
    },
    async edit() {
      let task_id = this.task.id;
      let created = new Date(this.date + ' ' + this.startTime);
      let stopped = new Date(this.date + ' ' + this.endTime);
      let data = {
        created: created.valueOf() / 1000,
        stopped: stopped.valueOf() / 1000
      };
      let url = Drupal.url('api/work-time/' + task_id + '/edit') + '?_format=json';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader()
      }
      axios.patch(url, data,
        {
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': Store.csrfToken
          }
        })
        .then((response) => {
          this.editTask = {
            index_data: this.index_data,
            index_task: this.index_task,
            data: response.data.updated
          };
          this.$emit('editTask', this.editTask);
          this.showEdit = false
        })
        .catch(error => {
          console.error('There was an error!', error);
        });
    },
    calTotal(e) {
      this.$refs.btnSubmit.setAttribute('disabled', 'disabled');
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let startTime = new Date(this.date + ' ' + this.startTime + ':00');
        let endTime = new Date(this.date + ' ' + this.endTime + ':00');
        if (endTime < startTime) {
          let temp = this.startTime;
          this.startTime = this.endTime;
          this.endTime = temp;
        }
        let diff = Math.abs(endTime - startTime);
        this.total = diff / 60000;
        this.$refs.btnSubmit.removeAttribute('disabled');
      }, 1000);
    },
    calEnd() {
      let startTime = new Date(this.date + ' ' + this.startTime + ':00');
      startTime.setMinutes(startTime.getMinutes() + parseInt(this.total));
      this.endTime = startTime.toLocaleTimeString().slice(0, 5);
    },
  },
  mounted() {
    let dateFormatStart = new Date(this.task.created);
    this.date = Store.formatDate(dateFormatStart, ['year', 'month', 'day'], '-');
    this.startTime = Store.formatDate(dateFormatStart, ['hour', 'minute'], ':');
    if (this.task.stopped) {
      let dateFormatEnd = new Date(this.task.stopped);
      this.endTime = Store.formatDate(dateFormatEnd, ['hour', 'minute'], ':');
    }
    this.total = Math.ceil(parseInt(this.task.time_total) / 60000);
  },
};
export default componentEdit;
