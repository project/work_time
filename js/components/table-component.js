import Store from "./../components/store.js";
import cellComponent from "./../components/cell.js";

let templateBootstrapTableDefault = `
  <div class="row row-cols-lg-auto g-3 align-items-center">
    <div class="col-12">
      <input
        type="text"
        class="form-control mb-3"
        v-model="searchQuery"
        placeholder="Serch..."
      />
    </div>
    <button class="btn btn-success mb-3" @click="downloadCSV"><i class="bi bi-download"></i></button>
  </div>
  <table class="table table-bordered table-hover table-responsive caption-top wt-table">
    <caption v-if="caption">{{ caption }}</caption>
    <thead>
      <th class="text-center align-middle">{{ dataHeader.label_index }}</th>
      <th class="text-center align-middle" v-if="dataHeader.field_price">{{ dataHeader.field_price }}</th>
        <template v-for="cell_header in dataHeader.cells">
        <Cell :settings="settings" :leaveDays="leaveDays" :dataCell="cell_header"></Cell>
        </template>
      <th class="text-center align-middle">{{ dataHeader.total_over }}</th>
      <th class="text-center align-middle">{{ dataHeader.total_normal }}</th>
      <th class="text-center align-middle" v-if="dataHeader.total_price">{{ dataHeader.total_price }}</th>
    </thead>
    <tbody>
      <template v-for="(dataRow, indexRow) in dataTable.filter(row => filterRow(row))">
      <tr :data-group-sign="dataRow.id">
        <th class="text-center align-middle"><a :href="dataRow.link_detail">{{ dataRow.label }}</a></th>
        <th v-if="dataRow.price" class="text-center align-middle">{{ dataRow.price }}</th>
        <template v-for="(dataCell, index_cell) in dataRow.cells">
         <Cell :settings="settings" :leaveDays="leaveDays" :dataCell="dataCell"></Cell>
         </td>
        </template>
        <th class="text-center align-middle">{{ dataRow.total_over }}</th>
        <th class="text-center align-middle">{{ dataRow.total_normal }}</th>
        <th v-if="dataRow.total_price" class="text-center align-middle">{{ dataRow.total_price }}</th>
      </tr>
      </template>
    </tbody>
    <tfoot>
      <tr>
        <th class="text-center align-middle">{{ dataFooter.label_index }}</th>
        <th class="text-center align-middle" v-if="dataHeader.field_price">{{ dataFooter.field_price }}</th>
          <template v-for="cellFooter in dataFooter.cells">
          <Cell :settings="settings" :leaveDays="leaveDays" :dataCell="cellFooter"></Cell>
          </template>
        <th class="text-center align-middle">{{ dataFooter.total_over }}</th>
        <th class="text-center align-middle">{{ dataFooter.total_normal }}</th>
        <th class="text-center align-middle" v-if="dataHeader.total_price">{{ dataFooter.total_price }}</th>
      </tr>
    </tfoot>
  </table>
`;

export default {
  template: typeof templateBootstrapTable !== 'undefined' ? templateBootstrapTable : templateBootstrapTableDefault,
  props: {
    settings: Object,
    leaveDays: Array,
    rawData: Array,
    listRowGroup: Array,
    tableInfo: String,
  },
  data() {
    return {
      bsPopover: null,
      filter: '',
      filterMode: '',
      dataTable: [],
      dataHeader: {},
      dataFooter: {},
      caption: '',
      searchQuery: "",
    };
  },
  methods: {
    genTable(data) {

      // 1st cell header.
      this.dataHeader = {
        'label_index': this.settings.label_group[this.settings.group_record],
        'cells': [],
      }
      if (this.settings.price_reference) {
        this.dataHeader.field_price = Drupal.t('Price');
      }

      // 1st cell footer.
      this.dataFooter = {
        'label_index': Drupal.t('Total'),
        'cells': [],
      }
      if (this.settings.price_reference) {
        this.dataFooter.field_price = null;
      }

      let range_filter = Store.getRangeFilter(this.settings.filter_value, this.settings.options.filter);
      range_filter.forEach(date => {
        let y = date.getFullYear();
        let m = (date.getMonth() + 1).toString().padStart(2, '0');
        let d = date.getDate().toString().padStart(2, '0');
        let str_date = `${y}-${m}-${d}`;
        if (this.settings.options.filter === 'year') {
          str_date = `${y}-${m}`;
        }
        // Header cell.
        let cell_header = {
          'data': this.settings.options.filter === 'year' ? m : d,
          'sign': str_date,
        };
        if (this.tableInfo) {
          cell_header[this.settings.group_table] = this.tableInfo.id;
        }
        this.dataHeader.cells.push(cell_header);

        // Footer cell.
        let cell_footer = {
          'data': null,
          'sign': str_date,
        };
        if (this.tableInfo) {
          cell_footer[this.settings.group_table] = this.tableInfo.id;
        }
        this.dataFooter.cells.push(cell_footer);
      })

      // Last cell header.
      this.dataHeader.total_over = Drupal.t('Over');
      this.dataHeader.total_normal = Drupal.t('Normal');
      if (this.settings.price_reference) {
        this.dataHeader.total_price = Drupal.t('Total price');
      }
      // Last cell footer.
      this.dataFooter.total_over = null;
      this.dataFooter.total_normal = null;
      if (this.settings.price_reference) {
        this.dataFooter.total_price = null;
      }

      // Body cell.
      if (this.listRowGroup.length && this.settings.options.group_row) {
        let field_group = this.settings.options.group_row.split('.').pop();
        this.listRowGroup.forEach((item_group) => {

          // 1st body cell.
          let cell_body = {
            'label': item_group.label,
            'id': item_group.id,
            'cells': [],
            'total_over': null,
            'total_normal': null,
          };

          // Check settings link detail.
          if (item_group.hasOwnProperty('link_detail') && item_group.link_detail.length) {
            let url = item_group.link_detail.replace(/^\/|\/$/g, '').trim();
            cell_body.link_detail = Drupal.url(url);
          }

          // Check settings field_price.
          if (this.settings.price_reference) {
            let info_price = this.getInfoPrice(item_group.id);
            if (info_price) {
              cell_body.price = (info_price.price || 0) + info_price.suffix_price;
              cell_body.total_price = null;
            }
            else {
              cell_body.price = '--';
              cell_body.total_price = null;
            }
          }
          range_filter.forEach((date) => {
            let y = date.getFullYear();
            let m = (date.getMonth() + 1).toString().padStart(2, '0');
            let d = date.getDate().toString().padStart(2, '0');
            let str_date = `${y}-${m}-${d}`;
            if (this.settings.options.filter === 'year') {
              str_date = `${y}-${m}`;
            }
            let data_filter = data.filter((item_data) => item_data.date.includes(str_date) && item_data[field_group] == item_group.id);
            let item_cell = {
              'sign': str_date,
              'uid': null,
              'reference_id': null,
              'wid_over': null,
              'wid_normal': null,
              'normal': 0,
              'over': 0,
            };
            item_cell[this.settings.group_record] = item_group.id;
            if (this.tableInfo) {
              item_cell[this.settings.group_table] = this.tableInfo.id;
            }

            if (data_filter.length) {
              data_filter.forEach((item) => {
                if (item.payroll == 1) {
                  item_cell['wid_normal'] = item.id
                  item_cell.normal += parseInt(item.time_total);
                }
                else {
                  if (this.settings.options.worktime_confirm) {
                    item_cell['status'] = item.status
                  }
                  item_cell['wid_over'] = item.id
                  item_cell.over += parseInt(item.time_total);
                }
              })
            }
            cell_body.cells.push(item_cell);
          });
          this.dataTable.push(cell_body);
        });

        this.updateTotalData();
      }
    },
    totalData() {
      let total = {
        'normal': 0,
        'over': 0
      };
      let total_row = {};
      let total_col = {};
      this.dataTable.forEach((row) => {
        if (row.cells.length) {
          row.cells.forEach((item) => {
            if (total_row[row.id]) {
              total_row[row.id].normal += Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_row[row.id].over += Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
            else {
              total_row[row.id] = {};
              total_row[row.id].normal = Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_row[row.id].over = Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }

            if (total_col[item['sign']]) {
              total_col[item['sign']].normal += Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_col[item['sign']].over += Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
            else {
              total_col[item['sign']] = {};
              total_col[item['sign']].normal = Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_col[item['sign']].over = Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
          })
        }
      })

      for (const totalColKey in total_col) {
        total.normal += Math.ceil(parseInt(total_col[totalColKey].normal) / 3600) * 3600 || Math.ceil(parseInt(total_col[totalColKey].worktime) / 3600) * 3600 || 0;
        total.over += Math.ceil(parseInt(total_col[totalColKey].over) / 3600) * 3600 || 0;
      }

      return {
        'total': total,
        'total_row': total_row,
        'total_col': total_col
      };
    },
    updateTotalData() {
      let total_data = this.totalData();

      // Set total row.
      let total_price = 0;
      for (const row_id in total_data.total_row) {
        let item_row = this.dataTable.find(item => item.id == row_id);
        if (item_row) {
          item_row.total_over = parseInt(total_data.total_row[row_id].over) / 3600 || null;
          item_row.total_normal = parseInt(total_data.total_row[row_id].normal) / 3600 || null;
          if (item_row.hasOwnProperty('total_price')) {
            let info_price = this.getInfoPrice(row_id);
            if (info_price) {
              item_row.total_price = (parseInt(total_data.total_row[row_id].over) / 3600 + parseInt(total_data.total_row[row_id].normal) / 3600) * (parseInt(info_price.price) || 0) + info_price.suffix_price;
              total_price += (parseInt(total_data.total_row[row_id].over) / 3600 + parseInt(total_data.total_row[row_id].normal) / 3600) * (parseInt(info_price.price) || 0);
            }
            else {
              item_row.total_price = '--'
            }
          }
        }
      }

      // Set total footer.
      let total_over = 0;
      let total_normal = 0;
      for (const col_id in total_data.total_col) {
        let indexCol = this.dataFooter.cells.findIndex((item) => item.sign == col_id);
        if (indexCol >= 0) {
          this.dataFooter.cells[indexCol].data = (parseInt(total_data.total_col[col_id].normal) + parseInt(total_data.total_col[col_id].over)) / 3600 || null;
          total_over += parseInt(total_data.total_col[col_id].over);
          total_normal += parseInt(total_data.total_col[col_id].normal);
        }
      }

      if (this.dataFooter.hasOwnProperty('total_over')) {
        this.dataFooter.total_over = (parseInt(total_over) || 0) / 3600 || null;
      }
      if (this.dataFooter.hasOwnProperty('total_normal')) {
        this.dataFooter.total_normal = (parseInt(total_normal) || 0) / 3600 || null;
      }
      if (this.dataFooter.hasOwnProperty('total_price')) {
        this.dataFooter.total_price = total_price + this.settings.suffix_price;
      }
    },
    getInfoPrice(row_id) {
      let info_price = null;
      switch (this.settings.price_reference) {
        case this.settings.group_record:
          info_price = this.settings.list_price[this.settings.price_reference][row_id];
          break;
        case this.settings.group_table:
          info_price = this.settings.list_price[this.settings.price_reference][this.tableInfo.id];
          break;
      }
      return info_price;
    },
    filterRow(row) {
      if (!this.searchQuery) {
        return true;
      }
      const query = this.searchQuery.toLowerCase();
      const rowString = [
        row.label,
        row.price,
        row.total_over,
        row.total_normal,
        row.total_price,
        ...(row.cells.map((cell) => JSON.stringify(cell)) || []),
      ]
        .join(" ")
        .toLowerCase();
      return rowString.includes(query);
    },
    downloadCSV() {
     const headers = [
       this.dataHeader.label_index,
       this.dataHeader.field_price || "",
       ...this.dataHeader.cells.map(cell => cell.data || ""),
       this.dataHeader.total_over,
       this.dataHeader.total_normal,
       this.dataHeader.total_price || "",
     ];
     const rows = this.dataTable
     .filter(row => this.filterRow(row))
     .map(row => [
       row.label,
       row.price || "",
       ...row.cells.map(cell => this.displayCell(cell) || ""),
       row.total_over,
       row.total_normal,
       row.total_price || "",
     ]);
     const footer = [
       this.dataFooter.label_index,
       this.dataFooter.field_price || "",
       ...this.dataFooter.cells.map(cell => cell.data || ""),
       this.dataFooter.total_over,
       this.dataFooter.total_normal,
       this.dataFooter.total_price || "",
     ];
      const pageTitle = document.title || "work-time-export";
      let bom = "\uFEFF";
      const csvContent = bom + [
        headers.join(";"), // Header
        ...rows.map(row => row.join(";")), // Rows
        footer.join(";"), // Footer
      ].join("\n");
      const link = document.createElement("a");
      link.setAttribute("href", `data:text/csv;charset=utf-8,${encodeURIComponent(csvContent)}`);
      link.setAttribute("download", `${pageTitle}.csv`);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    },
    displayCell(dataCell) {
      if (dataCell.hasOwnProperty('data')) {
        return dataCell.data;
      }
      let hour = null;
      if (dataCell.worktime !== null) {
        hour = 0;
        hour += Math.ceil(parseFloat(dataCell.worktime) / 3600);
      }
      if (dataCell.normal !== null || dataCell.over !== null) {
        hour = 0;
        if (dataCell.normal) {
          hour += Math.ceil(parseFloat(dataCell.normal) / 3600);
        }
        if (dataCell.over) {
          hour += Math.ceil(parseFloat(dataCell.over) / 3600);
        }
      }
      if (hour == null) {
        return null;
      }
      return hour || null;
    },
  },
  components: {
    'Cell': cellComponent,
  },
  created() {
    this.genTable(this.rawData);
    // Get caption group table.
    if (this.tableInfo) {
      this.caption = this.settings.label_group[this.settings.group_table] + ': ' + this.tableInfo.label;
    }
  },
}
