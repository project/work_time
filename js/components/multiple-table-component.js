import tableComponent from "./table-component.js";

let templateMultipleTableDefault = `
  <template v-for="(dataTable, keyTable) in data">
    <tableComponent :settings="settings" :leaveDays="leaveDays" :rawData="dataTable" :listRowGroup="listRowGroup[keyTable]" :tableInfo="listTableInfo[keyTable]" />
  </template>
`;

export default {
  template: typeof templateMultipleTable !== 'undefined' ? templateMultipleTable : templateMultipleTableDefault,
  props: {
    settings: Object,
    leaveDays: Array,
  },
  data() {
    return {
      data: {},
      listTableInfo: {},
      listRowGroup: {},
    };
  },
  methods: {
    // Get all data worktime.
    getListGroupRecordInTable(data_group_table, list_group_record) {
      let list_group_record_in_table = [];
      data_group_table.forEach((record) => {
        let find_reference = list_group_record.find((item) => item.id == record[this.settings.group_record]);
        if (find_reference) {
          list_group_record_in_table.push(find_reference);
        }
      })
      return Array.from(new Set(list_group_record_in_table));
    },
    genTable(data) {
      if (data.length && this.settings.list_group_table.length) {

        this.settings.list_group_table.forEach(item_group_table => {
          // Get listTableInfo.
          this.listTableInfo[item_group_table.id] = item_group_table;

          // Get data worktime by group table.
          let data_group_table = data.filter((item) => item[this.settings.group_table] == item_group_table.id);
          this.data[item_group_table.id] = data_group_table;

          let list_row_group = this.getListGroupRecordInTable(data_group_table, this.settings.list_group_record);
          this.listRowGroup[item_group_table.id] = list_row_group;

        })
      }
    },
  },
  components: {
    'tableComponent': tableComponent,
  },
  async created() {
    this.genTable(this.settings.data);
  },
}
