import Edit from "./edit.js";
import Delete from "./delete.js";
import Store from "./store.js";
import reactiveWorktime from "./reactive-worktime.js";

// Get Bootstrap popover tootip.
const { Popover, Tooltip } = bootstrap;
let _placeholderTxt = document.title.split(" | ")[0];
let _titleTxt = Drupal.t("What are you working on?");
let templateHistoryBlock = `
<button type="button" class="btn" :class="{'bi bi-clock-history': !bsPopover, 'bi-x-circle': bsPopover}" ref="btnToggle" role="button" @click="showPopupHistory($event)" data-bs-toggle="popover" data-bs-trigger="focus"></button>
<div class="d-none">
  <div class="popup-history" v-if="showHistory" ref="history">
    <a class="btn-close position-absolute" role="button" aria-label="Close" @click="close" ref="btnClose"></a>
    <div class="row mb-1">
      <span class="col-auto" id="txt_project">${_titleTxt}</span>
      <div class="col-auto">
        <input type="text" class="form-control form-control-sm" list="work-time-project" id="block_project" v-model="txt_project" title="${Drupal.t('Do not remove (x)')}"/>
      </div>
    </div>
    <div class="input-group">
      <input type="text" placeholder="${_placeholderTxt}" class="form-control" title="${_titleTxt}" v-model="label" />
      <button @click="play($event)" type="button" class="btn btn-outline-success" title="${Drupal.t('Play')}">
        <i class="bi bi-save-fill"></i>
      </button>
    </div>
    <div class="my-2 d-flex justify-content-between"><span class="fw-normal">{{caption}}</span><span class="fw-normal">{{ currentTotal }}</span></div>
    <div class="content-popover-worktime-block">
      <table class="table table-striped table-hover caption-top mt-0 wt-table">
        <thead class="position-sticky top-0">
          <tr>
            <th>${Drupal.t('Date')}</th>
            <th>${Drupal.t('Time')}</th>
            <th>${Drupal.t('Label')}</th>
            <th>${Drupal.t('Project')}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr v-if="taskPlaying">
            <td class="text-danger">{{ displayDate(taskPlaying.created) }}</td>
            <td class="text-danger">{{ timeUpPlaying }}</td>
            <td class="text-danger">{{ taskPlaying.label }}</td>
            <td class="text-danger">{{ taskPlaying.project }}</td>
            <td class="wc-25 text-warning text-center">
              <button class="btn btn-outline-danger" :class="{'bi bi-stop-circle': !taskPlaying.time_total }" @click="stop($event)"></button>
            </td>
          </tr>
          <tr v-for="(task, indexTask) in historyData" :key="task.id" class="position-relative">
            <td>{{ displayDate(task.created) }}</td>
            <td>{{ displayTimeTotal(task.time_total) }}</td>
            <td>{{ task.label }}</td>
            <td>{{ task.project }}</td>
            <td class="wc-25 text-center">
              <edit @saveEditTask="saveTask" :task="task" :index_task="indexTask" v-if="this.uid == task.uid && task.time_total" />
              <delete @removeTask="removeTask" :task="task" :index_task="indexTask" v-if="this.uid == task.uid && task.time_total" />
             </td>
          </tr>
        </tbody>
      </table>
    </div>
    <datalist id="work-time-project">
        <option v-for="project in projects" :data-value="project.nid" :value="project.title+' ('+project.nid+')'">
    </datalist>
  </div>
</div>
`;
const history = {
  template: templateHistoryBlock,
  props: ['uid', 'entity', 'mode_filter'],
  data() {
    return {
      showHistory: false,
      bsPopover: null,
      showPopupEdit: false,
      label: '',
      projects: [],
      txt_project: '',
      caption: '',
    }
  },
  computed: {
    currentTotal() {
      if (reactiveWorktime.isPlaying) {
        return Store.formatTime(reactiveWorktime.elapsedTime + reactiveWorktime.elapsedTimeTotal, Store.timeFormatFull.hour, Store.timeFormatFull.minute, Store.timeFormatFull.second);
      }
      return Store.formatTime(reactiveWorktime.elapsedTimeTotal, Store.timeFormatFull.hour, Store.timeFormatFull.minute, null);
    },
    timeUpPlaying() {
      return Store.formatTime(reactiveWorktime.elapsedTime, Store.timeFormatStandard.hour, Store.timeFormatStandard.minute, Store.timeFormatStandard.second);
    },
    taskPlaying() {
      return reactiveWorktime.taskPlaying
    },
    historyData() {
      return reactiveWorktime.historyData;
    }
  },
  methods: {
    displayDate(time) {
      let date = new Date(time);
      let options = {day: 'numeric', month: 'short'};
      return date.toLocaleString(Store.language, options);
    },
    displayTimeTotal(time) {
      return Store.formatTime(time, Store.timeFormatFull.hour, Store.timeFormatFull.minute, null, ':', 'compact')
    },
    removeTask(e) {
      reactiveWorktime.historyData.splice(e.index_task, 1);
    },
    close(){
      this.$refs.btnToggle.click();
    },
    async play(event) {

      const match = this.txt_project.match(/\((.*?)\)/);
      let reference_id = false;
      if (match && match[1] != undefined) {
        reference_id = match[1] ?? false;
      }

      let data = this.entity;
      if (this.label) {
        data.label = this.label;
      }
      if (reference_id) {
        data.referenceId = reference_id;
      }
      if (reactiveWorktime.isPlaying) {
        await reactiveWorktime.stop();
      }
      reactiveWorktime.play(null, data);
      // this.$parent.$refs.btnTimeBlock.click();
      this.$refs.btnToggle.click();
    },

    async stop() {
      await reactiveWorktime.stop();
    },

    showPopupHistory(event) {
      this.showHistory = true;
      if (!this.bsPopover) {
        this.bsPopover = new Popover(event.target, {
          trigger: 'click',
          title: document.title.split(" | ")[0],
          html: true,
          content: () => {
            return this.$refs.history;
          }
        });
        this.bsPopover.toggle();
      } else {
        this.bsPopover = null;
      }
    },
    editTask(event, task, indexData, indexDetail) {
      this.showPopupEdit = true;
    },
    saveTask(e) {
      e.data.created = parseInt(e.data.created) * 1000;
      e.data.stopped = parseInt(e.data.stopped) * 1000;
      e.data.time_total = parseInt(e.data.time_total) * 1000;
      this.historyData[e.index_task] = e.data;
    },
    async remove(task, indexData, indexDetail) {
      let url = Drupal.url('api/work-time/' + task.id + '/' + this.entity_field) + '?_format=json';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader();
      }
      axios.delete(url, {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }).then( () => {
        if(this.historyData){
          this.historyData[indexData].details.splice(indexDetail, 1);
        }
      }).catch(error => {
        console.error('There was an error!', error);
      });
    },
  },
  async mounted(){
    let now = new Date();
    switch (this.mode_filter) {
      case 'week':
        let currentDate = new Date();
        let startDate = new Date(currentDate.getFullYear(), 0, 1);
        let days = Math.floor((currentDate.valueOf() - startDate.valueOf()) / (24 * 60 * 60 * 1000));

        let weekNumber = Math.ceil(days / 7);
        this.caption = Drupal.t('Week') + ' ' + weekNumber + ', ' + Drupal.t(now.toLocaleString(Store.language, {
          year: 'numeric',
        }));
        break

      case 'month':
        this.caption = now.toLocaleString('default', { year: 'numeric', month: 'long' });
        break;

      default:
        this.caption = now.toLocaleString('default', { year: 'numeric', month: 'short' });
    }
    let url = drupalSettings.work_time_block.url_projects;
    if (url.length) {
      url = url.replace(/^\/|\/$/g, '').trim();
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader();
      }
      axios.get(Drupal.url(url), {
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }).then((response) => {
        if (typeof (response.data) !== 'undefined' && response.data.length) {
          this.projects = response.data;
        }
      }).catch(error => {
        console.error("There was an error!", error);
      });
    }
  },
  components:{
    'edit': Edit,
    'delete': Delete
  }
};

export default history;
