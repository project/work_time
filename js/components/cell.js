import Store from "./../components/store.js";

// Get Bootstrap popover.
const {Popover, Tooltip} = bootstrap;
let templateCellDefault = `
  <td ref="cell" class="text-center align-middle" :class="cellClass" @click="showConfirm">{{ displayCell(dataCell) }}</td>
  <div v-if="isConfirm" class="d-none">
    <div ref="popoverConfirm">
      <p>${Drupal.t('Over time (hour): ')} {{ hourRound(dataCell.over) }}</p>
      <button @click="confirmHandle" class="btn btn-success me-2">${Drupal.t('Accept')}</button>
      <button @click="showConfirm" class="btn btn-danger">${Drupal.t('Close')}</button>
    </div>
  </div>
`;
export default {
  template: typeof templateCell !== 'undefined' ? templateCell : templateCellDefault,
  props: {
    settings: Object,
    leaveDays: Array,
    dataCell: Object,
  },
  data() {
    return {
      bsPopover: null,
    }
  },
  computed: {
    cellClass() {
      let result_class = this.settings.options.filter === 'year' ? {} : Store.styleClass(this.dataCell.sign, this.settings.options.holidays, this.settings.options.work_day, this.leaveDays, this.dataCell.uid || null);

      if (this.dataCell.hasOwnProperty('data')) {
        result_class['fw-bolder'] = true;
      }
      else {
        result_class['text-secondary'] = true;
        result_class['wt-w-cell'] = true;
        result_class['wt-cell-pick'] = true;
        if (parseFloat(this.dataCell.worktime)) {
          result_class['text-secondary'] = true;
        }
        if (parseFloat(this.dataCell.normal)) {
          result_class['text-secondary'] = false;
          result_class['text-success'] = true;
        }
        if (parseFloat(this.dataCell.over)) {
          result_class['text-secondary'] = false;
          result_class['text-success'] = false;
          result_class['text-danger'] = true;
          if (parseInt(this.dataCell.status)) {
            result_class['wt-shadow-confirm'] = true;
          }
        }
        if (parseFloat(this.dataCell.normal) && parseFloat(this.dataCell.over)) {
          result_class['text-secondary'] = false;
          result_class['text-danger'] = false;
          result_class['text-success'] = false;
          result_class['text-warning'] = true;
        }
      }
      return result_class
    },
    isConfirm() {
      if (this.settings.options.worktime_confirm && this.dataCell.over && this.dataCell.status == 0) {
        return true;
      }
      return false;
    }
  },
  methods: {
    hourRound(time) {
      if (time == null) {
        return null;
      }
      return Math.ceil(parseFloat(time) / 3600) || 0;
    },

    displayCell(dataCell) {
      if (dataCell.hasOwnProperty('data')) {
        return dataCell.data;
      }
      let hour = null;
      if (dataCell.worktime !== null) {
        hour = 0;
        hour += Math.ceil(parseFloat(dataCell.worktime) / 3600);
      }
      if (dataCell.normal !== null || dataCell.over !== null) {
        hour = 0;
        if (dataCell.normal) {
          hour += Math.ceil(parseFloat(dataCell.normal) / 3600);
        }
        if (dataCell.over) {
          hour += Math.ceil(parseFloat(dataCell.over) / 3600);
        }
      }
      if (hour == null) {
        return null;
      }
      return hour || null;
    },

    showConfirm($event) {
      if (this.bsPopover) {
        this.bsPopover.toggle()
        return;
      }
      if (this.$refs.popoverConfirm && $event.target) {
        this.bsPopover = new Popover($event.target, {
          trigger: 'focus',
          title: `${Drupal.t('Confirm these business hours')}`,
          html: true,
          content: () => {
            return this.$refs.popoverConfirm;
          }
        });
        this.bsPopover.toggle();
      }
    },

    async confirmHandle($event) {
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader();
      }
      axios.patch(Drupal.url('api/work-time/0/sheet_confirm') + '?_format=json',
        {
          wid_over: this.dataCell.wid_over,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': Store.csrfToken
          }
        }).then((response) => {
          if (response.data['confirmed']) {
            this.dataCell.status = 1;
            this.bsPopover.dispose();
            this.setTooltip();
          }
        }).catch(error => {
          console.error("There was an error!", error);
        });
    },
    setTooltip() {
      let content_tooltip = '';
      if (this.dataCell.normal) {
        content_tooltip += `<p class="mb-0">${Drupal.t('Normal hour: @hour', {'@hour': Math.ceil(parseInt(this.dataCell.normal) / 3600)})}</p>`
      }
      if (this.dataCell.over) {
        content_tooltip += `<p class="mb-0">${Drupal.t('Over hour: @hour', {'@hour': Math.ceil(parseInt(this.dataCell.over) / 3600)})}</p>`
      }

      if (content_tooltip.length) {
        this.$refs.cell.setAttribute('title', content_tooltip);
        new Tooltip(this.$refs.cell, {
          html: true,
          delay: { "show": 300, "hide": 100 }
        })
      }
    }
  },
  mounted() {
    if (!this.$refs.popoverConfirm) {
      this.setTooltip()
    }
  }
};
