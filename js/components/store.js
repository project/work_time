const store = Vue.reactive({
  /* Variable */
  csrfToken: '',
  language: navigator.language,

  timeFormatFull: {
    hour: {format: 'HH', prefix: null, suffix: 'h'},
    minute: {format: 'MM', prefix: null, suffix: 'm'},
    second: {format: 'SS', prefix: null, suffix: 's'},
  },
  timeFormatStandard: {
    hour: {format: 'HH', prefix: null, suffix: null},
    minute: {format: 'MM', prefix: null, suffix: null},
    second: {format: 'SS', prefix: null, suffix: null},
  },
  timeFormatShort: {
    hour: {format: 'H', prefix: null, suffix: null},
    minute: {format: 'M', prefix: null, suffix: null},
    second: {format: 'S', prefix: null, suffix: null},
  },
  timeFormatShortS: {
    hour: {format: 'H', prefix: null, suffix: 'h'},
    minute: {format: 'M', prefix: null, suffix: 'm'},
    second: {format: 'S', prefix: null, suffix: 's'},
  },

  /* Function */

  // Load token.
  async csrfTokenLoader() {
    await axios.get(Drupal.url('session/token')).then((response) => {
      this.csrfToken = response.data;
    }).then(() => {

    });
  },

  // Display time.
  formatTime(time, format_hour = {format: 'HH', prefix: null, suffix: null}, format_minute = {format: 'MM', prefix: null, suffix: null}, format_second = {format: 'SS', prefix: null, suffix: null}, separator = ':', style = 'full') {
    time = parseInt(time) / 1000;
    if (!time) {
      return '';
    }

    let display_hour = format_hour && format_hour.format ? true : false;
    let display_minute = format_minute && format_minute.format ? true : false;
    let display_second = format_second && format_second.format ? true : false;

    const s = Math.floor(time % 60);
    const m = Math.floor(time / 60) % 60;
    const h = Math.floor(time / 3600);

    let second = '';
    let minute = '';
    let hour = '';
    let timeArray = [];

    // Format second.
    if (display_second) {
      second = /^SS$/.test(format_second.format) ? s.toString().padStart(2, '0') : s.toString();
    }
    if (format_second && format_second.prefix) {
      second = format_second.prefix + second;
    }
    if (format_second && format_second.suffix) {
      second += format_second.suffix;
    }
    if (display_second) {
      timeArray.push(second);
    }

    // Format minute.
    if (display_minute) {
      minute = /^MM$/.test(format_minute.format) ? m.toString().padStart(2, '0') : m.toString();
    }
    if (format_minute && format_minute.prefix) {
      minute = format_minute.prefix + minute;
    }
    if (format_minute && format_minute.suffix) {
      minute += format_minute.suffix;
    }
    if (display_minute) {
      timeArray.push(minute);
    }

    // Format hour.
    if (display_hour) {
      hour = /^HH$/.test(format_hour.format) ? h.toString().padStart(2, '0') : h.toString();
    }
    if (format_hour && format_hour.prefix) {
      hour = format_hour.prefix + hour;
    }
    if (format_hour && format_hour.suffix) {
      hour += format_hour.suffix;
    }
    if (display_hour) {
      timeArray.push(hour);
    }

    timeArray.reverse();
    if (style === 'compact') {
      if (h === 0 && display_hour && display_second) {
        timeArray.shift();
      }
      else if (display_second && display_hour) {
        timeArray.pop();
      }
    }

    return timeArray.join(separator);
  },

  // Get date format.
  formatDate(date, format = [], separation = '-', timeZone = 'default') {
    let formatResult = [];
    if (format.length) {
      let dateArr = {
        year: date.getFullYear().toString(),
        month: (date.getMonth() + 1).toString().padStart(2, '0'),
        day: date.getDate().toString().padStart(2, '0'),
        hour: date.getHours().toString().padStart(2, '0'),
        minute: date.getMinutes().toString().padStart(2, '0'),
        second: date.getSeconds().toString().padStart(2, '0'),
      };

      format.forEach(function (element) {
        formatResult.push(dateArr[element])
      })
    }

    return formatResult.join(separation);
  },

  // If date is weekend, holiday, leave day will change color background.
  styleClass(dateString, holidays = [], work_day = [], leave_days = [], uid = null) {
    const date = new Date(dateString);
    const dayOfWeekIndex = date.getDay();
    const day_long = dateString;
    const day_short = `${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
    let css = {};
    if (dayOfWeekIndex in work_day) {
      css['wt-weekend'] = true;
    }
    if (holidays.includes(day_long) || holidays.includes(day_short)) {
      css['wt-holiday'] = true
    }
    if (uid) {
      let day_off = leave_days.find((item) => item.date == day_long && item.uid == uid);
      if (day_off) {
        switch (parseInt(day_off.type)) {
          case 2:
            css['wt-half-day'] = true;
            break;

          case 3:
            css['wt-tardiness-leave-early'] = true;
            break;

          case 4:
            css['wt-vacation'] = true;
            break;

          default:
            css['wt-leave'] = true;
        }
      }
    }
    return css;
  },

  // Return array date in week/month or months in year.
  getRangeFilter(filter, mode_filter = 'month') {
    let [year, rest] = filter.split('-');

    let date_start = null;
    let date_end = null
    switch (mode_filter) {
      case 'year':
        date_start = new Date(year, 0, 1);
        date_end = new Date(year, 11, 31);
        break;

      case 'week':
        // Create a Date object with the first day of the year
        let week_index = parseInt(rest.replace('W', ''), 10)
        let date_first = new Date(year, 0, 1);

        // Calculate the first day of the week
        let dayNum = date_first.getDay();
        let diff = --week_index * 7 + (dayNum <= 4 ? 1 - dayNum : 8 - dayNum);
        date_first.setDate(date_first.getDate() + diff);

        date_start = new Date(date_first);
        date_end = new Date(date_first.setDate(date_first.getDate() + 6));
        break;

      default:
        let month = parseInt(rest);
        date_end = new Date(year, month, 0);
        date_start = new Date(year, (month - 1), 1);
    }

    let date_range = [];
    if (date_start && date_end && date_start < date_end) {
      if (mode_filter !== 'year') {
        while (date_start <= date_end) {
          date_range.push(new Date(date_start));
          date_start.setDate(date_start.getDate() + 1);
        }
      }
      else {
        while (date_start <= date_end) {
          date_range.push(new Date(date_start));
          date_start.setMonth(date_start.getMonth() + 1);
        }
      }
    }
    return date_range;
  },
  getRangeWeek(start, end) {
    let result = {};
    if (start.valueOf() <= end.valueOf()) {
      while (start.valueOf() < end.valueOf()) {
        let key = this.getISOWeekNumber(start);
        if (!result[key]) {
          result[key] = this.getRangeFilter(key, 'week');
        }
        start.setDate(start.getDate() + 1);
      }
    }
    return result;
  },
  getISOWeekNumber(date) {
    let target = new Date(date);
    let dayNumber = (target.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNumber + 3); // 3 here is for Monday
    let firstSunday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() !== 4) {
      target.setMonth(0, 1 + ((4 - target.getDay() + 7) % 7));
    }
    let weekNumber = 1 + Math.ceil((firstSunday - target) / (7 * 24 * 3600 * 1000));
    let year = target.getFullYear();
    return year + "-W" + weekNumber.toString().padStart(2, '0');
  },
  // Total col.
  totalCol(data, date, group_table, id_group_table, group_row) {
    let total_cell = {
      'worktime': {},
      'sheet': {}
    };
    let data_handle = data.filter((item) => {
      if (item.date.includes(date) && item[group_table] == id_group_table) {
        return item;
      }
    })
    if (data_handle.length) {
      data_handle.forEach((item) => {
        if (item.bundle == 'attendance') {
          if (total_cell.sheet[item[group_row]]) {
            total_cell.sheet[item[group_row]] += parseInt(item.time_total);
          }
          else {
            total_cell.sheet[item[group_row]] = parseInt(item.time_total);
          }
        }
        if (item.bundle == 'work_time') {
          if (total_cell.worktime[item[group_row]]) {
            total_cell.worktime[item[group_row]] += parseInt(item.time_total);
          }
          else {
            total_cell.worktime[item[group_row]] = parseInt(item.time_total);
          }
        }
      })
    }

    let result_sheet = 0;
    for (const totalKey in total_cell.sheet) {
      result_sheet += Math.ceil(total_cell.sheet[totalKey] / 3600);
    }

    let result_worktime = 0;
    for (const totalKey in total_cell.worktime) {
      result_worktime += Math.ceil(total_cell.worktime[totalKey] / 3600);
    }

    return result_sheet || result_worktime || null;
  },

  // Total row.
  totalRow(data, group_table, id_group_table, group_row, id_group_row, price = null) {
    let total_row = {
      'worktime': {},
      'sheet': {}
    };
    let data_handle = data.filter((item) => {
      if (item[group_table] == id_group_table && item[group_row] == id_group_row) {
        return item;
      }
    })
    if (data_handle.length) {
      data_handle.forEach((item) => {
        if (total_row.worktime[item.date + '_' + item[group_row]]) {
          total_row.worktime[item.date + '_' + item[group_row]] += parseInt(item.time_total);
        }
        else {
          total_row.worktime[item.date + '_' + item[group_row]] = parseInt(item.time_total);
        }
      })
    }

    let result_worktime = 0;
    for (const totalKey in total_row.worktime) {
      result_worktime += Math.ceil(total_row.worktime[totalKey] / 3600);
    }

    let hours_result = result_worktime || null;

    let result_price = 0;
    if (hours_result && price) {
      result_price = hours_result * parseFloat(price);
    }
    return {
      'hours': hours_result,
      'price': result_price
    }
  },
  // Display date.
  displayDate(time) {
    let date = new Date(time * 1000);
    let y = date.getFullYear();
    let m = date.getMonth().toString().padStart(2, '0');
    let d = date.getDate().toString().padStart(2, '0');
    let h = date.getHours();
    let i = date.getMinutes().toString().padStart(2, '0');
    let s = date.getSeconds().toString().padStart(2, '0');
    let timeString = `${h}:${i}`;
    return timeString;
  },
})

export default store;
