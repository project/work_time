import Store from './components/store.js';

let templateDistanceDefault = `
  <div> ${Drupal.t('Accuracy: ')} <span v-html="accuracy"></span></div>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>${Drupal.t('Checkin status')}</th>
        <th>${Drupal.t('Project')}</th>
        <th>${Drupal.t('Distance')}</th>
        <th>${Drupal.t('Action')}</th>
      </tr>
    </thead>
    <tbody>
      <template v-for="(item, index) in data">
      <tr>
        <td>
          <p v-if="item.checkin" class="mb-0 "><i class="bi bi-check-circle-fill text-success"></i> {{ item.checkin }}</p>
          <p v-if="item.checkout" class="mb-0"><i class="bi bi-check-circle-fill text-danger"></i> {{ item.checkout }}</p>
        </td>
        <th scope="row" v-html="item.label"></th>
        <td>{{ formatNumber(item.distance) }} {{ item.unit }}</td>
        <td>
        <button v-if="item.checkin && !item.checkout" @click="checkinHandle(item.id, $event)" class="btn btn-danger">Checkout</button>
        <button v-else @click="checkinHandle(item.id, $event)" class="btn btn-success">Checkin</button>
        </td>
      </tr>
      <template>
    </tbody>
  </table>
`;
let data_worktime_distance = drupalSettings.worktime_distance.result_data;
let options_distance = drupalSettings.worktime_distance.distance;
let checkin_only = drupalSettings.worktime_distance.checkin_only;

const App = {
  template: typeof templateDistance === 'undefined' ? templateDistance : templateDistanceDefault,
  data() {
    return {
      locales: navigator.language,
      accuracy: 'Low',
      data: [],
      checkin: [],
    }
  },
  methods: {
    async checkinHandle(id, $event) {
      $event.target.setAttribute("disabled", "");
      let row = data_worktime_distance.find(item_data => item_data.id == id);
      let url = 'api/work-time/'+id+'/checkin';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader();
      }
      axios.patch(Drupal.url(url), {},
        {
          params: {
            entity_type: row ? row.entity_type : '',
            checkin_only: checkin_only,
            _format: 'json'
          },
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': Store.csrfToken
          }
        }
      ).then((response) => {
        response.data.forEach(item_result => {
          let find = this.data.find((item_data) => item_data.id == item_result.id)
          if (find) {
            find.checkin = item_result.checkin ? Store.displayDate(item_result.checkin) : null
            find.checkout = item_result.checkout ? Store.displayDate(item_result.checkout) : null
          }
        })
      }).catch(error => {
        console.error("There was an error!", error);
      }).finally (() => {
        $event.target.removeAttribute("disabled");
      });
    },
    formatNumber(num) {
      num = num.toFixed(0);
      return num.toLocaleString(this.locales)
    },
    getLocation() {
      const successCallback = (position) => {
        if (position.coords.accuracy < 22) {
          this.accuracy = Drupal.t("High");
        }
        else {
          this.accuracy = Drupal.t("Low");
        }
        let data = [];
        data_worktime_distance.forEach((item) => {
          let distance = this.haversine(position.coords.latitude, position.coords.longitude, item.latitude, item.longitude, item.unit)
          if (distance <= options_distance) {
            item.distance = distance;
            data.push(item);
          }
        })
        if (data.length) {
          data = data.sort((a, b) => a.distance - b.distance)
        }
        this.data = data;
      };

      const errorCallback = (error) => {
        return;
      };

      const noPermission = () => {
        let linkAndroid = "<a href='https://support.google.com/android/answer/3467281?hl=" + navigator.language + "'>"+ Drupal.t('Android devices') +"</a>";
        let linkIphone = "<a href='https://support.apple.com/" + navigator.language + "/102647'>" + Drupal.t('Apple devices') + "</a>";
        this.accuracy = Drupal.t("Permission to share location has not been granted. " + linkIphone + ', ' + linkAndroid);
      };

      const noServiceLocation = () => {
        this.accuracy = Drupal.t("The browser does not support geolocation.");
      };

      let options = {
        enableHighAccuracy: true,
        maximumAge: 5000,
        timeout: 5000,
      };
      if (navigator.geolocation) {
        if (navigator.permissions) {
          navigator.permissions.query({name:'geolocation'}).then(function(result) {
            if (result.state === 'granted' || result.state === 'prompt') {
              navigator.geolocation.watchPosition(successCallback, errorCallback, options);
            }
            else if (result.state === 'denied') {
              noPermission()
            }
          });
        }
        else {
          navigator.geolocation.watchPosition(successCallback, errorCallback, options)
        }
      }
      else {
        noServiceLocation();
      }
    },
    haversine(lat1, lon1, lat2, lon2, unit) {
      let R = 6371000;
      switch (unit) {
        case 'miles':
          R = 3958.8;
          break;

        case 'kilometers':
          R = 6371;
          break;

        default:
          R = 6371000;
      }
      let dLat = this.deg2rad(lat2 - lat1);
      let dLon = this.deg2rad(lon2 - lon1);
      let a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      let d = R * c;
      return d;
    },
    deg2rad(deg) {
      return deg * (Math.PI/180)
    }
  },
  async mounted() {
    if (!Store.csrfToken) {
      await Store.csrfTokenLoader();
    }
    await axios.get(Drupal.url('api/work-time-list/checkin'),
      {
        params: {
          _format: 'json'
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }
    ).then(response => {
      if (response.data) {
        response.data.forEach(item_result => {
          let find = data_worktime_distance.find((item_data) => item_data.id == item_result.id)
          if (find) {
            find.checkin = item_result.checkin ? Store.displayDate(item_result.checkin) : null
            find.checkout = item_result.checkout ? Store.displayDate(item_result.checkout) : null
          }
        })
      }
    }).catch(error => {

    })
    this.getLocation();
  }
};

let app = Vue.createApp(App);
app.mount('#work-time-distance');
