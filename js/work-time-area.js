(function (Drupal, $, once) {
  $('input.work-time-filter').on('change', function () {
    let inputValue = $(this).val();
    let currentUrl = window.location.href;
    let baseUrl = currentUrl.split('?')[0];
    let newUrl = baseUrl + '?date=' + inputValue;
    window.location.href = newUrl;
  });
  let date = getParameterByName('date');
  $('input.work-time-filter').val(date);
}(Drupal, jQuery, once));

/**
 * Get value from query.
 */
function getParameterByName(name) {
  let url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
  let results = regex.exec(url);
  if (!results) {
    return null;
  }
  if (!results[2]) {
    return '';
  }
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
