import History from "./components/history-block.js";
import Store from "./components/store.js";
import reactiveWorktime from "./components/reactive-worktime.js";

(function (Drupal, drupalSettings, once) {
  Drupal.behaviors.workTime = {
    attach(context) {
      once('worktimeBlock', '.btn-work-time-block', context).forEach((element) => {
        let id = element.getAttribute('id');
        let uid = drupalSettings.user.uid;
        let label = element.querySelector('button[ref]').dataset.label || null;
        let entityId = element.querySelector('button[ref]').dataset.entityId || null;
        let entityType = element.querySelector('button[ref]').dataset.entityType || null;
        let entityField = element.querySelector('button[ref]').dataset.entityField || null;
        let entityBundle = element.querySelector('button[ref]').dataset.entityBundle || null;
        let referenceId = element.querySelector('button[ref]').dataset.referenceId || null;
        let referenceType = element.querySelector('button[ref]').dataset.referenceType || null;
        let referenceField = element.querySelector('button[ref]').dataset.referenceField || null;
        let limit = drupalSettings.work_time_block.limit || 'week';
        let app = [];
        const App = {
          data() {
            return {
              id: id,
              uid: uid,
              entity: {
                label: label,
                entityId: entityId,
                entityType: entityType,
                entityField: entityField,
                referenceId: referenceId,
                referenceType: referenceType,
                referenceField: referenceField,
              },
              isPlaying: false,
              elapsedTime: 0,
              elapsedTimeTotal: 0,
              interval: null,
              modeFilter: limit,
            }
          },
          computed: {
            timeUpBlock() {
              this.isPlaying = reactiveWorktime.isPlaying;
              this.taskPlaying = reactiveWorktime.taskPlaying;
              this.interval = reactiveWorktime.interval;
              if (this.isPlaying) {
                return Store.formatTime(reactiveWorktime.elapsedTime, Store.timeFormatStandard.hour, Store.timeFormatStandard.minute, Store.timeFormatStandard.second, ':', 'compact');
              }
              return Store.formatTime(reactiveWorktime.elapsedTimeTotal, Store.timeFormatStandard.hour, Store.timeFormatStandard.minute, Store.timeFormatStandard.second, ':', 'compact');
            }
          },
          methods: {
            // Counter time.
            countUp(time) {
              this.interval = setInterval(() => {
                reactiveWorktime.elapsedTimeTotal += 1000;
                reactiveWorktime.elapsedTime = Math.floor((Date.now() - time));
              }, 1000);
            },

            // Play/stop.
            async togglePlay() {
              let isActionPlay = true;
              if (reactiveWorktime.state.hasOwnProperty(id) && reactiveWorktime.state[id].isPlaying) {
                isActionPlay = false;
              }

              let isActionStop = true;
              if (reactiveWorktime.state.hasOwnProperty(id) && !reactiveWorktime.isPlaying) {
                isActionStop = false;
              }

              if (isActionStop) {
                await reactiveWorktime.stop();
              }

              if (isActionPlay) {
                reactiveWorktime.play(id, this.entity);
              }
            }
          },
          mounted() {
            if (this.label === null) {
              this.label = document.title.split(" | ")[0];
            }
          },
          created() {
            reactiveWorktime.getDataCondition("playing");
          },
          components: {
            'history': History
          },
        };
        app[id] = Vue.createApp(App);
        reactiveWorktime.state[id] = app[id].mount(element);
        reactiveWorktime.getDataCondition(limit);
      });
    }
  };
}(Drupal, drupalSettings, once));
