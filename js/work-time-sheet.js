import TimePickerComponent from './workTimeSheet/TimePickerComponent.js';
import TableComponent from './workTimeSheet/TableComponent.js';

let templateWorkTimeSheetDefault = `
  <div>
    <TimePickerComponent @pickChanged="updatePick"/>
    <TableComponent :uid="uid" :settings="settings" :typePick="typePick" :hourPick="hourPick" :hourMaxNormal="hourMaxNormal" :hourMaxOver="hourMaxOver" :hourMaxOverSpecial="hourMaxOverSpecial"/>
  </div>
`;

let App = Vue.createApp({
  template: typeof templateWorkTimeSheet !== 'undefined' ? templateWorkTimeSheet : templateWorkTimeSheetDefault,
  data() {
    return {
      uid: drupalSettings['user']['uid'],
      settings: drupalSettings['work_time_sheet'],
      typePick: 'normal',
      hourPick: null,
      hourMaxNormal: 8,
      hourMaxOver: 8,
      hourMaxOverSpecial: 8
    };
  },
  methods: {
    updatePick(value) {
      this.typePick = value.type;
      this.hourPick = value.hour;
      this.hourMaxNormal = value.hourMaxNormal;
      this.hourMaxOver = value.hourMaxOver;
      this.hourMaxOverSpecial = value.hourMaxOverSpecial;
    },
  },
  components: {
    TimePickerComponent,
    TableComponent,
  }
});

App.mount('#work-time-sheet');
