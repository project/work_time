import InfoCellComponent from "./InfoCellComponent.js";

let templateWorkTimeSheetCellDefault = `
<td ref="cell" class="position-relative text-center wt-w-cell wt-cell-pick" :class="cellClass(dataCell)" :class="dataCell.style" @click.self="pointHandle"  :data-wid-normal="dataCell.wid_normal" :data-wid-over="dataCell.wid_over" :data-uid="dataCell.uid" :data-reference-id="dataCell.reference_id" :data-date="dataCell.date" :data-is-commit="dataCell.is_commit" :data-worktime="hourRound(dataCell.worktime)" :data-sheet-normal="hourRound(dataCell.sheet_normal)" :data-sheet-over="hourRound(dataCell.sheet_over)">{{ displayCell(dataCell) }}
  <InfoCellComponent :date="dataCell.date" :reference_id="dataCell.reference_id"/>
</td>
`;
export default {
  template: typeof templateWorkTimeSheetCell !== 'undefined' ? templateWorkTimeSheetCell : templateWorkTimeSheetCellDefault,
  props: {
    dataCell: Object,
    typePick: String,
    hourPick: Number,
  },
  methods: {
    pointHandle($event) {
      // Event point.
      if (!this.typePick || !this.hourPick) {
        return
      }
      // Emit event click to update footer.
      this.$emit('cellClicked', {
        dataCell: this.dataCell,
        typePick: this.typePick,
        hourPick: this.hourPick
      });
    },
    hourRound(time) {
      if (time == null) {
        return null;
      }
      return Math.ceil(parseFloat(time) / 3600) || 0;
    },
    displayCell(dataCell) {
      let hour = 0;
      if (dataCell.worktime !== null) {
        hour += Math.ceil(parseFloat(dataCell.worktime) / 3600);
      }
      if (dataCell.normal !== null || dataCell.over !== null) {
        hour = 0;
        if (dataCell.normal) {
          hour += Math.ceil(parseFloat(dataCell.normal) / 3600);
        }
        if (dataCell.over) {
          hour += Math.ceil(parseFloat(dataCell.over) / 3600);
        }
      }
      if (!hour) {
        return null;
      }
      return hour || null;
    },
    cellClass(dataCell) {
      let classCell = 'text-secondary';
      if (dataCell.worktime && parseFloat(dataCell.worktime) > 0) {
        classCell = 'text-secondary';
      }
      if (dataCell.normal && parseFloat(dataCell.normal) > 0) {
        classCell = 'text-success';
      }
      if (dataCell.over && parseFloat(dataCell.over) > 0) {
        classCell = 'text-danger';
        if (dataCell.status == 1) {
          classCell += ' wt-shadow-confirm';
        }
      }
      if (dataCell.normal && dataCell.over && parseFloat(dataCell.normal) > 0 && parseFloat(dataCell.over) > 0) {
        classCell = 'text-warning';
      }
      return classCell
    },
  },
  components: {
    InfoCellComponent
  }
};
