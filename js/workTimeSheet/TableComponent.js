import Store from "./../components/store.js";
import CellComponent from "./CellComponent.js";
import CommitComponent from './CommitComponent.js';

let templateWorkTimeSheetTableDefault = `
<div>
  <CommitComponent :data="data" :csrfToken="csrfToken"/>
  <table class="table table-bordered table-hover table-responsive wt-table">
    <thead>
      <tr>
        <template v-for="(dataHeader, indexHeader) in dataHeader">
        <th v-if="dataHeader.sign == null" class="text-center">{{ dataHeader.data }}</th>
        <th v-else :data-date="dataHeader.sign" class="text-center wt-w-cell" :class="dataHeader.style">{{ dataHeader.data }}</th>
        </template>
      </tr>
    </thead>
    <tbody>
      <template v-for="(dataRow, indexRow) in data">
      <tr :data-reference-id="dataRow.id">
        <th>{{ dataRow.label }}</th>
        <th class="text-center align-middle" v-if="dataRow.price">{{ dataRow.price }}</th>
        <template v-for="(dataCell, index_cell) in dataRow.cells">
         <CellComponent :dataCell="dataCell" :typePick="typePick" :hourPick="hourPick" @cellClicked="handleCellClick" />
        </template>
        <td class="text-center align-middle">{{ totalRow[dataRow.id].hours }}</td>
        <td class="text-center align-middle" v-if="totalRow[dataRow.id].price">{{ totalRow[dataRow.id].price }}</td>
      </tr>
      </template>
    </tbody>
    <tfoot>
      <tr>
        <template v-for="(dataFooter, index_footer) in dataFooter">
        <th v-if="dataFooter.sign == null" class="text-center">{{ dataFooter.data }}</th>
        <td v-else :data-date="dataFooter.sign" class="text-center wt-w-cell" :class="dataFooter.style">{{ dataFooter.data }}</td>
        </template>
      </tr>
    </tfoot>
  </table>
</div>
`;

export default {
  template: typeof templateWorkTimeSheetTable !== 'undefined' ? templateWorkTimeSheetTable : templateWorkTimeSheetTableDefault,
  name: 'TableComponent',
  data() {
    return {
      data: [],
      dataHeader: [],
      dataFooter: [],
      totalRow: {},
      leaveDays: [],
    }
  },
  props: {
    uid: String,
    settings: Object,
    typePick: String,
    hourPick: String,
  },
  methods: {
    async getLeave() {
      // Load custom leave.
      let url_leave = this.settings.options.custom_data_leave;
      if (url_leave.length) {
        url_leave = url_leave.trim();
        await axios.get(url_leave, {
          params: {
            _format: 'json',
            uids: [this.uid],
            filter: this.settings.filter_value,
            filter_mode: this.settings.options.filter,
          },
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': ''
          }
        }).then((response) => {
          this.leaveDays = response.data;
        }).catch(error => {

        })
      }
    },
    parseDateRange(dataBody) {
      let dataWorktime = dataBody.filter(item => item.bundle == 'work_time');
      let dataSheet = dataBody.filter(item => item.bundle == 'attendance');

      // 1st cell header.
      this.dataHeader.push({
          'data': Drupal.t('Project'),
          'sign': null
        }
      );
      // Header price col.
      if (this.settings.options.price_field.length) {
        this.dataHeader.push({
            'data': Drupal.t('Price'),
            'sign': null
          }
        );
      }
      // 1st cell footer.
      this.dataFooter.push({
          'data': Drupal.t('Total'),
          'sign': null
        }
      );
      // Footer price col.
      if (this.settings.options.price_field.length) {
        this.dataFooter.push({
            'data': null,
            'sign': null
          }
        );
      }

      let range_filter = Store.getRangeFilter(this.settings.filter_value, this.settings.options.filter);
      range_filter.forEach(date => {
        let y = date.getFullYear();
        let m = (date.getMonth() + 1).toString().padStart(2, '0');
        let d = date.getDate().toString().padStart(2, '0');
        let str_date = `${y}-${m}-${d}`;
        // Header cell.
        let cell_header = {
          'data': d,
          'sign': str_date,
          'style': Store.styleClass(str_date, this.settings.options.holidays, this.settings.options.work_day, this.leaveDays, this.uid),
        };
        this.dataHeader.push(cell_header);
        // Footer cell.
        let cell_footer = {
          'data': null,
          'sign': str_date,
          'style': Store.styleClass(str_date, this.settings.options.holidays, this.settings.options.work_day, this.leaveDays, this.uid),
        };
        this.dataFooter.push(cell_footer);
      })

      // Last column header.
      this.dataHeader.push({
          'data': Drupal.t('Total hours'),
          'sign': null
        }
      );
      // Header column total price.
      if (this.settings.options.price_field.length) {
        this.dataHeader.push({
            'data': Drupal.t('Total price'),
            'sign': null
          }
        );
      }
      // Last column footer.
      this.dataFooter.push({
          'data': null,
          'sign': 'total_hours'
        }
      );
      // Footer column total price.
      if (this.settings.options.price_field.length) {
        this.dataFooter.push({
            'data': null,
            'sign': 'total_price'
          }
        );
      }

      // Body cell.
      this.settings.list.forEach((item_reference) => {

        // 1st body cell.
        let cell_body = {
          'label': item_reference.label,
          'entity_type': item_reference.entity_type,
          'sign': null,
          'id': item_reference.id,
          'cells': [],
        };
        if (this.settings.options.price_field.length) {
          cell_body.price = item_reference.price ? new Intl.NumberFormat(Store.language).format(item_reference.price) + (item_reference.suffix_price || '') : new Intl.NumberFormat(Store.language).format(0) + (item_reference.suffix_price || '');
        }
        range_filter.forEach((date) => {
          let y = date.getFullYear();
          let m = (date.getMonth() + 1).toString().padStart(2, '0');
          let d = date.getDate().toString().padStart(2, '0');
          let str_date = `${y}-${m}-${d}`;
          let dataFindSheet = dataSheet.filter((item_sheet) => item_sheet.date == str_date && item_sheet.reference_id == item_reference.id);
          let dataFindWorktime = dataWorktime.filter((item_worktime) => item_worktime.date == str_date && item_worktime.reference_id == item_reference.id)
          let item_cell = {
            'data_worktime': dataFindWorktime,
            'date': str_date,
            'uid': this.uid,
            'reference_id': item_reference.id,
            'worktime': null,
            'normal': null,
            'over': null,
            'wid_normal': null,
            'wid_over': null,
            'is_commit': false,
            'style': Store.styleClass(str_date, this.settings.options.holidays, this.settings.options.work_day, this.leaveDays, this.uid),
          };
          if (dataFindSheet.length) {
            dataFindSheet.forEach((item) => {
              if (item.payroll == 1) {
                item_cell.normal = item.time_total;
                item_cell.wid_normal = item.id;
              }
              else {
                item_cell.status = item.status;
                item_cell.over = item.time_total;
                item_cell.wid_over = item.id;
              }
            })
          }
          cell_body.cells.push(item_cell);
        });
        this.data.push(cell_body);
        this.totalRow[item_reference.id] = {};
      });

      this.updateTotalData();
    },
    // Handle action eraser.
    eraserHandle(infoPoint) {
      let changed = false;
      if (infoPoint.dataCell.worktime) {
        infoPoint.dataCell.worktime = 0;
        changed = true;
      }
      if (infoPoint.dataCell.normal) {
        infoPoint.dataCell.normal = 0;
        changed = true;
      }
      if (infoPoint.dataCell.over) {
        infoPoint.dataCell.over = 0;
        changed = true;
      }
      if (changed) {
        infoPoint.dataCell['is_commit'] = true;
      }
      else {
        infoPoint.dataCell['is_commit'] = false;
      }
      this.updateTotalData();
    },
    // Handle cell click.
    handleCellClick(infoPoint) {

      // Handle with case eraser.
      if (infoPoint.typePick === "eraser") {
        this.eraserHandle(infoPoint);
        return;
      }

      let special = false;
      if (infoPoint.dataCell.style['wt-weekend'] || infoPoint.dataCell.style['wt-holiday'] || infoPoint.dataCell.style['wt-vacation']) {
        infoPoint.typePick = 'over';
        special = true;
      }

      if (infoPoint.dataCell.style['leave']) {
        return;
      }

      let total_data = this.totalData();
      let sum_details = total_data.total_col[infoPoint.dataCell.date];

      // Get hour allowed.
      let hour_allow = 0;
      if (infoPoint.typePick === 'normal') {
        hour_allow = this.calculateHourPickAllow(infoPoint.typePick, sum_details[infoPoint.typePick] / 3600, infoPoint.hourPick, infoPoint.dataCell[infoPoint.typePick] / 3600, this.$parent.hourMaxNormal)
      }
      if (infoPoint.typePick === 'over') {
        let maxHour = this.$parent.hourMaxOver;
        if (special) {
          maxHour = this.$parent.hourMaxOverSpecial;
        }
        hour_allow = this.calculateHourPickAllow(infoPoint.typePick, sum_details[infoPoint.typePick] / 3600, infoPoint.hourPick, infoPoint.dataCell[infoPoint.typePick] / 3600, maxHour)
      }

      // Check the hours allowed.
      if (hour_allow > 0) {
        if (infoPoint.hourPick > hour_allow) {
          infoPoint.hourPick = hour_allow * 3600;
        }
        else {
          infoPoint.hourPick = parseInt(infoPoint.hourPick) * 3600;
        }
        infoPoint.dataCell[infoPoint.typePick] = infoPoint.hourPick;
        infoPoint.dataCell['is_commit'] = true;
      }
      else {
        return;
      }

      this.updateTotalData();
    },

    // Function return hour allow.
    calculateHourPickAllow(typePick, totalCol, hourPick, cellHour, hourMax) {
      if ((typePick === 'normal' || typePick === 'over') && totalCol - cellHour < hourMax) {
        if (totalCol + hourPick - (cellHour || 0) > hourMax) {
          let hour_pick_allow = hourMax - (totalCol - (cellHour || 0));
          return Math.max(0, hour_pick_allow);
        }
        else {
          return hourMax;
        }
      }
      return 0;
    },
    // Load work time to body cell.
    async worktime_sheet() {
      let url = 'api/work-time-list/worktime_sheet';
      if (!Store.csrfToken) {
        await Store.csrfTokenLoader();
      }
      axios.get(Drupal.url(url), {
        params: {
          _format: 'json',
          filter: this.settings.filter_value,
          filter_mode: this.settings.options.filter
        },
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': Store.csrfToken
        }
      }).then((response) => {
        this.parseDateRange(response.data);
      });
    },
    totalData() {
      let total = {
        'normal': 0,
        'over': 0
      };
      let total_row = {};
      let total_col = {};
      this.data.forEach((row) => {
        if (row.cells.length) {
          row.cells.forEach((item) => {
            if (total_row[row.id]) {
              total_row[row.id].normal += Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_row[row.id].over += Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
            else {
              total_row[row.id] = {};
              total_row[row.id].normal = Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_row[row.id].over = Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }

            if (total_col[item['date']]) {
              total_col[item['date']].normal += Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_col[item['date']].over += Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
            else {
              total_col[item['date']] = {};
              total_col[item['date']].normal = Math.ceil(parseInt(item.normal) / 3600) * 3600 || Math.ceil(parseInt(item.worktime) / 3600) * 3600 || 0;
              total_col[item['date']].over = Math.ceil(parseInt(item.over) / 3600) * 3600 || 0;
            }
          })
        }
      })

      for (const totalColKey in total_col) {
        total.normal += Math.ceil(parseInt(total_col[totalColKey].normal) / 3600) * 3600 || Math.ceil(parseInt(total_col[totalColKey].worktime) / 3600) * 3600 || 0;
        total.over += Math.ceil(parseInt(total_col[totalColKey].over) / 3600) * 3600 || 0;
      }

      return {
        'total': total,
        'total_row': total_row,
        'total_col': total_col
      };
    },
    updateTotalData() {
      let total_data = this.totalData();

      // Set total row.
      let total_price = 0;
      for (const row_id in total_data.total_row) {
        let row = this.settings.list.find(item_reference => item_reference.id == row_id);
        if (this.totalRow.hasOwnProperty(row_id)) {
          this.totalRow[row_id].hours = (parseInt(total_data.total_row[row_id].normal) + parseInt(total_data.total_row[row_id].over)) / 3600 || null;
          if (this.settings.options.price_field) {
            this.totalRow[row_id].price = (new Intl.NumberFormat(Store.language).format(((parseInt(total_data.total_row[row_id].normal) + parseInt(total_data.total_row[row_id].over)) / 3600 * row.price)) + (this.settings.suffix_price || '')) || null;
          }
          total_price += ((parseInt(total_data.total_row[row_id].normal) + parseInt(total_data.total_row[row_id].over)) / 3600 * row.price) || 0;
        }
      }

      // Set total footer.
      for (const col_id in total_data.total_col) {
        let indexCol = this.dataFooter.findIndex((item) => item.date == col_id);
        if (indexCol >= 0) {
          this.dataFooter[indexCol].data = (parseInt(total_data.total_col[col_id].normal) + parseInt(total_data.total_col[col_id].over)) / 3600 || null;
        }
      }
      let indexTotalHours = this.dataFooter.findIndex((item) => item.sign == 'total_hours');
      if (indexTotalHours >= 0) {
        this.dataFooter[indexTotalHours].data = new Intl.NumberFormat(Store.language).format((parseInt(total_data.total.normal) + parseInt(total_data.total.over)) / 3600) || null;
      }
      let indexTotalPrice = this.dataFooter.findIndex((item) => item.sign == 'total_price');
      if (indexTotalPrice >= 0) {
        this.dataFooter[indexTotalPrice].data = new Intl.NumberFormat(Store.language).format(total_price) + (this.settings.suffix_price || '');
      }
    }
  },
  components: {
    CommitComponent,
    CellComponent,
  },
  async mounted() {
    await this.getLeave();
    this.worktime_sheet();
  }
};
