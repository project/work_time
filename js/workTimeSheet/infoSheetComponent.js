const {Popover} = bootstrap;
let templateInfoDefault = `
<i @click.self="showInfo" class="wt-icon-info fs-4 bi bi-info-circle-fill"></i>
<div class="d-none">
  <ul class="content-popup-worktime-history wt-list-info" ref="popoverInfo">
    <li><i>${Drupal.t('Note the symbols:')}</i></li>
    <li><span class="wt-symbol wt-holiday"></span> ${Drupal.t('Holidays')}</li>
    <li><span class="wt-symbol wt-weekend"></span> ${Drupal.t('Last day off every week')}</li>
    <li><span class="wt-symbol wt-vacation"></span> ${Drupal.t('Paid days off')}</li>
    <li><span class="wt-symbol wt-leave"></span> ${Drupal.t('Unpaid day off')}</li>
    <li><span class="wt-symbol wt-half-day"></span> ${Drupal.t('Take a half day off')}</li>
    <li><span class="wt-symbol wt-tardiness-leave-early"></span> ${Drupal.t('Tardiness leave early')}</li>
    <li><span class="wt-symbol text-success">8</span> ${Drupal.t('Office hours')}</li>
    <li><span class="wt-symbol text-danger">2</span> ${Drupal.t('Hours of overtime')}</li>
    <li><span class="wt-symbol text-warning">10</span> ${Drupal.t('Office hours and overtime hours')}</li>
    <li><span class="wt-symbol position-relative"><i class="wt-icon-info position-absolute d-flex top-0 end-0 bi bi-info-circle-fill"></i></span> ${Drupal.t('Hour information from worktime')}</li>
  </ul>
</div>
`;

export default {
  template: typeof templateInfo !== 'undefined' ? templateInfo : templateInfoDefault,
  data() {
    return {
      bsPopover: null
    }
  },
  methods: {
    showInfo($event) {
      if (this.bsPopover) {
        this.bsPopover.toggle()
        return;
      }
      this.bsPopover = new Popover($event.target, {
        trigger: 'focus',
        html: true,
        content: () => {
          return this.$refs.popoverInfo;
        }
      });
      this.bsPopover.toggle();
    }
  }
}
