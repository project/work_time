import InfoSheetComponent from './infoSheetComponent.js';

let templateWorkTimeSheetPickerDefault = `
<div>
    <div class="btn-group">
      <div class="input-group-text" id="btnGroupAddon">${Drupal.t('Normal time')}</div>
      <template v-for="n in hourMaxNormal">
      <input type="radio" class="btn-check" name="radio-hour-pick" :id="'radio-normal-' + n" :value="n" data-hour-type="normal" @click="getHourPick">
      <label class="btn btn btn-outline-success" :for="'radio-normal-' + n">{{ n }}</label>
      </template>
    </div>
    <div class="btn-group">
      <div class="input-group-text" id="btnGroupAddon">${Drupal.t('Over time')}</div>
      <template v-for="n in hourMaxOverSpecial">
      <input type="radio" class="btn-check" name="radio-hour-pick" :id="'radio-over-' + n" :value="n" data-hour-type="over" @click="getHourPick" >
      <label class="btn btn btn-outline-danger" :for="'radio-over-' + n">{{ n }}</label>
      </template>
    </div>
    <div class="btn-group">
      <div class="input-group-text" id="btnGroupAddon">${Drupal.t('Remove')}</div>
      <input type="radio" class="btn-check" name="radio-hour-pick" id="radio-eraser" data-hour-type="eraser" :value="0" @click="getHourPick($event)">
      <label class="btn btn btn-outline-danger" for="radio-eraser"><i class="bi bi-eraser-fill"></i></label>
    </div>
    <InfoSheetComponent/>
</div>
`;

export default {
  template: typeof templateWorkTimeSheetPicker !== 'undefined' ? templateWorkTimeSheetPicker : templateWorkTimeSheetPickerDefault,
  name: 'TimePickerComponent',
  data() {
    return {
      hourMaxNormal: parseInt(drupalSettings['work_time_sheet']['options']['hours']) || 8,
      hourMaxOver: parseInt(drupalSettings['work_time_sheet']['options']['over_hours']) || 8,
      hourMaxOverSpecial: (parseInt(drupalSettings['work_time_sheet']['options']['over_hours']) + parseInt(drupalSettings['work_time_sheet']['options']['hours'])) || 8,
      typePick: 'normal',
      hourPick: 8,
    };
  },
  methods: {
    getHourPick(event) {
      this.typePick = event.target.dataset.hourType;
      this.hourPick = event.target.value;
      this.$emit('pickChanged', {
        type: this.typePick,
        hour: this.hourPick,
        hourMaxNormal: this.hourMaxNormal,
        hourMaxOver: this.hourMaxOver,
        hourMaxOverSpecial: this.hourMaxOverSpecial
      });
    },
  },
  components: {
    InfoSheetComponent
  },
  mounted() {
    document.querySelector('#radio-normal-' + this.hourMaxNormal).checked = true;
    this.$emit('pickChanged', {
      type: 'normal',
      hour: this.hourMaxNormal,
      hourMaxNormal: this.hourMaxNormal,
      hourMaxOver: this.hourMaxOver,
      hourMaxOverSpecial: this.hourMaxOverSpecial
    });
  },
};
