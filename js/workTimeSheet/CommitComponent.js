import Store from "./../components/store.js";

let templateWorkTimeSheetCommitDefault = `
<div class="mt-2">
  <button class="btn btn-success" @click="commitData">${Drupal.t('Commit')}</button>
</div>
`;
export default {
  template: typeof templateWorkTimeSheetCommit !== 'undefined' ? templateWorkTimeSheetCommit : templateWorkTimeSheetCommitDefault,
  name: 'workTimeSheetCommit',
  props: {
    data: Array,
    csrfToken: String,
    hourMaxNormal: Number
  },
  methods: {
    async commitData(event) {
      event.target.setAttribute('disabled', 'disabled');
      let data = {};
      this.data.forEach((item) => {
        if (!data[item.id]) {
          data[item.id] = {};
          item.cells.forEach((cell) => {
            if (cell.is_commit) {
              data[item.id][cell.date] = [];
              let payroll = 0;
              let time_normal =  parseFloat(cell.normal || cell.worktime) || 0;
              let time_over = parseFloat(cell.over) || 0;
              if (!payroll && cell.style['wt-holiday']) {
                payroll = 2;
                time_over += time_normal;
                time_normal = 0;
              }
              if (!payroll && cell.style['wt-weekend']) {
                payroll = 3;
                time_over += time_normal;
                time_normal = 0;
              }
              if (!payroll && cell.style['wt-vacation']) {
                payroll = 4;
                time_over += time_normal;
                time_normal = 0;
              }
              data[item.id][cell.date].push({
                'wid': cell.wid_normal || null,
                'entity_type': item.entity_type || null,
                'hour': time_normal,
                'status': 1,
                'payroll': payroll || 1
              })
              data[item.id][cell.date].push({
                'wid': cell.wid_over || null,
                'entity_type': item.entity_type || null,
                'hour': time_over,
                'status': 0,
                'payroll': payroll || 0
              })
            }
          })
          if (!Object.keys(data[item.id]).length) {
            delete data[item.id];
          }
        }
      })
      if (Object.keys(data).length) {
        if (!Store.csrfToken) {
          await Store.csrfTokenLoader();
        }
        axios.patch(Drupal.url('api/work-time/0/sheet_commit') + '?_format=json',
          {
            data: data
          },
          {
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-Token': Store.csrfToken
            }
          }
        ).then((response) => {
          location.reload();
        }).catch(error => {
          event.target.removeAttribute('disabled');
          console.error("There was an error!", error);
        });
      }
      else {
        event.target.removeAttribute('disabled');
      }
    },
  }
};
