import Store from "./../components/store.js";

const {Popover} = bootstrap;
let templateInfoCellDefault = `
<template v-if="dataInfo.length || normal || over">
  <i @click.self="showInfo" class="wt-icon-info position-absolute d-flex top-0 end-0 bi bi-info-circle-fill"></i>
  <div class="d-none">
    <div class="content-popup-worktime-history" ref="popoverInfo">
      <div>
        <p v-if="normal" class="mb-0 text-success">${Drupal.t('Normal hour')}: {{ normal }}</p>
        <p v-if="over" class="mb-0 text-danger">${Drupal.t('Over hour')}: {{ over }}</p>
      </div>
      <table v-if="dataInfo.length" class="table mb-0 wt-table">
        <thead>
          <tr>
            <th>${Drupal.t('Total')}</th>
            <th><b>{{ displayTimeTotal(total_worktime) }}</b></th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(task, indexTask) in dataInfo" class="position-relative">
            <td>{{ task.label }}</td>
            <td>{{ displayTimeTotal(task.time_total) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</template>
`;

export default {
  template: typeof templateInfoCell !== 'undefined' ? templateInfoCell : templateInfoCellDefault,
  data() {
    return {
      total_worktime: 0,
      normal: null,
      over: null,
      bsPopover: null
    }
  },
  props: {
    date: String,
    reference_id: Number
  },
  methods: {
    displayTimeTotal(time) {
      time *= 1000;
      return Store.formatTime(time, Store.timeFormatFull.hour, Store.timeFormatFull.minute, null)
    },
    showInfo($event) {
      this.data = [];
      this.data.forEach((item) => {
        this.total_worktime += parseInt(item.time_total);
      })

      if (this.bsPopover) {
        this.bsPopover.toggle()
        return;
      }
      this.bsPopover = new Popover($event.target, {
        trigger: 'focus',
        html: true,
        content: () => {
          return this.$refs.popoverInfo;
        }
      });
      this.bsPopover.toggle();
    }
  },
  computed: {
    dataInfo() {
      this.normal = null;
      this.over = null;
      let total_worktime = 0;
      let dataInfo = [];
      if (this.$parent.dataCell.data_worktime.length) {
        this.$parent.dataCell.data_worktime.forEach(item => {
          dataInfo.push(item);
          total_worktime += parseInt(item.time_total);
        })
      }
      this.total_worktime = total_worktime;
      if (this.$parent.dataCell.normal) {
        this.normal = Math.ceil(this.$parent.dataCell.normal / 3600)
      }
      if (this.$parent.dataCell.over) {
        this.over = Math.ceil(this.$parent.dataCell.over / 3600)
      }
      return dataInfo;
    }
  }
}
