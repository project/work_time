import History from "./components/history.js";
import Store from "./components/store.js";
import reactiveWorktime from "./components/reactive-worktime.js";

(function (Drupal, drupalSettings, once) {

  const vueElements = document.querySelectorAll(".btn-work-time");
  const app = {};
  const listBtnIds = [];
  vueElements.forEach(element => {
    let id = element.getAttribute('id');
    let uid = drupalSettings.user.uid;
    let entityId = element.querySelector('button[ref]').dataset.entityId || null;
    let entityType = element.querySelector('button[ref]').dataset.entityType || null;
    let entityField = element.querySelector('button[ref]').dataset.entityField || null;
    let referenceId = element.querySelector('button[ref]').dataset.referenceId || null;
    let referenceType = element.querySelector('button[ref]').dataset.referenceType || null;
    let referenceField = element.querySelector('button[ref]').dataset.referenceField || null;
    listBtnIds.push(id);

    app[id] = Vue.createApp({
      data() {
        return {
          id: id,
          uid: uid,
          entity: {
            entityId: entityId,
            entityType: entityType,
            entityField: entityField,
            referenceId: referenceId,
            referenceType: referenceType,
            referenceField: referenceField,
          },
          isPlaying: false,
          taskPlaying: null,
          interval: null,
          startTime: null,
          elapsedTime: 0,
          elapsedTimeTotal: 0,
          bsPopover: null,
          historyData: [],
        }
      },
      computed: {
        formatTime() {
          if (this.isPlaying) {
            return Store.formatTime(this.elapsedTime, Store.timeFormatFull.hour, Store.timeFormatFull.minute, Store.timeFormatFull.second, ':')
          }
          return Store.formatTime(this.elapsedTimeTotal, Store.timeFormatFull.hour, Store.timeFormatFull.minute, Store.timeFormatFull.second, ':', 'compact')
        },
      },
      methods: {

        // Counter time.
        countUp(time) {
          this.interval = setInterval(() => {
            this.elapsedTime = Math.floor((Date.now() - time));
          }, 1000);
        },

        // Play/stop.
        async togglePlay() {
          let isActionPlay = true;
          if (reactiveWorktime.state.hasOwnProperty(id) && reactiveWorktime.state[id].isPlaying) {
            isActionPlay = false;
          }

          let isActionStop = true;
          if (reactiveWorktime.state.hasOwnProperty(id) && !reactiveWorktime.isPlaying) {
            isActionStop = false;
          }

          if (isActionStop) {
            await reactiveWorktime.stop();
          }

          if (isActionPlay) {
            reactiveWorktime.play(id, this.entity);
          }
        },
      },
      components: {
        "history": History
      },
    });
    reactiveWorktime.state[id] = app[id].mount(element);
  });

  // Check playing.
  if(reactiveWorktime.isPlaying && reactiveWorktime.taskPlaying) {
    let id = reactiveWorktime.taskPlaying.entity_type + '-' + reactiveWorktime.taskPlaying.entity_id;
    if (reactiveWorktime.state.hasOwnProperty(id)) {
      reactiveWorktime.state[id].isPlaying = true;
      reactiveWorktime.state[id].taskPlaying = reactiveWorktime.taskPlaying;
      reactiveWorktime.state[id].countUp(reactiveWorktime.taskPlaying.created);
    }
  }

  // Request all time button.
  if(listBtnIds.length) {
    reactiveWorktime.loadTotalTime(listBtnIds);
  }
  reactiveWorktime.getDataCondition("playing");
})(Drupal, drupalSettings, once);
