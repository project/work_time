<?php

/**
 * @file
 * Provides a work time entity type.
 */

use Drupal\Core\Render\Element;

/**
 * Implements hook_theme().
 */
function work_time_theme() {
  return [
    'work_time' => [
      'render element' => 'elements',
    ],
    'work_time_block' => [
      'variables' => [
        'label' => '',
        'display' => '',
        'entity_id' => '',
        'entity_type' => '',
        'entity_bundle' => '',
        'entity_field' => '',
        'reference_id' => '',
        'reference_type' => '',
        'reference_field' => '',
        'limit' => 'week',
      ],
    ],
    'work_time_formatter' => [
      'variables' => [
        'entity_id' => '',
        'entity_type' => '',
        'entity_field' => '',
        'reference_id' => '',
        'reference_type' => '',
        'reference_field' => '',
        'beginning' => 0,
        'time' => 0,
      ],
    ],
    'views_view_work_time' => [
      'file' => 'work_time.views.theme.inc',
    ],
    'views_view_work_time_table' => [
      'file' => 'work_time.views.theme.inc',
    ],
    'views_view_work_time_sheet' => [
      'file' => 'work_time.views.theme.inc',
    ],
    'views_view_work_timekeeper' => [
      'file' => 'work_time.views.theme.inc',
    ],
    'views_view_work_time_worktime_distance' => [
      'file' => 'work_time.views.theme.inc',
    ],
    'views_view_work_time_worktime_salary' => [
      'file' => 'work_time.views.theme.inc',
    ],
  ];
}

/**
 * Prepares variables for work time templates.
 *
 * Default template: work-time.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing work time information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_work_time(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
